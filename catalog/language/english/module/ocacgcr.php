<?php
      //===========================================//
     // Customer Group Checkout Requirements      //
    // Author: Joel Reeds                        //
   // Company: OpenCart Addons                  //
  // Website: http://opencartaddons.com        //
 // Contact: webmaster@opencartaddons.com     //
//===========================================//

// Checkout Errors
$_['error_min_total']	= 'A minimum cart value of %s is required to checkout!';
$_['error_max_total']	= 'Your order must not exceed %s!';

$_['error_min_quantity']	= 'A minimum of %s items is required to checkout!';
$_['error_max_quantity']	= 'Your order must not exceed %s items!';

$_['error_min_weight']	= 'A minimum cart weight of %s is required to checkout!';
$_['error_max_weight']	= 'Your order must not exceed %s!';

$_['error_min_product']	= 'Minimum order amount for %s is %s!';
$_['error_max_product']	= 'Maximum order amount for %s is %s!';

//Product Min/Max (Product Page)
$_['text_minimum'] 		= 'This product has a minimum quantity of %s!';
$_['text_maximum'] 		= 'This product has a maximum quantity of %s!';
?>