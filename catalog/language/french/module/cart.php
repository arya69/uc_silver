<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//----------------------------------

// Heading
$_['heading_title']			= 'Panier';

// Text
$_['text_items']			= '%s article(s) - %s';
$_['text_empty']			= 'Votre panier est vide !';
$_['text_cart']				= 'Voir panier';
$_['text_checkout']			= 'Commander';
$_['text_payment_profile']	= 'Profil de paiement';
?>