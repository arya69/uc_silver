<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Text
$_['text_title']			= 'Compte Klarna';
$_['text_pay_month']		= 'Compte Klarna - Paiement depuis %s/mois <span id="klarna_account_toc_link"></span><script text="javascript">$.getScript(\'http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js\', function(){ var terms = new Klarna.Terms.Account({ el: \'klarna_account_toc_link\', eid: \'%s\',   ville : \'%s\'});})</script>';
$_['text_information']		= 'Information du compte Klarna';
$_['text_additional']		= 'Compte Klarna requiert des informations suppl&eacute;mentaires avant de pouvoir proc&eacute;der &agrave; votre commande.';
$_['text_wait']				= 'Veuillez patienter !';
$_['text_male']				= 'Homme';
$_['text_female']			= 'Femme';
$_['text_year']				= 'Ann&eacute;e';
$_['text_month']			= 'Mois';
$_['text_day']				= 'Jour';
$_['text_payment_option']	= 'Options de paiement';
$_['text_single_payment']	= 'Simple paiement';
$_['text_monthly_payment']	= '%s - %s par mois';
$_['text_comment']			= 'Identifiant de la facture Klarna: %s\n%s/%s: %.4f';

// Entry
$_['entry_gender']			= 'Genre :';
$_['entry_pno']				= 'N&deg; personnel :<br /><span class="help">Veuillez entrer ici votre N&deg; de s&eacute;rit&eacute; socialen ici.</span>';
$_['entry_dob']				= 'Date de naissance';
$_['entry_phone_no']       = 'N&deg; de t&eacute;l&eacute;phone :<br /><span class="help">Veuillez entrer ici votre N&deg; de t&eacute;l&eacute;phone.</span>';
$_['entry_street']         = 'Rue :<br /><span class="help">Veuillez noter que la livraison ne peut pas avoir lieu &agrave; l&#8217;adresse enregistr&eacute;e lorsque vous payez par Klarna.</span>';
$_['entry_house_no']       = 'N&deg; de rue :<br /><span class="help">Veuillez entrer ici votre N&deg; de rue.</span>';
$_['entry_house_ext']      = 'Informations compl&eacute;mentaires :<br /><span class="help">Veuillez entrer ici les informations compl&eacute;mentaires concernant votre compl&eacute;ment d&#8217;adresse. Ex. : A, B, C, Rouge, Bleu ect...</span>';
$_['entry_company']        = 'Num&eacute;ro d&#8217;immatriculation soci&eacute;t&eacute; :<br /><span class="help">Veuillez entrer ici votre N&deg; d&#8217;immatriculation soci&eacute;t&eacute;.</span>';

// Error
$_['error_deu_terms']		= 'Vous devez accepter la politique de confidentialit&eacute; de Klarna.';
$_['error_address_match']	= 'Les adresses de facturation et d&#8217:exp&eacute;dition doivent correspondre si vous souhaitez utiliser Klarna';
$_['error_network']			= 'Une erreur est survenue lors de la connexion &agrave; Klarna. Veuillez r&eacute;essayer plus tard.';
?>