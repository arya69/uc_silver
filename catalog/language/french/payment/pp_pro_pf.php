<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Text
$_['text_title']           = 'Carte de cr&eacute;dit (Traitement s&eacute;curis&eacute; par PayPal)';
$_['text_credit_card']     = 'D&eacute;tails de la carte de cr&eacute;dit';
$_['text_start_date']      = '(si disponible)';
$_['text_issue']           = '(pour carte Maestro et Solo seulement)';
$_['text_wait']            = 'Veuillez patienter !';

// Entry
$_['entry_cc_owner']       = 'Propri&eacute;taire de la carte :';
$_['entry_cc_type']        = 'Type de carte :';
$_['entry_cc_number']      = 'N&deg; de la carte :';
$_['entry_cc_start_date']  = 'Date de validit&eacute; de la carte :';
$_['entry_cc_expire_date'] = 'Date d&#8217;expiration de la carte :';
$_['entry_cc_cvv2']        = 'Code de s&eacute;curit&eacute; de la carte (CVV2) :';
$_['entry_cc_issue']       = 'Num&eacute;ro d&#8217;&eacute;mission de la carte :';

// Error
$_['error_required']       = 'Attention : Tous les champs d&#8217;information de paiement sont requis.';
$_['error_general']        = 'Attention : Un probl&egrave;me g&eacute;n&eacute;ral a eu lieu &agrave; la transaction. Veuillez essayer &agrave; nouveau.';
$_['error_config']         = 'Attention : Erreur de configuration du module de paiement. Veuillez v&eacute;rifier les informations de connexion.';
$_['error_address']        = 'Attention : La correspondance du paiement entre l&#8217;adresse, la ville, le d&eacute;partement et le code postal a &eacute;chou&eacute;. Veuillez essayer &agrave; nouveau.';
$_['error_declined']       = 'Attention : Cette transaction a &eacute;t&eacute; refus&eacute;e. Veuillez essayer &agrave; nouveau.';
$_['error_invalid']        = 'Attention : Les informations de carte de cr&eacute;dit ne sont pas valide. Veuillez essayer &agrave; nouveau.';
?>