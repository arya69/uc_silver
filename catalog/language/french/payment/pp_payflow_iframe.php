<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Text
$_['text_title']				= 'Carte de cr&eacute;dit PAYFLOW';
$_['text_secure_connection']	= 'Cr&eacute;er une connexion s&eacute;curis&eacute;e...';

//Errors
$_['error_connection']			= 'Impossible de se connecter &agrave; PayPal. Veuillez prendre contact avec l&#8217;administrateur de la boutique pour obtenir une assistance ou veuillez opter pour un autre mode de paiement.';
?>