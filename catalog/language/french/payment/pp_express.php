<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Text
$_['text_title']					= 'PayPal Express Checkout';
$_['button_continue']               = 'Continuer';
$_['text_cart']                     = 'Panier';
$_['text_shipping_updated']         = 'Le service de livraison a &eacute;t&eacute; mis &agrave; jour';
$_['text_trial']                    = '%s chaque %s %s pour %s paiements depuis ';
$_['text_recurring']                = '%s chaque %s %s';
$_['text_length']                   = ' pour %s paiements';

// Standard checkout error page
$_['error_heading_title']           = 'Il y a une erreur';
$_['error_too_many_failures']       = 'Le d&eacute;lai est d&eacute;pass&eacute; et votre paiement a &eacute;chou&eacute; ';

// Express confirm page
$_['express_text_title']            = 'Confirmer la commande';
$_['express_button_coupon']         = 'Ajouter';
$_['express_button_confirm']        = 'Confirmer';
$_['express_button_login']          = 'Continuer sur PayPal';
$_['express_button_shipping']       = 'Mise &agrave; jour de la livraison';
$_['express_entry_coupon']          = 'Veuillez entrer le code de votre coupon, ici :';
?>