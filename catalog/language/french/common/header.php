<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Text
$_['text_home']			= 'Accueil';
$_['text_wishlist']		= 'Liste de souhaits (%s)';
$_['text_shopping_cart']= 'Panier';
$_['text_search']		= 'Recherche';
$_['text_welcome']		= '<a href="%s">Connexion</a> | <a href="%s">Enregistrement</a>';
$_['text_logged']		= 'Vous &ecirc;tes connect&eacute; sur <a href="%s">%s</a> <b>(</b> <a href="%s">Se d&eacute;connecter</a> <b>)</b>';
$_['text_account']		= 'Compte';
$_['text_checkout']		= 'Commander';
?>