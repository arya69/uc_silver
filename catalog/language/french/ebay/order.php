<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Order totals
$_['lang_shipping']     = 'Livraison';
$_['lang_discount']     = 'Remise';
$_['lang_tax']          = 'Taxe';
$_['lang_subtotal']     = 'Sous-total';
$_['lang_total']        = 'Total';
?>