<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Text
$_['paid_on_amazon_text']	= 'Paiement Amazon';

//Order totals
$_['shipping_text']			= 'livraison';
$_['shipping_tax_text']		= 'Taxe sur la livraison';
$_['gift_wrap_text']		= 'Emballage';
$_['gift_wrap_tax_text']	= 'Taxe sur l&#8217;emballage';
$_['sub_total_text']		= 'Sous-Total';
$_['tax_text']				= 'Taxe';
$_['total_text']			= 'Total';
?>