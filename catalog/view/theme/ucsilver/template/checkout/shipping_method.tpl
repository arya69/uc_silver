<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php 
// print_r($shipping_methods)."<br>";
if ($shipping_methods) { 
?>
<p><?php echo $text_shipping_method; ?></p>
<table class="radio" style="color: #000 !important;">
  <?php 
  foreach ($shipping_methods as $key=>$shipping_method) {
  ?>
	<tr style="color: #000 !important;">
		<td colspan="3"><b><?php echo $shipping_method['title']; ?></b></td>
	</tr>
  <?php
  if ('ucsilver'==$key) 
  {
  ?>
	  <?php if (!$shipping_method['error']) { ?>
	  <?php foreach ($shipping_method['quote'] as $quote) { ?>
	  <tr class="highlight">
		<td valign="top">
			<?php if ($quote['code'] == $code || !$code) { ?>
				<?php $code = $quote['code']; ?>
				<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
			<?php } else { ?>
				<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
			<?php } ?>
		</td>
		<td>			
			<table class="table table-striped">  
				<thead>
				  <col width="auto">
				  <col width="auto">
				  <tr>  
					<th><span class="required">*</span>Shipping Metdod</th>  
					<th><span class="required">*</span>Courier</th>
				  </tr>  
				</thead>  
				<tbody>  
				  <tr>  
					<td>
						<p>
						<select name="method" class="form-control" id="method">
							<option value="">Please select one</option>
							<?php foreach ($quote['method'] as $method) { ?>
							<option value="<?php echo $method; ?>"><?php echo $method; ?></option>
							<?php } ?>
						</select>
						</p>
						<p id="acc_number">
							<label>Account Number :</label>
							<input type="text" name="acc_number" class="form-control"/>
						</p>
					</td>  
					<td>
						<select name="courier" class="form-control" id="courier">
							<option value="">Please select one</option>
							<?php foreach ($quote['courier'] as $courier) { ?>
							<option value="<?php echo $courier; ?>"><?php echo $courier; ?></option>
							<?php } ?>
						</select>
					</td>
				  </tr> 
				</tbody> 
			</table>
		</td>
	  </tr>
	  <?php } ?>
	  <?php } else { ?>
	  <tr>
		<td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
	  </tr>
	  <?php } ?>
  <?php
  } 
  else 
  {
  ?>
	  <?php if (!$shipping_method['error']) { ?>
	  <?php foreach ($shipping_method['quote'] as $quote) { ?>
	  <tr class="highlight">
		<td><?php if ($quote['code'] == $code || !$code) { ?>
		  <?php $code = $quote['code']; ?>
		  <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
		  <?php } else { ?>
		  <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
		  <?php } ?></td>
		<td><label for="<?php echo $quote['code']; ?>"><?php echo $quote['title']; ?></label></td>
		<td style="text-align: right;"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['text']; ?></label></td>
	  </tr>
	  <?php } ?>
	  <?php } else { ?>
	  <tr>
		<td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
	  </tr>
	  <?php } ?>
  <?php
  }
  ?>
  <?php } ?>
</table>
<br />
<?php } ?>
<b><?php echo $text_comments; ?></b>
<textarea name="comment" rows="8" style="width: 98%;"><?php echo $comment; ?></textarea>
<br />
<br />
<div class="buttons">
  <div class="right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="btn btn-primary" />
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	var method = $('#method');
	if (("Collect Account"==method.val()) && (method.val()!="undefined") && (method.val()!=null)) {
		$('#acc_number').show();
	}
	else {
		$('#acc_number').hide();
	}
	method.change(function(e){
		if (("Collect Account"==method.val()) && (method.val()!="undefined") && (method.val()!=null)) {
			$('#acc_number').show();
		}
		else {
			$('input[name=\'acc_number\']').val('');
			$('#acc_number').hide();
		}
	})
});
</script>
