<?php echo $header; ?>
<section>
  <div class="container bn register">
    <div class="row">
      <div class="col-md-12">
        <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <div class="carousel-inner">
            <div class="item active">
              <img src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container cl">
    <div class="row">
      <div class="tab-bg">
        <div class"col-md-12">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#boutique" data-toggle="tab">
                <?php echo $heading_title; ?></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="boutique">
              <div class="row">
                <div class="col-md-2">
                  <?php //echo $column_left; ?>
                  <?php //echo $column_right; ?>
				</div>
                <div class="col-md-10">
                  <!-- register form -->
					<h1><?php echo $heading_title; ?></h1>
					<div class="content"><?php echo $text_error; ?></div>
					<?php /*
					<div class="buttons">
						<div class="right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
					</div>
					*/ ?>
                  <!--end register form --> 
				</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer;?>

<?php /*echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="content"><?php echo $text_error; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; */?>