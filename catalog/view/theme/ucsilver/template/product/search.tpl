<?php 
	echo $header;
?>
	<section>
		<div class="clearfix container bn">
			<!--div class="clearfix col-md-12"-->
				<!--div class="row"-->
					<div id="carousel-example-generic" class="carousel slide carousel-fade">
						<!-- <div class="carousel-inner"> -->
							<!--div class="item active"-->
								<img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/collection-banner.jpg">
							<!--/div-->
						<!-- </div> -->
					</div>
				<!--/div-->
			<!--/div-->
		</div>
				<div class="container cl">
			<div class="row">
				<div class="tab-bg">
					<div class="">
						<ul class="nav nav-tabs">
						  <li class="active" ><a href="#catalogue" data-toggle="tab">CATALOGUE</a></li>
						</ul>
						<div class="tab-content">
						  <div class="tab-pane active" id="catalogue">
							<div class="row">
								<div class="col-md-12">
									<div class="tab-content sec">
									  <div class="tab-pane active" id="ring">
										<div class="ctp">
											
											<div id="content">
												<p class="form-inline" role="form">
													<?php if ($search) { ?>
														<input type="text" class="form-control" name="search" value="<?php echo $search; ?>" />
													<?php } else { ?>
														<input type="text" class="form-control" name="search" value="<?php echo $search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
													<?php } ?>
														<select name="category_id" class="form-control">
															<option value="0"><?php echo $text_category; ?></option>
															<?php foreach ($categories as $category_1) { ?>
															<?php if ($category_1['category_id'] == $category_id) { ?>
															<option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
															<?php } else { ?>
															<option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
															<?php } ?>
															<?php foreach ($category_1['children'] as $category_2) { ?>
															<?php if ($category_2['category_id'] == $category_id) { ?>
															<option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
															<?php } else { ?>
															<option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
															<?php } ?>
															<?php foreach ($category_2['children'] as $category_3) { ?>
															<?php if ($category_3['category_id'] == $category_id) { ?>
															<option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
															<?php } else { ?>
															<option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
															<?php } ?>
															<?php } ?>
															<?php } ?>
															<?php } ?>
														  </select>													
												</p>
												<?php if ($sub_category) { ?>
													<input type="checkbox" name="sub_category" value="1" id="sub_category" checked="checked" />
												<?php } else { ?>
													<input type="checkbox" name="sub_category" value="1" id="sub_category" />
												<?php } ?>
												<label for="sub_category"><?php echo $text_sub_category; ?></label>
												<?php if ($description) { ?>
													<input type="checkbox" name="description" value="1" id="description" checked="checked" />
												<?php } else { ?>
													<input type="checkbox" name="description" value="1" id="description" />
												<?php } ?>
												<label for="description"><?php echo $entry_description; ?></label>
												<div class="buttons">
													<div class="right">
														<input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
													</div>
												</div>
												<hr>
												<h2><?php echo $text_search; ?></h2>
												<?php if ($products) { ?>
													<div class="box-product-src clearfix">
														<?php foreach ($products as $product) { ?>							
															<div class="box">
																<a href="<?php echo $product['href']; ?>">
																	<div class="hover">
																		<div class="hover-content">
																			<h3><?php echo $product['name']; ?></h3>
																			<p><?php echo $product['description']; ?></p>
																			<?php if (false!=$product['price']) { ?>
																			<div class="link">
																				<div class="price btn btn-block btn-primary">
																				<?php echo $product['price']; ?>
																				</div>
																			</div>
																			<?php } ?>
																		</div>
																		<img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
																	</div>
																</a>
															</div>													
														<?php } ?>
													</div>
												<div class="pagination">
													<?php echo $pagination; ?>
												</div>
												<?php } else { ?>
												<div class="content">
													<?php echo $text_empty; ?>
												</div>
												<?php }?>
												</div>
											
										</div>                                                
									  </div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					</div>                                  
				</div>
			</div>
		</div>
	</section>
<script type="text/javascript"><!--
$('#content input[name=\'search\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').bind('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').attr('disabled', 'disabled');
		$('input[name=\'sub_category\']').removeAttr('checked');
	} else {
		$('input[name=\'sub_category\']').removeAttr('disabled');
	}
});

$('select[name=\'category_id\']').trigger('change');

$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';
	
	var search = $('#content input[name=\'search\']').attr('value');
	
	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').attr('value');
	
	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}
	
	var sub_category = $('#content input[name=\'sub_category\']:checked').attr('value');
	
	if (sub_category) {
		url += '&sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

// function display(view) {
	// if (view == 'list') {
		// $('.product-grid').attr('class', 'product-list');
		
		// $('.product-list > div').each(function(index, element) {
			// html  = '<div class="right">';
			// html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
			// html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			// html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
			// html += '</div>';			
			
			// html += '<div class="left">';
			
			// var image = $(element).find('.image').html();
			
			// if (image != null) { 
				// html += '<div class="image">' + image + '</div>';
			// }
			
			// var price = $(element).find('.price').html();
			
			// if (price != null) {
				// html += '<div class="price">' + price  + '</div>';
			// }
						
			// html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			// html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
			
			// var rating = $(element).find('.rating').html();
			
			// if (rating != null) {
				// html += '<div class="rating">' + rating + '</div>';
			// }
				
			// html += '</div>';
						
			// $(element).html(html);
		// });		
		
		// $('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		// $.totalStorage('display', 'list'); 
	// } else {
		// $('.product-list').attr('class', 'product-grid');
		
		// $('.product-grid > div').each(function(index, element) {
			// html = '';
			
			// var image = $(element).find('.image').html();
			
			// if (image != null) {
				// html += '<div class="image">' + image + '</div>';
			// }
			
			// html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			// html += '<div class="description">' + $(element).find('.description').html() + '</div>';
			
			// var price = $(element).find('.price').html();
			
			// if (price != null) {
				// html += '<div class="price">' + price  + '</div>';
			// }	
					
			// var rating = $(element).find('.rating').html();
			
			// if (rating != null) {
				// html += '<div class="rating">' + rating + '</div>';
			// }
						
			// html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			// html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			// html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';
			
			// $(element).html(html);
		// });	
					
		// $('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
		
		// $.totalStorage('display', 'grid');
	// }
// }

// view = $.totalStorage('display');

// if (view) {
	// display(view);
// } else {
	// display('list');
// }
//--></script> 
<?php echo $footer; ?>