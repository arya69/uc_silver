										<div class="tab-pane active">
											<h3><?php echo $heading_title; ?></h3>
											<hr>
											<?php echo $text_critea; ?>											
											<div class="row">
												<div class="col-md-12">
												<div id="content">
												<p class="form-inline" role="form">
													<?php if ($search) { ?>
														<input type="text" class="form-control" name="search" value="<?php echo $search; ?>" />
													<?php } else { ?>
														<input type="text" class="form-control" name="search" value="<?php echo $search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
													<?php } ?>
														<select name="category_id" class="form-control">
															<option value="0"><?php echo $text_category; ?></option>
															<?php foreach ($categories as $category_1) { ?>
															<?php if ($category_1['category_id'] == $category_id) { ?>
															<option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
															<?php } else { ?>
															<option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
															<?php } ?>
															<?php foreach ($category_1['children'] as $category_2) { ?>
															<?php if ($category_2['category_id'] == $category_id) { ?>
															<option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
															<?php } else { ?>
															<option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
															<?php } ?>
															<?php foreach ($category_2['children'] as $category_3) { ?>
															<?php if ($category_3['category_id'] == $category_id) { ?>
															<option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
															<?php } else { ?>
															<option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
															<?php } ?>
															<?php } ?>
															<?php } ?>
															<?php } ?>
														  </select>													
												</p>
												<?php if ($sub_category) { ?>
													<input type="checkbox" name="sub_category" value="1" id="sub_category" checked="checked" />
												<?php } else { ?>
													<input type="checkbox" name="sub_category" value="1" id="sub_category" />
												<?php } ?>
												<label for="sub_category"><?php echo $text_sub_category; ?></label>
												<?php if ($description) { ?>
													<input type="checkbox" name="description" value="1" id="description" checked="checked" />
												<?php } else { ?>
													<input type="checkbox" name="description" value="1" id="description" />
												<?php } ?>
												<label for="description"><?php echo $entry_description; ?></label>
												<div class="buttons">
													<div class="right">
														<input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
													</div>
												</div>
												<hr>
												<h2><?php echo $text_search; ?></h2>
												<?php if ($products) { ?>
													<div class="box-product-src clearfix">
														<?php foreach ($products as $product) { ?>							
															<div class="box">
																<a href="<?php echo $product['href']; ?>">
																	<div class="hover">
																		<div class="hover-content">
																			<h3><?php echo $product['name']; ?></h3>
																			<p><?php echo $product['description']; ?></p>
																			<?php if (false!=$product['price']) { ?>
																			<div class="link">
																				<div class="price btn btn-block btn-primary">
																				<?php echo $product['price']; ?>
																				</div>
																			</div>
																			<?php } ?>
																		</div>
																		<img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
																	</div>
																</a>
															</div>
														<?php /*if ($product['thumb']) { ?>
														<div class="image">
															<a href="<?php echo $product['href']; ?>">
																<img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
															</a>
														</div>
														<?php } ?>
														<div class="name">
															<a href="<?php echo $product['href']; ?>">
																<?php echo $product['name']; ?>
															</a>
														</div>
														<div class="description">
															<?php echo $product['description']; ?>
														</div>
														<?php if ($product['price']) { ?>
														<div class="price">
															<?php if (!$product['special']) { ?>
															<?php echo $product['price']; ?>
															<?php } else { ?>
															<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
															<?php } ?>
															<?php if ($product['tax']) { ?>
															<br />
															<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
															<?php } ?>
														</div>
														<?php } 
														<?php if ($product['rating']) { ?>
															<div class="rating">
																<img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />
															</div>
														<?php } ?>
														<div class="cart">
															<input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
														</div>
														*/?>														
														<?php } ?>
													</div>
												<div class="pagination">
													<?php echo $pagination; ?>
												</div>
												<?php } else { ?>
												<div class="content">
													<?php echo $text_empty; ?>
												</div>
												<?php }?>
												</div>
												</div>
											</div>
										</div>
											
											
		<div class="container cl">
			<div class="row">
				<div class="tab-bg">
					<ul class="nav nav-tabs">
						<li class="<?php if (!isset($_GET['page'])) { echo "active"; }?>">
							<a href="#overview" data-toggle="tab">
								SEARCH
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="overview">
							<div class="row">
								<div class="col-md-12">
									<div class="tab-content sec overview">
									</div>
								</div>
							</div>										 
						</div>
					</div>
				</div>                                  
			</div>
		</div>
	</section>