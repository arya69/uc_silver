<?php 
	echo $header;
?>
	<section>
		<div class="clearfix container bn">
			<!--div class="clearfix col-md-12"-->
				<!--div class="row"-->
					<div id="carousel-example-generic" class="carousel slide carousel-fade">
						<!-- <div class="carousel-inner"> -->
							<!--div class="item active"-->
								<img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/collection-banner.jpg">
							<!--/div-->
						<!-- </div> -->
					</div>
				<!--/div-->
			<!--/div-->
		</div>
		<div class="container cl">
			<div class="row">
				<div class="tab-bg">
					<div class="">
						<ul class="nav nav-tabs">
						  <li class="<?php if (!isset($_GET['page'])) { echo "active"; }?>"><a href="#overview" data-toggle="tab">OVERVIEW</a></li>
						  <li class="<?php if (isset($_GET['page'])) { echo "active"; }?>" ><a href="#catalogue" data-toggle="tab">CATALOGUE</a></li>
						</ul>
						<div class="tab-content">
						  <div class="tab-pane <?php if (!isset($_GET['page'])) { echo "active"; }?>" id="overview">
							  <div class="row">
								<div class="col-md-12">
									<ul class="ct-nav" id="myTab">
										<?php 
										if (!empty($overview_data_categories)){
										foreach ($overview_data_categories as $category) { 
										?>
											<li>
												<input type="button" value="<?php echo $category['name']; ?>" id="button-cat" class="btn <?php echo "overvw".$category['category_id']; ?> ovr" onclick="content_overview('<?php echo $category['category_id']; ?>');" />
													<?php if (!empty($category['children'])) { ?>
														<ul>
															<?php foreach ($category['children'] as $child) { ?>
															<li>
																<input type="button" value="<?php echo $child['name']; ?>" id="button-cat" class="btn btn-active" onclick="content_overview('<?php echo $child['category_id']; ?>');" />
															</li>
															<?php } ?>
														</ul>
													<?php } ?>
											 </li>
										<?php
										}
										}
										?>
									</ul>
									<div class="tab-content sec overview">
										<div class="tab-pane active">
											<h3 class="ovr-name"></h3>
											<div class="row">
												<div class="col-md-12">
													<div class="ovr-description">
													</div>
												</div>
											</div>
											<hr>
											<div class="row">
												<div class="col-md-12">
													<a href="" id="ovr-link">Product List</a>
												</div>
											</div>
										</div>
									</div>
								</div>										 
							  </div>
						  </div>
						  <div class="tab-pane <?php if (isset($_GET['page'])) { echo "active"; }?>" id="catalogue">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-4">
											<?php echo $column_left; ?>
										</div>
										<div class="col-md-8">
											<?php echo $column_right; ?>
										</div>
									</div>
									<?php //echo $column_left; ?>
									<div class="tab-content sec">
									  <div class="tab-pane active" id="ring">
										<div class="ctp">
											<div class="box-product clearfix">
											</div>
										</div>
										<div id="pagin" class="fagination">
											<?php echo $pagination; ?>
										</div>                                                  
									  </div>
									</div>
								</div>
							</div>
						  </div>
						</div>
					</div>                                  
				</div>
			</div>
		</div>
	</section>
<?php echo $footer; ?>