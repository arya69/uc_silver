<?php echo $header; ?>
<section>
	<div class="container dtl">
		<div class="code-bar clearfix">
			<div class="row">
				<div class="col-md-3">
					<div class="cod">
						<?php echo $heading_title; ?>
					</div>
				</div>
				<div class="col-md-9 detail-p">
					<?php echo $column_left; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container dtl product">
		<div class="content-detail clearfix col-md-12">
			<div class="row">
				<div class="col-md-12">
					<p>
						<?php echo $cat_title; ?>
						|
						<?php echo $heading_title; ?></p>
				</div>
			</div>
			<div class="row" style="position: relative; margin-right: 15px;">
				<?php if ($thumb) { ?>
					<div class="col-md-6">
						<div class="dt-img">
							<img src="<?php echo $thumb; ?>">
							<a href="<?php echo $popup; ?>"></a>
						</div>
					</div>
				<?php } ?>
				<div class="row">
					<div class="col-md-6" style="height: 389px; position: relative;">
						<div class="col-md-12">
							<div class="co-p">
								<?php echo $model_title; ?>
							</div>
						</div>
						<div class="col-md-12">
							<?php echo $description; ?>
						</div>
						<?php if (!empty($products)) { ?>
							<!-- Edit By RYUKYMARU -->
							<div class="col-md-12" >
								<div class="row">
									<div class="options">
							      <div class="group">
							        <?php foreach ($options as $option) { ?>
												<?php //var_dump($option); ?>
							        <?php if ($option['type'] == 'select') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>" class="option opt-select">
							          <?php if ($option['required']) { ?>
							          <!--span class="required">*</span-->
							          <?php } ?> <!--b><?php echo $option['name']; ?>:</b-->
							          <div class="form-group">
											    	<label class="col-sm-2 control-label">* <?php echo $option['name']; ?> : </label>
											    	<div class="col-sm-4">
											      <select class="form-control" name="option[<?php echo $option['product_option_id']; ?>]">
							            	<option value=""><?php echo $text_select; ?></option>
							            <?php $arr_name[] = array(
							                      'name_size' =>$option['name'],
							                      'name_key' => $option['product_option_id']
							                    );
							              ?>
							            <?php foreach ($option['option_value'] as $option_value) { ?>
							            <?php
							                $arr_value[] = array(
							                      'key'      =>
							            $option['product_option_id'],
							                      'key_id'   => $option_value['product_option_value_id'],
							                      'key_name' => $option_value['name']
							                );
							              ?>
							            <option value="<?php echo $option_value['product_option_value_id']; ?>
							              ">
							              <?php echo $option_value['name']; ?>
							              <?php if ($option_value['price']) { ?>
							              (
							              <?php echo $option_value['price']; ?>
							              )
							              <?php } ?></option>

							            <?php } ?></select>
							            </div>
											  </div>
							        </div>
							        <?php
							          }
							          ?>

							        <?php if ($option['type'] == 'radio') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?> <b><?php echo $option['name']; ?>:</b>
							          <br />
							          <?php foreach ($option['option_value'] as $option_value) { ?>
							          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>
							          ]" value="
							          <?php echo $option_value['product_option_value_id']; ?>
							          " id="option-value-
							          <?php echo $option_value['product_option_value_id']; ?>
							          " />
							          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>
							            ">
							            <?php echo $option_value['name']; ?>
							            <?php if ($option_value['price']) { ?>
							            (
							            <?php echo $option_value['price_prefix']; ?>
							            <?php echo $option_value['price']; ?>
							            )
							            <?php } ?></label>
							          <br />
							          <?php } ?></div>
							        <br />

							        <?php } ?>
							        <?php if ($option['type'] == 'checkbox') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <?php foreach ($option['option_value'] as $option_value) { ?>
							          <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>
							          ][]" value="
							          <?php echo $option_value['product_option_value_id']; ?>
							          " id="option-value-
							          <?php echo $option_value['product_option_value_id']; ?>
							          " />
							          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>
							            ">
							            <?php echo $option_value['name']; ?>
							            <?php if ($option_value['price']) { ?>
							            (
							            <?php echo $option_value['price_prefix']; ?>
							            <?php echo $option_value['price']; ?>
							            )
							            <?php } ?></label>
							          <br />
							          <?php } ?></div>
							        <br />
							        <?php } ?>
							        <?php if ($option['type'] == 'image') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <table class="option-image">
							            <?php foreach ($option['option_value'] as $option_value) { ?>
							            <tr>
							              <td style="width: 1px;">
							                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>
							                ]" value="
							                <?php echo $option_value['product_option_value_id']; ?>
							                " id="option-value-
							                <?php echo $option_value['product_option_value_id']; ?>" /></td>
							              <td>
							                <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>
							                  ">
							                  <img src="<?php echo $option_value['image']; ?>
							                  " alt="
							                  <?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label>
							              </td>
							              <td>
							                <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>
							                  ">
							                  <?php echo $option_value['name']; ?>
							                  <?php if ($option_value['price']) { ?>
							                  (
							                  <?php echo $option_value['price_prefix']; ?>
							                  <?php echo $option_value['price']; ?>
							                  )
							                  <?php } ?></label>
							              </td>
							            </tr>
							            <?php } ?></table>
							        </div>
							        <br />
							        <?php } ?>
							        <?php if ($option['type'] == 'text') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <input type="text" name="option[<?php echo $option['product_option_id']; ?>
							          ]" value="
							          <?php echo $option['option_value']; ?>" /></div>
							        <br />
							        <?php } ?>
							        <?php if ($option['type'] == 'textarea') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <textarea name="option[<?php echo $option['product_option_id']; ?>
							            ]" cols="40" rows="5">
							            <?php echo $option['option_value']; ?></textarea>
							        </div>
							        <br />
							        <?php } ?>
							        <?php if ($option['type'] == 'file') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <input type="button" value="<?php echo $button_upload; ?>
							          " id="button-option-
							          <?php echo $option['product_option_id']; ?>
							          " class="button">
							          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" /></div>
							        <br />
							        <?php } ?>
							        <?php if ($option['type'] == 'date') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <input type="text" name="option[<?php echo $option['product_option_id']; ?>
							          ]" value="
							          <?php echo $option['option_value']; ?>" class="date" /></div>
							        <br />
							        <?php } ?>
							        <?php if ($option['type'] == 'datetime') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <input type="text" name="option[<?php echo $option['product_option_id']; ?>
							          ]" value="
							          <?php echo $option['option_value']; ?>" class="datetime" /></div>
							        <br />
							        <?php } ?>
							        <?php if ($option['type'] == 'time') { ?>
							        <div id="option-<?php echo $option['product_option_id']; ?>
							          " class="option">
							          <?php if ($option['required']) { ?>
							          <span class="required">*</span>
							          <?php } ?>
							          <b>
							            <?php echo $option['name']; ?>:</b>
							          <br />
							          <input type="text" name="option[<?php echo $option['product_option_id']; ?>
							          ]" value="
							          <?php echo $option['option_value']; ?>" class="time" /></div>
							        <br />
							        <?php } ?>

							        <?php } ?></div>
							    </div>
							    <br clear="all"><br clear="all">
							    <div class="form-group">
								    <label class="col-sm-2 control-label">Qty : </label>
								    <div class="col-sm-2">
										<input type="text" class="form-control" id="quantity" name="quantity" size="5" <?php if ($options != false) { ?>value="<?php echo $minimum; ?>"<?php } ?> >
										<input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />										
									</div>
									<input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="btn btn-primary" onclick="addToCart('<?php echo $product_id; ?>');" />
								</div>
								  <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
								  <!--<br clear="all"><br clear="all">
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="btn btn-primary" onclick="addToCart('<?php echo $product_id; ?>');" />
								    </div>
								  </div>-->
								</div>
							</div>
							<?php
			          /* Ryukymaru */
			          //print_r($arr_name);
			          if ($options != false) {
			            $sumArray = array();
			            $id_arr = 0;
			              foreach ($arr_name as $ky=>$sub_name) {
			                foreach ($arr_value as $id=>$value_value) {
			                  if ($sub_name['name_key'] == $value_value['key']) {
								$anak_name_sumArray                   = $sub_name['name_size'];
								$anak_id_sumArray                     = $sub_name['name_key'];
								$anak_sumArray[$id_arr]['id_value']   = $value_value['key_id'];
								$anak_sumArray[$id_arr]['id_master']  = $value_value['key'];
								$anak_sumArray[$id_arr]['data_value'] = $value_value['key_name'];
								$sumArray[$ky]['id']                  = $anak_id_sumArray;
								$sumArray[$ky]['name']                = $anak_name_sumArray;
								$sumArray[$ky]['value']               = $anak_sumArray;
								$id_arr                               = $id_arr + $id;
			                  }
			                }
			              }
			          }
			          //print_r($sumArray);
			          ?>
							<!-- END EDIT BY RYUKYMARU -->
							<div class="col-md-12" style="position: absolute; bottom: 0px;">
								<p class="ww">you may also see</p>
								<div class="slide-content clearfix">
									<div id="container">
										<div id="carousel">
											<?php 
											foreach ($products as $product) {
												if ($product['thumb']) {
											?>
												<a href="<?php echo $product['href']; ?>">
													<img src="<?php echo $product['thumb']; ?>"  />
												</a>
											<?php 
												}
											}
											?>
										</div>
										<a href="#" id="ui-carousel-next">
											<span>next</span>
										</a>
										<a href="#" id="ui-carousel-prev">
											<span>prev</span>
										</a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<link type="text/css" rel="stylesheet" href="catalog/view/theme/ucsilver/stylesheet/css/widget/css/rcarousel.css" />
<script type="text/javascript" src="catalog/view/theme/ucsilver/stylesheet/css/widget/lib/jquery.ui.core.js"></script>
<script type="text/javascript" src="catalog/view/theme/ucsilver/stylesheet/css/widget/lib/jquery.ui.widget.js"></script>
<script type="text/javascript" src="catalog/view/theme/ucsilver/stylesheet/css/widget/lib/jquery.ui.rcarousel.js"></script>
<script type="text/javascript">
	jQuery(function( $ ) {
		$( "#carousel" ).rcarousel({
			auto: {
				enabled: true,
				interval: 3000,
				direction: "prev"
			}
		});
	});
</script>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script> 
<script type="text/javascript"><!--

$('select[name="profile_id"], input[name="quantity"]').change(function(){
    $.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name="product_id"], input[name="quantity"], select[name="profile_id"]'),
		dataType: 'json',
        beforeSend: function() {
            $('#profile-description').html('');
        },
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
            
			if (json['success']) {
                $('#profile-description').html(json['success']);
			}	
		}
	});
});
    
$('#button-cart').bind('click', function() {
	//console.log($('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'));
	//return false;
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
                
                if (json['error']['profile']) {
                    $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
                }
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<!-- <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>  -->
<script type="text/javascript"><!--
// $(document).ready(function() {
// 	if ($.browser.msie && $.browser.version == 6) {
// 		$('.date, .datetime, .time').bgIframe();
// 	}

// 	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
// 	$('.datetime').datetimepicker({
// 		dateFormat: 'yy-mm-dd',
// 		timeFormat: 'h:m'
// 	});
// 	$('.time').timepicker({timeFormat: 'h:m'});
// });
//--></script>
<?php echo $footer; ?>