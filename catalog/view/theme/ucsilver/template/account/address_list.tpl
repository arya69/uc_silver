<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
    <div class="row">
        <div class="tab-bg">
        <div class="col-md-12">
          <div class="row">
            <ul class="nav nav-tabs">
              <li class="active">
                  <a href="#boutique" data-toggle="tab">
                <?php echo $heading_title; ?></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="boutique">
              <div class="row">
                <div class="col-md-12">
                <div class="col-md-3">
                  <?php echo $column_left; ?>
                  <?php //echo $column_right; ?></div>
                <div class="col-md-9">
        					<!-- register form -->
        					<h2><?php echo $text_address_book; ?></h2>
        					<?php foreach ($addresses as $result) { ?>
        					<div class="content">
        						<table style="width: 100%;" class="table table-striped">
        						  <tr>
        							<td><?php echo $result['address']; ?></td>
        							<td style="text-align: right;"><a href="<?php echo $result['update']; ?>" class="btn btn-primary"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-primary"><?php echo $button_delete; ?></a></td>
        						  </tr>
        						</table>
        					</div>
        					<?php } ?>
        					<a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
        					<a href="<?php echo $insert; ?>" class="btn btn-primary"><?php echo $button_new_address; ?></a>
                  <br clear="all"><br clear="all"><br clear="all">
        					<!--end register form --> 
        				</div>
              </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>