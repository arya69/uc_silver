<?php echo $header; ?>
<section>
  <div class="container bn">
    <!--div class="row"-->
      <?php if ($success) { ?>
      <div class="alert alert-success"><?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><?php echo $error_warning; ?></div>
      <?php } ?>
      <!-- <div class="container bn register"> -->
        <!-- <div class="row"> -->
          <!--div class="col-md-12"-->
            <div id="carousel-example-generic" class="carousel slide carousel-fade">
              <!--div class="carousel-inner">
                <div class="item active"-->
                  <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
                <!--/div>
              </div-->
            </div>
          <!--/div-->
        <!-- </div> -->
      <!-- </div> -->
    <!--/div-->
  </div>
  <div class="container cl">
    <div class="row">
      <div class="tab-bg">
        <div class="col-md-12"><?php //echo $content_top; ?>
          <div class="row">
            <ul class="nav nav-tabs">
              <li class="active">
                <a data-toggle="tab" href="#overview"><?php echo $heading_title; ?></a>
              </li>
            </ul>
            <div class="tab-content">
              <!--div class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                <?php } ?>
              </div-->
              <!--h1></h1-->
              <div id="overview" class="login-content tab-pane active">
                <div class="row">
                  <div class="col-md-12">
                    <!-- <div class="row"> -->
                      <div class="col-md-3">
                        <?php echo $column_left; ?>
                        <?php //echo $column_right; ?>
                      </div>
                      <div class="col-md-5">
                        <h2><?php echo $text_new_customer; ?></h2>
                        <div class="content">
                          <p><b><?php echo $text_register; ?></b></p>
                          <p><?php echo $text_register_account; ?></p>
                          <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <h2><?php echo $text_returning_customer; ?></h2>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                          <div class="content">
                            <!--p><?php echo $text_i_am_returning_customer; ?></p-->
                            <div class="form-group">
                              <label><?php echo $entry_email; ?></label>
                              <input class="form-control" type="text" name="email" value="<?php echo $email; ?>" />
                            </div>
                            <div class="form-group">
                              <label><?php echo $entry_password; ?></label>
                              <input class="form-control" type="password" name="password" value="<?php echo $password; ?>" />
                            </div>
                            <a href="<?php echo $forgotten; ?>" ><?php echo $text_forgotten; ?></a>
                            <br /><br />
                            <input class="btn btn-primary" type="submit" value="<?php echo $button_login; ?>" class="button" />
                            <?php if ($redirect) { ?>
                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                            <?php } ?>
                          </div>
                        </form>
                      </div>
                    <!-- </div> -->
                  </div>
                </div>
              </div>
              <?php echo $content_bottom; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#login').submit();
	}
});
//--></script> 
<?php echo $footer; ?>