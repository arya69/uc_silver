<?php echo $header; ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
    <div class="row">
        <div class="tab-bg">
        <div class="col-md-12">
          <div class="row">
            <ul class="nav nav-tabs">
              <li class="active">
                  <a href="#boutique" data-toggle="tab">
                <?php echo $heading_title; ?></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="boutique">
              <div class="row">
                <div class="col-md-12">
                <div class="col-md-3">
                  <?php echo $column_left; ?>
                  <?php //echo $column_right; ?></div>
                <div class="col-md-9">
                  <!-- register form -->
      					  <h1><?php echo $heading_title; ?></h1>
      					  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      						<div class="content">
      						  <table class="form">
      							<tr>
      							  <td><?php echo $entry_newsletter; ?></td>
      							  <td><?php if ($newsletter) { ?>
      								<input type="radio" name="newsletter" value="1" checked="checked" />
      								<?php echo $text_yes; ?>&nbsp;
      								<input type="radio" name="newsletter" value="0" />
      								<?php echo $text_no; ?>
      								<?php } else { ?>
      								<input type="radio" name="newsletter" value="1" />
      								<?php echo $text_yes; ?>&nbsp;
      								<input type="radio" name="newsletter" value="0" checked="checked" />
      								<?php echo $text_no; ?>
      								<?php } ?></td>
      							</tr>
      						  </table>
      						</div>
      						<a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
      						<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                  <br clear="all"><br clear="all"><br clear="all">
      					  </form>
                        <!--end register form --> 
      				</div>
            </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>
<?php /* echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="content">
      <table class="form">
        <tr>
          <td><?php echo $entry_newsletter; ?></td>
          <td><?php if ($newsletter) { ?>
            <input type="radio" name="newsletter" value="1" checked="checked" />
            <?php echo $text_yes; ?>&nbsp;
            <input type="radio" name="newsletter" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="newsletter" value="1" />
            <?php echo $text_yes; ?>&nbsp;
            <input type="radio" name="newsletter" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a></div>
      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" /></div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; */ ?>