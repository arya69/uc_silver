<?php echo $header; ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
		<div class="row">
	  		<div class="tab-bg">
				<div class="col-md-12">
					<div class="row">
						<ul class="nav nav-tabs">
							<li class="active">
			  					<a href="#boutique" data-toggle="tab">
				                <?php echo $heading_title; ?></a>
				            </li>
				          </ul>
				          <div class="tab-content">
				            <div class="tab-pane active" id="boutique">
				              <div class="row">
				              	<div class="col-md-12">
					                <div class="col-md-3">
					                  <?php echo $column_left; ?>
					                  <?php //echo $column_right; ?>
									</div>
					                <div class="col-md-9">
					                  <!-- register form -->					
										<h1><?php echo $heading_title; ?></h1>
											<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
											<h2><?php echo $text_password; ?></h2>
											<div class="content">
												<div class="form-group">
													<label for="inputPassword" class="col-sm-3 control-label">* <?php echo $entry_password; ?></label>
													<div class="col-sm-5">
														<input  class="form-control" type="password" name="password" value="<?php echo $password; ?>" />
														<?php if ($error_password) { ?>
														<span class="error"><?php echo $error_password; ?></span>
														<?php } ?>
													</div>
												</div>
												<br clear="all"><br clear="all">
												
												<div class="form-group">
													<label for="inputPassword" class="col-sm-3 control-label">* <?php echo $entry_confirm; ?></label>
													<div class="col-sm-5">
														<input  class="form-control" type="password" name="confirm" value="<?php echo $confirm; ?>" />
														<?php if ($error_confirm) { ?>
														<span class="error"><?php echo $error_confirm; ?></span>
														<?php } ?>
													</div>
												</div>
												<br clear="all"><br clear="all">
											</div>
											<a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
											<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
											<br clear="all"><br clear="all">
											</form>					
					                  <!--end register form --> 
									</div>
								</div>
				              </div>
				            </div>
				          </div>
			      	</div>
		        </div>
		      </div>
		    </div>
		  </div>
</section>
<?php echo $footer; ?>