<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning">
  <?php echo $error_warning; ?></div>
<?php } ?>
<section>
	<div class="container bn">
		<!--div class="row">
		  <div class="col-md-12"-->
			<div id="carousel-example-generic" class="carousel slide carousel-fade">
			  <!--div class="carousel-inner">
				<div class="item active"-->
				  <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg"><!--/div>
			  </div-->
			</div>
		  <!--/div>
		</div-->
	</div>
	<div class="container cl">
		<div class="row">
	  		<div class="tab-bg">
				<div class="col-md-12">
					<div class="row">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#boutique" data-toggle="tab"><?php echo $heading_title; ?></a>
							</li>
						</ul>
						<div class="tab-content">
							<div id="overview" class="login-content tab-pane active">
				  				<div class="row">
				  					<div class="col-md-12">
										<div class="col-md-3"><?php echo $column_left; ?></div>
										<div class="col-md-9">
						  					<!-- register form -->
											<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
												<p><?php echo $text_email; ?></p>
												<h2><?php echo $text_your_email; ?></h2>
												<div class="content">
													<div class="form-group">
														<label for="inputPassword" class="col-sm-3 control-label"><?php echo $entry_email; ?></label>
														<div class="col-sm-5">
															<input class="form-control" type="text" name="email" value="" />
														</div>
													</div>
												</div>
												<br clear="all">
												<br clear="all">
												<a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
												<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
												<br clear="all">
												<br clear="all">
											</form>
						  					<!--end register form --> 
										</div>
									</div>
				  				</div>
							</div>
			  			</div>
		  			</div>
				</div>
	  		</div>
		</div>
	</div>
</section>
<?php echo $footer; ?>