<?php echo $header; ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
    <div class="row">
        <div class="tab-bg">
        <div class="col-md-12">
          <div class="row">
            <ul class="nav nav-tabs">
              <li class="active">
                  <a href="#boutique" data-toggle="tab">
                <?php echo $heading_title; ?></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="boutique">
              <div class="row">
                <div class="col-md-12">
                <div class="col-md-3">
                  <?php echo $column_left; ?>
                  <?php //echo $column_right; ?></div>
                <div class="col-md-9">
                  <!-- register form -->
      					  <h1><?php echo $heading_title; ?></h1>
      					  <?php /*
      					  <p><?php echo $text_total; ?><b> <?php echo $total; ?></b>.</p>
      					  */ ?>
      					  <table class="table table-striped list">
      						<thead>
      						  <tr>
      							<td class="left"><?php echo $column_date_added; ?></td>
      							<td class="left"><?php echo $column_description; ?></td>
      							<td class="right"><?php echo $column_amount; ?></td>
      						  </tr>
      						</thead>
      						<tbody>
      						  <?php if ($transactions) { ?>
      						  <?php foreach ($transactions  as $transaction) { ?>
      						  <tr>
      							<td class="left"><?php echo $transaction['date_added']; ?></td>
      							<td class="left"><?php echo $transaction['description']; ?></td>
      							<td class="right"><?php echo $transaction['amount']; ?></td>
      						  </tr>
      						  <?php } ?>
      						  <?php } else { ?>
      						  <tr>
      							<td class="center" colspan="5"><?php echo $text_empty; ?></td>
      						  </tr>
      						  <?php } ?>
      						</tbody>
      					  </table>
      					  <div class="pagination"><?php echo $pagination; ?></div>
      					  <div class="buttons">
      						<div class="right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      					  </div>
                  <br clear="all"><br clear="all"><br clear="all">
                        <!--end register form --> 
      				  </div>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>
<?php /*echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <p><?php echo $text_total; ?><b> <?php echo $total; ?></b>.</p>
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $column_date_added; ?></td>
        <td class="left"><?php echo $column_description; ?></td>
        <td class="right"><?php echo $column_amount; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php if ($transactions) { ?>
      <?php foreach ($transactions  as $transaction) { ?>
      <tr>
        <td class="left"><?php echo $transaction['date_added']; ?></td>
        <td class="left"><?php echo $transaction['description']; ?></td>
        <td class="right"><?php echo $transaction['amount']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan="5"><?php echo $text_empty; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <div class="pagination"><?php echo $pagination; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; */ ?>