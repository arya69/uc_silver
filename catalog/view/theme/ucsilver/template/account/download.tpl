<?php echo $header; ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
    <div class="row">
      <div class="tab-bg">
        <div class"col-md-12">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#boutique" data-toggle="tab">
                <?php echo $heading_title; ?></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="boutique">
              <div class="row">
                <div class="col-md-3">
                  <?php echo $column_left; ?>
                  <?php //echo $column_right; ?></div>
                <div class="col-md-9">
                  <!-- register form -->
					  <h1><?php echo $heading_title; ?></h1>
					  <?php foreach ($downloads as $download) { ?>
					  <div class="download-list">
						<div class="download-id"><b><?php echo $text_order; ?></b> <?php echo $download['order_id']; ?></div>
						<div class="download-status"><b><?php echo $text_size; ?></b> <?php echo $download['size']; ?></div>
						<div class="download-content">
						  <div><b><?php echo $text_name; ?></b> <?php echo $download['name']; ?><br />
							<b><?php echo $text_date_added; ?></b> <?php echo $download['date_added']; ?></div>
						  <div><b><?php echo $text_remaining; ?></b> <?php echo $download['remaining']; ?></div>
						  <div class="download-info">
							<?php if ($download['remaining'] > 0) { ?>
							<a href="<?php echo $download['href']; ?>"><img src="catalog/view/theme/default/image/download.png" alt="<?php echo $button_download; ?>" title="<?php echo $button_download; ?>" /></a>
							<?php } ?>
						  </div>
						</div>
					  </div>
					  <?php } ?>
					  <div class="pagination"><?php echo $pagination; ?></div>
					  <div class="buttons">
						<div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
					  </div>
                  <!--end register form --> 
				</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php echo $footer; ?>
</section>