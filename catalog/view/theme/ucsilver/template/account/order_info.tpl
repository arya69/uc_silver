<?php echo $header; ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
    <div class="row">
      <div class="tab-bg">
        <div class"col-md-12">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#boutique" data-toggle="tab">
                <?php echo $heading_title; ?></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="boutique">
              <div class="row">
                <div class="col-md-3">
                  <?php echo $column_left; ?>
				</div>
                <div class="col-md-9">
					<!-- register form -->					
					  <h1><?php echo $heading_title; ?></h1>
					  <table class="table table-striped">
						<col width="20%">
						<col width="auto">
						<thead>
						  <tr>
							<td class="left" colspan="2"><?php echo $text_order_detail; ?></td>
						  </tr>
						</thead>
						<tbody>
							<tr>
								<?php if ($invoice_no) { ?>
								<td>
									<b><?php echo $text_invoice_no; ?></b>
								</td>
								<td>
									<?php echo $invoice_no; ?><br />
								</td>
								<?php } ?>
								<td>
									<b><?php echo $text_order_id; ?></b>
								</td>
								<td>
									#<?php echo $order_id; ?><br />
								</td>
							</tr>
							<tr>
								<td>
									<b><?php echo $text_date_added; ?></b>
								</td>
								<td>
									<?php echo $date_added; ?>
								</td>
							</tr>
							<tr>
								<?php if ($payment_method) { ?>
								<td>
									<b><?php echo $text_payment_method; ?></b>
								</td>
								<td>
									<?php echo $payment_method; ?><br />
								</td>
								<?php } ?>								
							</tr>
							<?php
							if ($shipping_method) {
							foreach (explode('|',$shipping_method) as $metod)
							{
								list ($key, $value) = explode(':', $metod);
							?>
								<tr>
									<td>
										<b><?php echo $key." : "; ?></b>
									</td>
									<td>
										<?php echo $value; ?>
									</td>
								</tr>
							<?php }	}?>
					  </tbody>
					  </table>
					  <table class="table table-striped list">
						<thead>
						  <tr>
							<td class="left"><?php echo $text_payment_address; ?></td>
							<?php if ($shipping_address) { ?>
							<td class="left"><?php echo $text_shipping_address; ?></td>
							<?php } ?>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td class="left"><?php echo $payment_address; ?></td>
							<?php if ($shipping_address) { ?>
							<td class="left"><?php echo $shipping_address; ?></td>
							<?php } ?>
						  </tr>
						</tbody>
					  </table>
					  <table class="table table-striped list">
						<thead>
						  <tr>
							<td class="left"><?php echo $column_name; ?></td>
							<td class="left"><?php echo $column_model; ?></td>
							<td class="right"><?php echo $column_quantity; ?></td>
							<?php /*
							<td class="right"><?php echo $column_price; ?></td>
							<td class="right"><?php echo $column_total; ?></td>
							*/ ?>
							<?php if ($products) { ?>
							<td style="width: 1px;"></td>
							<?php } ?>
						  </tr>
						</thead>
						<tbody>
						  <?php foreach ($products as $product) { ?>
						  <tr>
							<td class="left"><?php echo $product['name']; ?>
							  <?php foreach ($product['option'] as $option) { ?>
							  <br />
							  &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
							  <?php } ?></td>
							<td class="left"><?php echo $product['model']; ?></td>
							<td class="right"><?php echo $product['quantity']; ?></td>
							<?php /*
							<td class="right"><?php echo $product['price']; ?></td>
							<td class="right"><?php echo $product['total']; ?></td>
							*/ ?>
							<td class="right"><a href="<?php echo $product['return']; ?>"><img src="catalog/view/theme/default/image/return.png" alt="<?php echo $button_return; ?>" title="<?php echo $button_return; ?>" /></a></td>
						  </tr>
						  <?php } ?>
						  <?php /* foreach ($vouchers as $voucher) { ?>
						  <tr>
							<td class="left"><?php echo $voucher['description']; ?></td>
							<td class="left"></td>
							<td class="right">1</td>
							<td class="right"><?php echo $voucher['amount']; ?></td>
							<td class="right"><?php echo $voucher['amount']; ?></td>
							<?php if ($products) { ?>
							<td></td>
							<?php } ?>
						  </tr>
						  <?php } */ ?>
						</tbody>
						<?php /*
						<tfoot>
						  <?php foreach ($totals as $total) { ?>
						  <tr>
							<td colspan="3"></td>
							<td class="right"><b><?php echo $total['title']; ?>:</b></td>
							<td class="right"><?php echo $total['text']; ?></td>
							<?php if ($products) { ?>
							<td></td>
							<?php } ?>
						  </tr>
						  <?php } ?>
						</tfoot>
						*/ ?>
					  </table>
					  <?php if ($comment) { ?>
					  <table class="table table-striped list">
						<thead>
						  <tr>
							<td class="left"><?php echo $text_comment; ?></td>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td class="left"><?php echo $comment; ?></td>
						  </tr>
						</tbody>
					  </table>
					  <?php } ?>
					  <?php if ($histories) { ?>
					  <h2><?php echo $text_history; ?></h2>
					  <table class="table table-striped list">
						<thead>
						  <tr>
							<td class="left"><?php echo $column_date_added; ?></td>
							<td class="left"><?php echo $column_status; ?></td>
							<td class="left"><?php echo $column_comment; ?></td>
						  </tr>
						</thead>
						<tbody>
						  <?php foreach ($histories as $history) { ?>
						  <tr>
							<td class="left"><?php echo $history['date_added']; ?></td>
							<td class="left"><?php echo $history['status']; ?></td>
							<td class="left"><?php echo $history['comment']; ?></td>
						  </tr>
						  <?php } ?>
						</tbody>
					  </table>
					  <?php } ?>
					  <div class="buttons">
						<div class="right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
					  </div>					
					<!--end register form --> 
				</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>