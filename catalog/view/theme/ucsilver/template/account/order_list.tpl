<?php echo $header; ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
		<div class="row">
	  		<div class="tab-bg">
				<div class="col-md-12">
					<div class="row">
						<ul class="nav nav-tabs">
							<li class="active">
			  					<a href="#boutique" data-toggle="tab">
                <?php echo $heading_title; ?></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="boutique">
              <div class="row">
              	<div class="col-md-12">
                <div class="col-md-3">
                  <?php echo $column_left; ?>
				</div>
                <div class="col-md-9">
                  <!-- register form -->				  
					<h1><?php echo $heading_title; ?></h1>
					<hr>
					<?php if ($orders) { ?>
					<?php foreach ($orders as $order) { ?>				
					<div class="row">
						<div class="order-list">
							<div class="order-id">
								<div class="col-md-4">
									<b><?php echo $text_order_id; ?></b> 
								</div>
								<div class="col-md-8">
									#<?php echo $order['order_id']; ?>
								</div>
							</div>
							<div class="order-status">
								<div class="col-md-4">
									<b><?php echo $text_status; ?></b>
								</div>
								<div class="col-md-8">
									<?php echo $order['status']; ?>
								</div>
							</div>
							<div class="order-content">
								<div>
									<div class="col-md-4">
										<b><?php echo $text_date_added; ?></b>
									</div>
									<div class="col-md-8">
										<?php echo $order['date_added']; ?><br />
									</div>
									<div class="col-md-4">
										<b><?php echo $text_products; ?></b>
									</div>
									<div class="col-md-8">
										<?php echo $order['products']; ?>
									</div>
								</div>
								<div>
									<div class="col-md-4">
										<b><?php echo $text_customer; ?></b>
									</div>
									<div class="col-md-8">
										<?php echo $order['name']; ?><br />
									</div>
									<?php /*
									<div class="col-md-4">
										<b><?php echo $text_total; ?></b>
									</div>
									<div class="col-md-8">
										<?php echo $order['total']; ?>
									</div>
									*/ ?>
								</div>
								<div class="order-info">
									<div class="col-md-4 col-md-offset-4">
										<a href="<?php echo $order['href']; ?>"><img src="catalog/view/theme/default/image/info.png" alt="<?php echo $button_view; ?>" title="<?php echo $button_view; ?>" /></a>&nbsp;&nbsp;
										<a href="<?php echo $order['reorder']; ?>"><img src="catalog/view/theme/default/image/reorder.png" alt="<?php echo $button_reorder; ?>" title="<?php echo $button_reorder; ?>" /></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<?php } ?>
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<div class="pagination">
								<?php echo $pagination; ?>
							</div>
						<?php } else { ?>
							<div class="content"><?php echo $text_empty; ?></div>
						<?php } ?>
						</div>
						<div class="col-md-4 col-md-offset-4">
							<a href="<?php echo $continue; ?>" class="btn btn-primary">
								<?php echo $button_continue; ?>
							</a>
						</div>
					</div>
					<br clear="all"><br clear="all"><br clear="all">
                  <!--end register form --> 
				</div>
				</div>
              </div>
            </div>
          </div>
        </div>
    	</div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>