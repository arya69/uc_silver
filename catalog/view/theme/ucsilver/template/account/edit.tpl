<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning">
  <?php echo $error_warning; ?></div>
<?php } ?>
<section>
  <div class="container bn">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
          <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
    </div>
  </div>
  <div class="container cl">
		<div class="row">
	  		<div class="tab-bg">
				<div class="col-md-12">
					<div class="row">
						<ul class="nav nav-tabs">
							<li class="active">
			  					<a href="#boutique" data-toggle="tab">
                					<?php echo $heading_title; ?></a>
            				</li>
          				</ul>
	          <div class="tab-content">
	            <div class="tab-pane active" id="boutique">
	              <div class="row">
	              	<div class="col-md-12">
		                <div class="col-md-3">
		                  <?php echo $column_left; ?>
		                  <?php //echo $column_right; ?>
						</div>
		                <div class="col-md-9">
		                  <!-- register form -->
							<h1><?php echo $heading_title; ?></h1>
								<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
								<h2><?php echo $text_your_details; ?></h2>
								<div class="content">
								
									<div class="form-group">
										<label for="inputPassword" class="col-sm-2 control-label">* <?php echo $entry_firstname; ?></label>
										<div class="col-sm-5">
											<input  class="form-control" type="text" name="firstname" value="<?php echo $firstname; ?>">
											<?php if ($error_firstname) { ?>
											<span class="error"><?php echo $error_firstname; ?></span>
											<?php } ?></td>
										</div>
									</div>
									<br clear="all"><br clear="all">
									
									<div class="form-group">
										<label for="inputPassword" class="col-sm-2 control-label">* <?php echo $entry_lastname; ?></label>
										<div class="col-sm-5">
											<input  class="form-control" type="text" name="lastname" value="<?php echo $lastname; ?>">
											<?php if ($error_lastname) { ?>
											<span class="error">
												<?php echo $error_lastname; ?></span>
											<?php } ?>
										</div>
									</div>
									<br clear="all"><br clear="all">
									
									<div class="form-group">
										<label for="inputPassword" class="col-sm-2 control-label">* <?php echo $entry_email; ?></label>
										<div class="col-sm-5">
											<input  class="form-control" type="text" name="email" value="<?php echo $email; ?>">
											<?php if ($error_email) { ?>
											<span class="error"><?php echo $error_email; ?></span>
											<?php } ?>
										</div>
									</div>
									<br clear="all"><br clear="all">
									
									<div class="form-group">
										<label for="inputPassword" class="col-sm-2 control-label">* <?php echo $entry_telephone; ?></label>
										<div class="col-sm-5">
											<input  class="form-control" type="text" name="telephone" value="<?php echo $telephone; ?>">
											<?php if ($error_telephone) { ?>
											<span class="error"><?php echo $error_telephone; ?></span>
											<?php } ?>
										</div>
									</div>
									<br clear="all"><br clear="all">
									
									<div class="form-group">
										<label for="inputPassword" class="col-sm-2 control-label"><?php echo $entry_fax; ?></label>
										<div class="col-sm-5">
											<input  class="form-control" type="text" name="fax" value="<?php echo $fax; ?>">
										</div>
									</div>
									<br clear="all"><br clear="all">
										
								</div>
								
								<a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
								<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
								<br clear="all"><br clear="all">
							  </form>
		                  <!--end register form --> 
						</div>
					</div>
	              </div>
	            </div>
	          </div>
	      </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>