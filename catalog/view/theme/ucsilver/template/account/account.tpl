<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<section>
  <div class="container bn">
	<!--div class="row">
	  <div class="col-md-12"-->
		<div id="carousel-example-generic" class="carousel slide carousel-fade">
		  <!--div class="carousel-inner">
			<div class="item active"-->
			  <img style="width: 97.6%;" src="catalog/view/theme/ucsilver/stylesheet/images/stores-bn.jpg">
			  <!--/div>
		  </div-->
		</div>
	  <!--/div>
	</div-->
  </div>
	<div class="container cl">
		<div class="row">
	  		<div class="tab-bg">
				<div class="col-md-12">
					<div class="row">
						<ul class="nav nav-tabs">
							<li class="active">
			  					<a href="#boutique" data-toggle="tab"><?php echo $heading_title; ?></a>
							</li>
		  				</ul>
			  			<div class="tab-content">
							<div class="tab-pane active" id="boutique">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-3">
						  					<?php echo $column_left; ?>
						  				</div>
										<div class="col-md-9">
											<h2><?php echo $text_my_account; ?></h2>
											<div class="content">
												<ul>
													<li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
													<li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
													<li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
													<?php /* <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li> */ ?>
												</ul>
											</div>
											<h2><?php echo $text_my_orders; ?></h2>
											<div class="content">
												<ul>
													<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
													<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
													<?php /* <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li> 
													<?php if ($reward) { ?>
													<li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
													<?php } ?>
													<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>             
													<li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li> */ ?>
												</ul>
											</div>
											<h2><?php echo $text_my_newsletter; ?></h2>
											<div class="content">
												<ul>
													<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
												</ul>
											</div>
											<br clear="all"><br clear="all">
				  						</div>
			  						</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php echo $footer; ?>