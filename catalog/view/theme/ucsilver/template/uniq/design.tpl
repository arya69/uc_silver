<?php echo $header; ?>
				<section>
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<ul class="cat-top">
										<li><a href="<?php echo $base."index.php?route=unikdt/uniqhal/stories"; ?>">Stories</a></li>
										<li class="active"><a href="<?php echo $base."index.php?route=unikdt/uniqhal/design"; ?>">Design</a></li>
										<li><a href="process.html">Process</a></li>
										<li><a href="support.html">Support</a></li>
									</ul>
								</div>
							<div class="col-md-12">
								<div id="owl-demo2" class="owl-carousel owl-theme">	
									<div class="item">
										<div class="row">
											<div class="col-md-5">
												<img src="catalog/view/theme/ucsilver/stylesheet/images/design-round.png">
												<span class="tl">
													Sweet Dragonfly
													<span class="reg">&reg;</span>
													<br>												
													<span  class="ct-i">18k Gold with Diamond Microsetting</span>
												</span>
											</div>
											<div class="col-md-7">
												<div class="txt-content">
												The buzzing and wheezing sound that a dragonfly makes as it passes by on the edge of the 
												yellowish rice fields in the harvest season inspired UC Silver to design our trademark pieces of 
												jewelry, “Sweet Dragonfly”. The beauty of its wings –as if made of soft transparent muslin, fluttering at the slightest hint of the wind– that stole our heart. Watching a dragonfly takes  flight from the 
												flowers by the river bed that it rests on, and zooms past into the air. People might just see it as one other species in nature, but for us, we see a lot more to the dragonfly than what meets the mere eye. There is something about the way it flies which expresses their hopes, dreams, needs and wishes. And above all, there is sense of freedom and change, and being okay with the change. In Native American cultures, the dragonfly spirit is the essence of the winds of change, the messages of wisdom and enlightenment; and the communication from the elemental world. In other words, it tells people to seek out the parts of their habits which need changing; to guide them through the mists of illusion to the pathway of transformation. <br><br>
												This is what triggered us to make dragonfly as our trademark. The“Sweet Dragonfly” that we created symbolizes a passion to live and to be meaningful to all living beings. We are eager to share our love to the nature, and to maintain the nature to be the way it is. Living in a 
												peaceful environment for the beauty of the universe is our way to run this life. As every life has its limit, a dragonfly has also a very short life. We believe in this short life we have to show the quality of life that we have and live the life to the fullest.Our “Sweet Dragonfly” is symbolic of how well we should lead our lives no matter how short our life is.
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-md-12">
												<ul class="round_img last">
													<li>
														<img src="catalog/view/theme/ucsilver/stylesheet/images/acs1.png"><br><br>
														<span>
															Sweet Dragonfly<span class="reg">&reg;</span><br>
														<span  class="ct-i">18k Gold with Diamond Microsetting</span>
														</span>
													</li>
													<li>
														<img src="catalog/view/theme/ucsilver/stylesheet/images/acs2.png">
														<span>
															Sweet Dragonfly<span class="reg">&reg;</span><br>
														<span  class="ct-i">925 sterling silver</span>
														</span>
													</li>
													<li>
														<img src="catalog/view/theme/ucsilver/stylesheet/images/acs3.png">
													</li>
												</ul>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</section>
		<script>
			    $(document).ready(function() {
			      $("#owl-demo2").owlCarousel({

			      navigation : true,
			      slideSpeed : 300,
			      paginationSpeed : 400,
			      singleItem : true,
			      autoHeight : true
			      //autoPlay: 5000
			      });
			    });
		</script>
<?php echo $footer; ?>