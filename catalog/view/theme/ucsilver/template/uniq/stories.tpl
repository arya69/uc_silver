<?php echo $header; ?>
<section>
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<ul class="cat-top">
										<li class="active"><a href="<?php echo $base."index.php?route=unikdt/uniqhal/stories"; ?>">Stories</a></li>
										<li><a href="<?php echo $base."index.php?route=unikdt/uniqhal/design"; ?>">Design</a></li>
										<li><a href="process.html">Process</a></li>
										<li><a href="support.html">Support</a></li>
									</ul>
							</div>
							<div class="col-md-12">
								<div id="owl-demo" class="owl-carousel owl-theme">	
									<div class="item">
										<div class="row">
											<div class="col-md-4">
												<img src="catalog/view/theme/ucsilver/stylesheet/images/stories1.png">
												<p class="title">i am unique <span class="reg">&reg;</span></p>
											</div>
											<div class="col-md-8">
												<div class="txt-content">
													In 1989, UC silver started as a small silver jewelry retail shop specializing in unique and unusual designs. Our name, “UC Silver”, stands for “Ubud Corner Silver”, Bali, Indonesia, where our first retail store was located. Our logo, the strong red circle surrounded by circle lines, represents the four Balinese brothers who created the legacy that is UC Silver. To this day, we are honored to welcome customers from around the world to experience true Balinese-style jewelry and expert craftsmanship with UC silver.
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-md-4">
												<img src="catalog/view/theme/ucsilver/stylesheet/images/stories2.png">
												<p class="title">i am unique <span class="reg">&reg;</span></p>
											</div>
											<div class="col-md-8">
												<div class="txt-content">
													<h2>Philosophy</h2>
													UC Silver splendour found it difficult at this time to believe when originally came from small roadside stalls Batubulan. at that time in 1998, Wayan began to pursue silver with 3 brothers, I Made Darmawan, I Nyoman eriawan, and I Ketut Sudiarsana. Wayan recognised that plunge into the world of the silver is not something that is planned. <br/><br/>

													Prior to his silver, Wayan brothers first work in the field of wood craft in 1985. All accessories that add to the appearance of a person made of wood albizia, such as earrings, bracelets, brooches, necklaces, rings, and so forth. Actually, wooden handicraft business in live Wayan running smoothly. However, since wood is a material that can not be recycled, wood crafts plus a difficult trend develops, practically Wayan only lasted for three years.
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-md-4">
												<img src="catalog/view/theme/ucsilver/stylesheet/images/wayan.png">
												<p class="name">I WAYAN SUTEDJA</p class="name">
												<p class="title">i am unique<span class="reg">&reg;</span></p>
											</div>
											<div class="col-md-8">
												<div class="txt-content">
													<h2>Philosophy</h2>
													“Arguably, we are totally blind to the art of silver craft. Our time only with the creativity and spirit of high art. Along with my younger siblings do not become discouraged. We design and create your own production house, and then we market at a small kiosk that we rent. Anyway all our own doing, ”recalls Wayan.<br><br>
													In 1989, Wayan can rent a simple stall in the region known as the Centre and handicrafts in Bali, the Ubud. Ubud step that the name of the UC born, the acronym of Ubud Corner because his shop is in a corner. The spirit and courage of the four brothers then little by little to fruition. 
													“To be like now, we never lose sight of the historical roots of the UC Silver is built through hard work. We always maintain cohesiveness and mutual understanding of each other. Principle we have an all for one, one for all,” he said.
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="row">
											<div class="col-md-12">
												<img src="catalog/view/theme/ucsilver/stylesheet/images/stories4.png" style="margin-left:10em;">
												<p class="name last">I Kt Sudiarsana, I Nym Eriawan, I Md Dharmawan, I Wyn Sutedja</p>
												<div class="txt-content last">
													<h2>Stories, The Secret</h2>
													There are million questions popping up about what makes UC Silver growing so rapidly. It is simply 
													because of the combination of four different pillars that make a great solid-strength and perfect. These four basic principles are the foundation that we can always count on to run this life in which we will always apply to any business that we do. These four principles are as follow :<br><br>

													<ul>
														<li>To get involved in maintaining and preserving the culture that has been handed down to us.</li>
														<li>The kinship systems that unite us in every aspect.</li>
														<li>The courage in taking initiative and bold steps to become the best and foremost.</li>
														<li>To always strive to provide happiness and to love others and the universe so that they too can feel the equal happiness in life.</li>
													</ul><br>
													Grasping tightly to these basic principles inspired and made a solid structure on the development of UC Silver business. These four pillars are also what inspired the four brothers’ figure behind the success of UC Silver. Although each of these pillars has different advantages, they never neglect the vow to reach the ultimate goal, to promote UC Silver to become the famous Indonesian local brand that has a name in the eyes of the world.
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</section>
				
			<script>
			    $(document).ready(function() {
			      $("#owl-demo").owlCarousel({

			      navigation : true,
			      slideSpeed : 300,
			      paginationSpeed : 400,
			      singleItem : true,
			      autoHeight : true,
			      autoPlay: 5000
			      });
			    });
			</script>			
<?php echo $footer; ?>