<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<!-- <meta name="viewport" content="width=device-width"> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>

<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>

<!-- css ucsilver -->
<link rel="stylesheet" type="text/css" href="catalog/view/theme/ucsilver/stylesheet/css/style.css">
<link rel="stylesheet" type="text/css" href="catalog/view/theme/ucsilver/stylesheet/css/bootstrap.min.css">
<!-- end css ucsilver -->

<!-- js template uc silver -->
<!--<script src="catalog/view/theme/ucsilver/stylesheet/js/jquery.min.js"></script>-->
<script>window.jQuery || document.write('<script src="catalog/view/theme/ucsilver/stylesheet/js/jquery-1.10.2.min.js"><\/script>')</script></script>
<script type="text/javascript"src="catalog/view/theme/ucsilver/stylesheet/js/bootstrap.min.js"></script>
<!-- end js template uc silver -->

<!-- owl carousel -->
<link href="catalog/view/theme/ucsilver/stylesheet/js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="catalog/view/theme/ucsilver/stylesheet/js/owl-carousel/owl.theme.css" rel="stylesheet">
<script src="catalog/view/theme/ucsilver/stylesheet/js/owl-carousel/owl.carousel.js"></script>
<!-- end owl carousel -->

</head>
<body id="detail">
				<header>
					<div class="container ">
						<div class="row">
							<div class="col-md-12">
								<div class="header-bar">
									<div class="row">
										<div id="search" class="col-md-3 col-md-offset-4" style="padding-top: 13px;">
										    <div class="button-search"></div>
											<input type="text" name="search" value="<?php echo $search; ?>" class="form-control src"/>
										</div>
										<div class="wh-login col-md-3 col-md-offset-3">
											<li class="pull-right">
												<!--a href="<?php echo $links; ?>">Wholesale Login</a-->
												<!--div id="welcome" class="account_right"-->
													<?php if (!$logged) { ?>
													<?php echo $text_welcome; ?>
													<?php } else { ?>
													<?php echo $text_logged; ?>
													<?php } ?>
												<!--/div-->
											</li>
										</div>
										<div class="col-md-2">
											<div class="row">
												<?php echo $cart; ?>
											</div>
										</div>
									</div>
								</div>																
							</div>
						</div>
					</div>
					<div class="nav-bg " style="background: rgba(131, 129, 129, 0.72); position:absolute; width:100%; z-index:10;">
						<div class="container">
							<div class="row">
								<div class="col-md-3 center-logo ">
									<!-- <h1 class=""> --><a href="">
										<img class="logo logo-uc" src="catalog/view/theme/ucsilver/stylesheet/images/logo.png" class="logo"></a><!-- </h1> -->
								</div>
								<div class="col-md-9">
									<nav class="navbar navbar-inverse" role="navigation">
										<!-- Brand and toggle get grouped for better mobile display -->
										<div class="navbar-header">
											<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
												<span class="sr-only">Toggle navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</button>
											<a class="navbar-brand visible-xs" href="#"></a>
										</div>

										<!-- Collect the nav links, forms, and other content for toggling -->
										<div class="collapse navbar-collapse navbar-ex1-collapse">
											<ul class="nav navbar-nav">
												<?php
												foreach ($categories as $model=>$category) {
													if($model ==0)
													{
														echo '<li class="dropdown">';
													}
													else {
														echo '<li>';
													}
												?>
													<a href="<?php echo $category['href']; ?>" class="dropdown-toggle <?php echo ( 1==$model ? 'aktif' : '' ); ?>" data-toggle="" ><?php echo $category['name']; ?></a>
													<?php if ($category['children']) { ?>
													<?php for ($i = 0; $i < count($category['children']);) { ?>
														<ul class="mega-menu">
															<?php
																$j = $i + ceil(count($category['children']) / $category['column']);
																for (; $i < $j; $i++)
																{
																	if (isset($category['children'][$i]))
																	{
															?>
																	<li>
																		<a href="<?php echo $category['children'][$i]['href']; ?>">
																			<div class="img-<?php echo $this->createslug($category['children'][$i]['name']);?>"></div>
																			<span class="cont">
																				<span class="title"><?php echo $category['children'][$i]['name']; ?></span>
																				<span class="desc"><?php echo $category['children'][$i]['desc_link']; ?></span>
																			</span>
																		</a>
																	</li>
															<?php
																	}
																}
															?>
														</ul>
													<?php } ?>
													<?php } ?>
												</li>
												<?php } ?>
											</ul>
										</div>
										<!-- /.navbar-collapse -->
									</nav>
								</div>
							</div>
						</div>
					</div>
				</header>
<div id="notification"></div>	
<?php if ($error) { ?>    
<div class="warning">
<?php echo $error ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>    
<?php } ?>
