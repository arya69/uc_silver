$(document).ready(function() {
	content_overview();
	category();
	collection();
	// overview();	
	/* Search */
	$('.button-search').bind('click', function() {
		url = $('base').attr('href') + 'index.php?route=product/search';
				 
		var search = $('input[name=\'search\']').attr('value');
		
		if (search) {
			url += '&search=' + encodeURIComponent(search);
		}
		
		location = url;
	});
	
	$('#search input[name=\'search\']').bind('keydown', function(e) {
		if (e.keyCode == 13) {
			url = $('base').attr('href') + 'index.php?route=product/search';
			 
			var search = $('input[name=\'search\']').attr('value');
			
			if (search) {
				url += '&search=' + encodeURIComponent(search);
			}
			
			location = url;
		}
	});
	
	/* Ajax Cart */
	$('#cart > .heading a').live('click', function() {
		$('#cart').addClass('active');
		
		$('#cart').load('index.php?route=module/cart #cart > *');
		
		$('#cart').live('mouseleave', function() {
			$(this).removeClass('active');
		});
	});
	
	/* Mega Menu */
	$('#menu ul > li > a + div').each(function(index, element) {
		// IE6 & IE7 Fixes
		if ($.browser.msie && ($.browser.version == 7 || $.browser.version == 6)) {
			var category = $(element).find('a');
			var columns = $(element).find('ul').length;
			
			$(element).css('width', (columns * 143) + 'px');
			$(element).find('ul').css('float', 'left');
		}		
		
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();
		
		i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
		
		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// IE6 & IE7 Fixes
	if ($.browser.msie) {
		if ($.browser.version <= 6) {
			$('#column-left + #column-right + #content, #column-left + #content').css('margin-left', '195px');
			
			$('#column-right + #content').css('margin-right', '195px');
		
			$('.box-category ul li a.active + ul').css('display', 'block');	
		}
		
		if ($.browser.version <= 7) {
			$('#menu > ul > li').bind('mouseover', function() {
				$(this).addClass('active');
			});
				
			$('#menu > ul > li').bind('mouseout', function() {
				$(this).removeClass('active');
			});	
		}
	}
	
	$('.success img, .warning img, .attention img, .information img').live('click', function() {
		$(this).parent().fadeOut('slow', function() {
			$(this).remove();
		});
	});	
});

function getURLVar(key) {
	var value = [];
	
	var query = String(document.location).split('?');
	
	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');
			
			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}
		
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
} 

function addToCart(product_id, quantity) {
	//quantity = typeof(quantity) != 'undefined' ? quantity : 1;
	qty = document.getElementById('quantity').value;
	if (quantity == null || qty == null) {
		quantity=1;
	}
	else {
		quantity=qty;
	}	

	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: 'product_id=' + product_id + '&quantity=' + quantity,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			}
			
			if (json['success']) {
				//$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				$('#notification').html('<div id="notif">'+json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div></div>');
				
				$('.success').fadeIn('slow');
				
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
}
function addToWishList(product_id) {
	$.ajax({
		url: 'index.php?route=account/wishlist/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#wishlist-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}	
		}
	});
}

function addToCompare(product_id) { 
	$.ajax({
		url: 'index.php?route=product/compare/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#compare-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function category(id,page) {
	var path = getUrlVars()["path"];
	id = typeof(id) != 'undefined' ? id : path;
	page = typeof(page) != 'undefined' ? page : 1;
	$('.rmv').removeClass('btn-active');
	// $('.path'+id).addClass('btn-active');
	$.ajax({
		url: 'index.php?route=product/category/njingajax',
		type: 'post',
		data: 'path=' + id + '&page=' + page,
		dataType: 'json',
		success: function(json) {
			if (json) {
				$('.path'+id).addClass('btn-active');
				$('.box-product').html('');
				$('.fagination').html('');
				for (var i=0;i<json['product'].length;i++) {
					if (json['product'][i]['price']==false) {
						$('.box-product').append('<div class="box"><a href="'+json['product'][i]['href'] +'"><div class="hover"><div class="hover-content"><h3>'+json['product'][i]['name']+'</h3><p>'+json['product'][i]['description']+'</p><div class="link"></div></div><img src="'+json['product'][i]['thumb'] +'" title="'+json['product'][i]['name'] +'" alt="'+json['product'][i]['name'] +'" /></div></a></div>');
					} else {
						$('.box-product').append('<div class="box"><a href="'+json['product'][i]['href'] +'"><div class="hover"><div class="hover-content"><h3>'+json['product'][i]['name']+'</h3><p>'+json['product'][i]['description']+'</p><div class="link"><div class="price btn btn-block btn-primary">'+json['product'][i]['price']+'</div></div></div><img src="'+json['product'][i]['thumb'] +'" title="'+json['product'][i]['name'] +'" alt="'+json['product'][i]['name'] +'" /></div></a></div>');
					}
				}
				$('.fagination').append(json['pagination']);
				
			}
		}
	});
}

function collection(id,page) {
	var path = getUrlVars()["path"];
	id = typeof(id) != 'undefined' ? id : path;
	page = typeof(page) != 'undefined' ? page : 1;
	$('.rmv').removeClass('btn-active');
	// $('.path'+id).addClass('btn-active');
	$.ajax({
		url: 'index.php?route=product/collection/overviewajax',
		type: 'post',
		data: 'path=' + id + '&page=' + page,
		dataType: 'json',
		success: function(json) {
			if (json) {
				$('.path'+id).addClass('btn-active');
				// $('.box-product-collection').html('');
				// $('.fagination-collection').html('');
				// for (var i=0;i<json['product'].length;i++) {
					// $('.box-product-collection').append('<div class="box"><a href="'+json['product'][i]['href'] +'"><div class="hover"><div class="hover-content"><h3>'+json['product'][i]['name']+'</h3><p>'+json['product'][i]['description']+'</p><div class="link"><div class="price btn btn-block btn-primary">'+json['product'][i]['price']+'</div></div></div><img src="'+json['product'][i]['thumb'] +'" title="'+json['product'][i]['name'] +'" alt="'+json['product'][i]['name'] +'" /></div></a></div>');
				// }
				// $('.fagination-collection').append(json['pagination']);
				$('.box-product').html('');
				$('.fagination').html('');
				for (var i=0;i<json['product'].length;i++) {
					$('.box-product').append('<div class="box"><a href="'+json['product'][i]['href'] +'"><div class="hover"><div class="hover-content"><h3>'+json['product'][i]['name']+'</h3><p>'+json['product'][i]['description']+'</p><div class="link"><div class="price btn btn-block btn-primary">'+json['product'][i]['price']+'</div></div></div><img src="'+json['product'][i]['thumb'] +'" title="'+json['product'][i]['name'] +'" alt="'+json['product'][i]['name'] +'" /></div></a></div>');
				}
				$('.fagination').append(json['pagination']);
				
			}
		}
	});
}

function overview(idov,page) {
	if (typeof(idov) != 'undefined' || null!=idov){
		id=idov;
	}
	else{
		id=null;
	}
	page = typeof(page) != 'undefined' ? page : 1;
	$('.ovr').removeClass('btn-active');
	$('.path'+id).addClass('btn-active');
	$.ajax({
		url: 'index.php?route=product/category/overview',
		type: 'post',
		data: 'path=' + id + '&page=' + page,
		dataType: 'json',
		success: function(json) {
			if (json) {
				$('.ovr').html('');
				$('.faginationovr').html('');
				for (var i=0;i<json['product'].length;i++) {
					$('.ovr').append('<div class="box"><a href="'+json['product'][i]['href'] +'"><div class="hover"><div class="hover-content"><h3>'+json['product'][i]['name']+'</h3><p>'+json['product'][i]['description']+'</p><div class="link"><div class="price btn btn-block btn-primary">'+json['product'][i]['price']+'</div></div></div><img src="'+json['product'][i]['thumb'] +'" title="'+json['product'][i]['name'] +'" alt="'+json['product'][i]['name'] +'" /></div></a></div>');
				}
				$('.fagination').append(json['pagination']);
				
			}
		}
	});
}

function content_overview(idov) {
	if (typeof(idov) != 'undefined' || null!=idov){
		id=idov;
	}
	else{
		id='';
	}
	$('.ovr').removeClass('btn-active');
	// $('.overvw'+id).addClass('btn-active');
	$('#ovr-link').hide();
	$.ajax({
		url: 'index.php?route=product/category/content_overview',
		type: 'post',
		data: 'path=' + id,
		dataType: 'json',
		success: function(json) {
			if (json) {
				$('.overvw'+json['overview_id']).addClass('btn-active');
				if ('empty'!=json['href']) {
					$('#ovr-link').show();
					$('#ovr-link').attr("href", json['href']);
				} else {
					$('#ovr-link').hide();
				}
				$('.ovr-name').html('');
				$('.ovr-description').html('');
				$('.ovr-name').append(json['name']);
				$('.ovr-description').append(json['description']);
			}
		}
	});
}