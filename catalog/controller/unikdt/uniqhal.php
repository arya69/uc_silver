<?php
class ControllerUnikdtUniqhal extends Controller { 
	public function design() {
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
	
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/uniq/design.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/uniq/design.tpl';
		}
		$this->children = array(
			'common/footer',
			'common/header'
		);										
		$this->response->setOutput($this->render());
	}
	
	public function stories() {
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
	
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/uniq/stories.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/uniq/stories.tpl';
		}
		$this->children = array(
			'common/footer',
			'common/header'
		);										
		$this->response->setOutput($this->render());
	}

}
?>