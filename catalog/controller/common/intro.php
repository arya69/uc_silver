<?php  
class ControllerCommonIntro extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));

		$this->data['heading_title'] = $this->config->get('config_title');
		
		$this->template = $this->config->get('config_template') . '/template/common/intro.tpl';
	
		$this->children = array(
			'common/footer',
			'common/headerintro'
		);
										
		$this->response->setOutput($this->render());
	}
}
?>