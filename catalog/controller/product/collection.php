<?php 
class ControllerProductCollection extends Controller {  
	public function index() {
		$this->language->load('product/category');
		
		$this->load->model('catalog/category');
		
		$this->load->model('catalog/product');
		
		$this->load->model('tool/image'); 
		
		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}
				
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
							
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
       		'separator' => false
   		);	
			
		if (isset($this->request->get['path'])) {
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
									
			$path = '';
		
			if (empty($this->request->get['path'])) {
				$parts = explode('_', (string)$this->request->post['path']);
			}
			else {
				$parts = explode('_', (string)$this->request->get['path']);
			}
		
			$category_id = (int)array_pop($parts);
		
			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}
									
				$category_info = $this->model_catalog_category->getOverview($path_id);
				
				if ($category_info) {
	       			$this->data['breadcrumbs'][] = array(
   	    				'text'      => $category_info['name'],
						'href'      => $this->url->link('product/category', 'path=' . $path . $url),
        				'separator' => $this->language->get('text_separator')
        			);
				}
			}
		} else {
			$category_id = 0;
		}
				
		$category_info = $this->model_catalog_category->getOverview($category_id);
	
		if ($category_info) {
	  		//$this->document->setTitle($category_info['name']);
	  		$this->document->setTitle('FINE COLLECTIONS');
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);
			$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
			
			$this->data['heading_title'] = $category_info['name'];
			
			$this->data['text_refine'] = $this->language->get('text_refine');
			$this->data['text_empty'] = $this->language->get('text_empty');			
			$this->data['text_quantity'] = $this->language->get('text_quantity');
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_points'] = $this->language->get('text_points');
			$this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$this->data['text_display'] = $this->language->get('text_display');
			$this->data['text_list'] = $this->language->get('text_list');
			$this->data['text_grid'] = $this->language->get('text_grid');
			$this->data['text_sort'] = $this->language->get('text_sort');
			$this->data['text_limit'] = $this->language->get('text_limit');
					
			$this->data['button_cart'] = $this->language->get('button_cart');
			$this->data['button_wishlist'] = $this->language->get('button_wishlist');
			$this->data['button_compare'] = $this->language->get('button_compare');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			// Set the last category breadcrumb		
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
									
			$this->data['breadcrumbs'][] = array(
				'text'      => $category_info['name'],
				'href'      => $this->url->link('product/category', 'path=' . $this->request->get['path']),
				'separator' => $this->language->get('text_separator')
			);
								
			if ($category_info['image']) {
				$this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			} else {
				$this->data['thumb'] = '';
			}
									
			$this->data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['compare'] = $this->url->link('product/compare');
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}	
						
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
								
			$this->data['categories'] = array();
			
			$results = $this->model_catalog_category->getOverviewCategories($category_id);
			
			foreach ($results as $result) {
				$data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);
				
				$product_total = $this->model_catalog_product->getTotalProducts($data);				
				
				$this->data['categories'][] = array(
					'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			}
			
			// $this->data['products'] = array();
			
			// $data = array(
				// 'filter_category_id' => $category_id,
				// 'filter_filter'      => $filter, 
				// 'sort'               => $sort,
				// 'order'              => $order,
				// 'start'              => ($page - 1) * $limit,
				// 'limit'              => $limit
			// );
			
			//overview
			$overview_id = $this->model_catalog_category->getLastCategory();
			if (!empty($overview_id)) {			
				$dataoverview = array(
					'filter_category_id' => $overview_id['category_id'],
					'filter_filter'      => $filter, 
					'sort'               => $sort,
					'order'              => $order,
					'start'              => ($page - 1) * $limit,
					'limit'              => $limit
				);
				$overview_categories = $this->model_catalog_category->getOverviewCategories(0);
				
				foreach ($overview_categories as $overview_category) {
					$total = $this->model_catalog_product->getTotalOverview(array('filter_category_id' => $overview_category['category_id']));

					$children_data = array();

					$children = $this->model_catalog_category->getCategories($overview_category['category_id']);

					foreach ($children as $child) {
						$data = array(
							'filter_category_id'  => $child['category_id'],
							'filter_sub_category' => true
						);

						$product_total = $this->model_catalog_product->getTotalOverview($data);

						$total += $product_total;

						$children_data[] = array(
							'category_id' => $child['category_id'],
							'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
							'href'        => $this->url->link('product/category', 'path=' . $overview_category['category_id'] . '_' . $child['category_id'])	
						);		
					}

					$this->data['overview_data_categories'][] = array(
						'category_id' => $overview_category['category_id'],
						'name'        => $overview_category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
						'children'    => $children_data,
						'href'        => $this->url->link('product/category', 'path=' . $overview_category['category_id'])
					);	
				}
				
				$overview_total = $this->model_catalog_product->getTotalOverview($dataoverview);
				$overviews = $this->model_catalog_product->getOverviews($dataoverview);
				
				foreach ($overviews as $overview) {
					if ($overview['image']) {
						$image = $this->model_tool_image->resize($overview['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						$image = false;
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($overview['price'], $overview['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					if ((float)$overview['special']) {
						$special = $this->currency->format($this->tax->calculate($overview['special'], $overview['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}	
					
					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$overview['special'] ? $overview['special'] : $overview['price']);
					} else {
						$tax = false;
					}				
					
					if ($this->config->get('config_review_status')) {
						$rating = (int)$overview['rating'];
					} else {
						$rating = false;
					}
									
					$this->data['overview_product'][] = array(
						'product_id'  => $overview['product_id'],
						'thumb'       => $image,
						'name'        => $overview['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($overview['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $overview['rating'],
						'reviews'     => sprintf($this->language->get('text_reviews'), (int)$overview['reviews']),
						'href'        => $this->url->link('product/collectionproduct', 'path=' . $this->request->get['path'] . '&product_id=' . $overview['product_id'] . $url)
					);
				}
			}
			//end overview
					
			// $product_total = $this->model_catalog_product->getTotalProducts($data);			
			// $results = $this->model_catalog_product->getProducts($data);
							
			// foreach ($results as $result) {
				// if ($result['image']) {
					// $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				// } else {
					// $image = false;
				// }
				
				// if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					// $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				// } else {
					// $price = false;
				// }
				
				// if ((float)$result['special']) {
					// $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				// } else {
					// $special = false;
				// }	
				
				// if ($this->config->get('config_tax')) {
					// $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				// } else {
					// $tax = false;
				// }				
				
				// if ($this->config->get('config_review_status')) {
					// $rating = (int)$result['rating'];
				// } else {
					// $rating = false;
				// }
								
				// $this->data['products'][] = array(
					// 'product_id'  => $result['product_id'],
					// 'thumb'       => $image,
					// 'name'        => $result['name'],
					// 'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
					// 'price'       => $price,
					// 'special'     => $special,
					// 'tax'         => $tax,
					// 'rating'      => $result['rating'],
					// 'reviews'     => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					// 'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				// );
			// }
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
										
			$this->data['sorts'] = array();
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			); 

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			); 
			
			if ($this->config->get('config_review_status')) {
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				); 
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->data['limits'] = array();
	
			$limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));
			
			sort($limits);
	
			foreach($limits as $limits){
				$this->data['limits'][] = array(
					'text'  => $limits,
					'value' => $limits,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $limits)
				);
			}
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
	
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
					
			$pagination = new Fakgination();
			$pagination->total = $overview_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
		
			$this->data['pagination'] = $pagination->render();
		
			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;
		
			$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/collection.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/collection.tpl';
			} else {
				$this->template = 'default/template/product/category.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
				
			$this->response->setOutput($this->render());										
    	} else {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
												
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
				
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
						
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/category', $url),
				'separator' => $this->language->get('text_separator')
			);
				
			$this->document->setTitle($this->language->get('text_error'));

      		$this->data['heading_title'] = $this->language->get('text_error');

      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
					
			$this->response->setOutput($this->render());
		}
  	}
	
	public function overviewajax() {
		$this->language->load('product/category');
		
		$this->load->model('catalog/category');
		
		$this->load->model('catalog/product');
		
		$this->load->model('tool/image'); 
		
		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}
				
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		$json = array();
		
		if (isset($this->request->post['path'])) {
			
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			$path = '';
		
			if (!empty($this->request->post['path'])) {
				$parts = explode('_', (string)$this->request->post['path']);
			}
			else {				
				$parts = explode('_', (string)$this->request->get['path']);
			}
		
			$category_id = (int)array_pop($parts);
		
		} else {
			$category_id = 0;
		}
		
		
		$category_info = $this->model_catalog_category->getOverview($category_id);
		
		if ($category_info) {
			$this->data['categories'] = array();
			
			$results = $this->model_catalog_category->getOverviewCategories($category_id);
			
			foreach ($results as $result) {
				$data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);
				
				$product_total = $this->model_catalog_product->getTotalProducts($data);				
				
				$this->data['categories'][] = array(
					'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			}
			
			$this->data['products'] = array();
			
			//overview
			if (empty($category_id))
			{
				$overview_id = $this->model_catalog_category->getLastCategory();
				$ovr_id=$overview_id['category_id'];
			}
			else
			{
				$ovr_id=$category_id;
			}
			
				$dataoverview = array(
					'filter_category_id' => $ovr_id,
					'filter_filter'      => $filter, 
					'sort'               => $sort,
					'order'              => $order,
					'start'              => ($page - 1) * $limit,
					'limit'              => $limit
				);
				$overview_categories = $this->model_catalog_category->getOverviewCategories(0);
				
				foreach ($overview_categories as $overview_category) {
					$total = $this->model_catalog_product->getTotalOverview(array('filter_category_id' => $overview_category['category_id']));

					$children_data = array();

					$children = $this->model_catalog_category->getCategories($overview_category['category_id']);

					foreach ($children as $child) {
						$data = array(
							'filter_category_id'  => $child['category_id'],
							'filter_sub_category' => true
						);

						$product_total = $this->model_catalog_product->getTotalOverview($data);

						$total += $product_total;

						$children_data[] = array(
							'category_id' => $child['category_id'],
							'name'        => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
							'href'        => $this->url->link('product/category', 'path=' . $overview_category['category_id'] . '_' . $child['category_id'])	
						);		
					}

					$this->data['overview_data_categories'][] = array(
						'category_id' => $overview_category['category_id'],
						'name'        => $overview_category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
						'children'    => $children_data,
						'href'        => $this->url->link('product/category', 'path=' . $overview_category['category_id'])
					);	
				}
				
				$overview_total = $this->model_catalog_product->getTotalOverview($dataoverview);
				$overviews = $this->model_catalog_product->getOverviews($dataoverview);
				
				foreach ($overviews as $overview) {
					if ($overview['image']) {
						$image = $this->model_tool_image->resize($overview['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
					} else {
						$image = false;
					}
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($overview['price'], $overview['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					if ((float)$overview['special']) {
						$special = $this->currency->format($this->tax->calculate($overview['special'], $overview['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}	
					
					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$overview['special'] ? $overview['special'] : $overview['price']);
					} else {
						$tax = false;
					}				
					
					if ($this->config->get('config_review_status')) {
						$rating = (int)$overview['rating'];
					} else {
						$rating = false;
					}
					
					//limit title on hover
					if (strlen($overview['name']) >= 15) {
						$_name = substr($overview['name'], 0, 15);
					}else{
						$_name = $overview['name'];
					}
									
					$this->data['overview_product'][] = array(
						'product_id'  => $overview['product_id'],
						'category_id'  => $this->request->post['path'],
						'category_id'  => $this->request->post['path'],
						'thumb'       => $image,
						// 'name'        => $overview['name'],
						'name'        => $_name,
						'description' => utf8_substr(strip_tags(html_entity_decode($overview['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $overview['rating'],
						'reviews'     => sprintf($this->language->get('text_reviews'), (int)$overview['reviews']),
						'href'        => $this->url->link('product/collectionproduct', 'path=' . $this->request->post['path'] . '&product_id=' . $overview['product_id'] . $url)
					);
				}
			//end overview
			
			// $data = array(
				// 'filter_category_id' => $category_id,
				// 'filter_filter'      => $filter, 
				// 'sort'               => $sort,
				// 'order'              => $order,
				// 'start'              => ($page - 1) * $limit,
				// 'limit'              => $limit
			// );
					
			// $product_total = $this->model_catalog_product->getTotalProducts($data); 
			
			// $results = $this->model_catalog_product->getProducts($data);
			
			// foreach ($results as $result) {
				// if ($result['image']) {
					// $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				// } else {
					// $image = false;
				// }
				
				// if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					// $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				// } else {
					// $price = false;
				// }
				
				// if ((float)$result['special']) {
					// $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				// } else {
					// $special = false;
				// }	
				
				// if ($this->config->get('config_tax')) {
					// $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				// } else {
					// $tax = false;
				// }				
				
				// if ($this->config->get('config_review_status')) {
					// $rating = (int)$result['rating'];
				// } else {
					// $rating = false;
				// }

				// //limit title on hover
				// if (strlen($result['name']) >= 15) {
					// $_name = substr($result['name'], 0, 15);
				// }else{
					// $_name = $result['name'];
				// }

				// $this->data['products'][] = array(
					// 'product_id'  => $result['product_id'],
					// 'thumb'       => $image,
					// 'name'        => $_name,
					// 'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 163) . '...',
					// 'price'       => $price,
					// 'special'     => $special,
					// 'tax'         => $tax,
					// 'rating'      => $result['rating'],
					// 'reviews'     => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					// 'href'        => $this->url->link('product/product', 'path=' . $this->request->post['path'] . '&product_id=' . $result['product_id'] . $url)
				// );
			// }
			// $json['product']=$this->data['products'];
			if (!empty($this->data['overview_product']))
			{
				$json['product']=$this->data['overview_product'];
			}
			else
			{
				$json['product']='';
			}
			
			$pagination = new Ajaxfagination();
			$pagination->total = $overview_total;
			$pagination->category = $category_id;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->post['path'] . $url . '&page={page}');
		
			$json['pagination'] = $pagination->render();
		
			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;			
			
			$this->response->setOutput(json_encode($json));
		}
	}
	
	public function overview() {
		$this->language->load('product/category');
		
		$this->load->model('catalog/category');
		
		$this->load->model('catalog/product');
		
		$this->load->model('tool/image'); 
		
		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}
				
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		$json = array();
		$overview_id = $this->model_catalog_category->getLastCategory();
		
		if (isset($this->request->post['path'])) {
			
			$path = '';
		
			if (!empty($this->request->post['path'])) {
				$parts = explode('_', (string)$this->request->post['path']);
			}
			else {
				$parts = explode('_', $overview_id['category_id']);
			}
		
			$overview_id = (int)array_pop($parts);
		
		} else {
			$overview_id = 0;
		}
		print_r($overview_id);
		$category_info = $this->model_catalog_category->getOverview($overview_id);
		
		if ($category_info) {
		
			// $this->data['categories'] = array();			
			// $results = $this->model_catalog_category->getCategories($overview_id);			
			// foreach ($results as $result) {
				// $data = array(
					// 'filter_category_id'  => $result['category_id'],
					// 'filter_sub_category' => true
				// );				
				// $product_total = $this->model_catalog_product->getTotalProducts($data);				
				// $this->data['categories'][] = array(
					// 'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					// 'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				// );
			// }
			
			$this->data['products_overview'] = array();
			
			$dataoverview = array(
				'filter_category_id' => $overview_id,
				'filter_filter'      => $filter, 
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
				
			$product_total = $this->model_catalog_product->getTotalOverview($dataoverview); 
			
			$results = $this->model_catalog_product->getOverviews($dataoverview);
			
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = false;
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
				
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}	
				
				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}				
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				//limit title on hover
				if (strlen($result['name']) >= 15) {
					$_name = substr($result['name'], 0, 15);
				}else{
					$_name = $result['name'];
				}

				$this->data['products_overview'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $_name,
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 163) . '...',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $result['rating'],
					'reviews'     => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					'href'        => $this->url->link('product/product', 'path=' . $this->request->post['path'] . '&product_id=' . $result['product_id'] . $url)
				);
			}
			$json['product']=$this->data['products_overview'];
			
			$pagination = new Ajaxfagination();
			$pagination->total = $product_total;
			$pagination->category = $overview_id;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->post['path'] . $url . '&page={page}');
		
			$json['pagination'] = $pagination->render();
		
			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;			
			
			$this->response->setOutput(json_encode($json));
		}
	}
	
	public function content_overview() {
		$this->language->load('product/category');
		
		$this->load->model('catalog/category');
		
		$this->load->model('tool/image'); 
		
		$json = array();
		
		$cat=$this->model_catalog_category->getLastCategory();
		
		if (isset($this->request->post['path'])) {
			
			$path = '';
		
			if (!empty($this->request->post['path'])) {
				$parts = explode('_', (string)$this->request->post['path']);
			}
			else {
				$parts = explode('_', $cat['category_id']);
			}
		
			$overview_id = (int)array_pop($parts);
		
		} else {
			$overview_id = $cat['category_id'];
		}
		
		$category_info = $this->model_catalog_category->getOverview($overview_id);
		$overview_content=array();
		if ($category_info) {
			$overview_content['overview_id']=$category_info['category_id'];
			
			if (0!=$category_info['status']) {
				$overview_content['href']=html_entity_decode($this->url->link('product/category', 'path=' . $category_info['category_id'].'&page=catalogue'));
			}
			else {
				$overview_content['href']='empty';
			}
			
			$overview_content['name']=html_entity_decode($category_info['name'], ENT_QUOTES, 'UTF-8');
			
			$overview_content['description']=html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
		
			$json=$overview_content;
			
			$this->response->setOutput(json_encode($json));
		}
	}	
}
?>