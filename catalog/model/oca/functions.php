<?php 
     //===========================================//
    // Author: Joel Reeds                        //
   // Company: OpenCart Addons                  //
  // Website: http://opencartaddons.com        //
 // Contact: webmaster@opencartaddons.com     //
//===========================================//

class ModelOCAFunctions extends Model {  
	
	public function getField($extension, $field) {
		$value = $this->config->get($extension . '_' . $field);
		return (is_string($value) && strpos($value, 'a:') === 0) ? unserialize($value) : $value;
	}
}
?>