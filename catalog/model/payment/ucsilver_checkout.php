<?php 
class ModelPaymentUcsilverCheckout extends Model {
  	public function getMethod($address, $total) {
		$this->language->load('payment/ucsilver_checkout');
		
		if ($total >= 0) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();
			
		if ($status) {  
			$method_data = array( 
				'code'       => 'ucsilver_checkout',
				'title'      => $this->language->get('text_title'),
				'decription'      => $this->language->get('text_decription'),
				'sort_order' => $this->config->get('ucsilver_checkout_sort_order')
			);
		}
		
    	return $method_data;
  	}
}
?>