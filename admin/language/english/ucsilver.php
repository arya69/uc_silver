<?php
// Heading
$_['heading_title']    = 'UC Silver Shipping Module';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified flat rate shipping!';

// Entry
$_['entry_shipping_courier'] = 'Shipping Courier :<br/><span class="help">Please separate different courier with semicolon (;).</span>';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify UC Silver Shipping Module!';
?>