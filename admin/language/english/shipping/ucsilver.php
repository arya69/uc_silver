<?php
// Heading
$_['heading_title']    = 'UC Silver Shipping Method';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified ucsilver shipping!';

// Entry
$_['entry_shipping_courier'] = 'Shipping Courier :<br/><span class="help">Please separate different courier with semicolon (;).</span>';
$_['entry_ucsilver_method'] = 'Shipping Method :<br/><span class="help">Please separate different method with semicolon (;).</span>';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify UC Silver Shipping Module!';
?>