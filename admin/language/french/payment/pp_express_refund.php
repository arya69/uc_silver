<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']				= 'Transaction du remboursement';

// Text
$_['text_pp_express']           = 'PayPal Express Checkout';
$_['text_current_refunds']      = 'Les remboursements ont d&eacute;j&agrave; &eacute;t&eacute; effectu&eacute;s pour cette transaction. Le remboursement maximum est de';

// Button
$_['btn_cancel']                = 'Annuler';
$_['btn_refund']                = 'Issue du remboursement';

// Entry
$_['entry_transaction_id']      = 'N&deg; de transaction';
$_['entry_full_refund']         = 'Remboursement int&eacute;gral';
$_['entry_amount']              = 'Compte';
$_['entry_message']             = 'Message';

// Error
$_['error_partial_amt']         = 'Vous devez entrer un montant de remboursement partiel';
$_['error_data']                = 'Dans la demande les donn&eacute;es sont manquantes';
?>