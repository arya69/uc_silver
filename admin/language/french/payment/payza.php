<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'Payza';

// Text 
$_['text_payment']			= 'Paiement';
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du paiement <b>Payza</b> avec succ&egrave;s !';

// Entry
$_['entry_merchant']		= 'Identifiant marchand :';
$_['entry_security']		= 'Code de s&eacute;curit&eacute; :';
$_['entry_callback']		= 'Alerte URL :<br /><span class="help">R&eacute;glage dans le panneau de contr&ocirc;le Payza. Vous devrez &eacute;galement cocher la case "&Eacute;tat IPN" sur "autoris&eacute;".</span>';
$_['entry_total']			= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_order_status']	= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>Payza</b> !';
$_['error_merchant']		= 'L&#8217;<b>Identifiant marchand</b> est requis !';
$_['error_security']		= 'Le <b>Code de s&eacute;curit&eacute;</b> est requis !';
?>