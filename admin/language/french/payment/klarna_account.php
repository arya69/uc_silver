<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//----------------------------------

// Heading
$_['heading_title']         = 'Compte Klarna';

// Text
$_['text_payment']          = 'Paiement';
$_['text_success']          = 'f&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails de paiement par Klarna avec succ&egrave;s !';
$_['text_klarna_account']   = '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Account" title="Klarna Account" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']             = 'R&eacute;el';
$_['text_beta']             = 'B&eacute;ta';
$_['text_sweden']           = 'Su&egrave;de';
$_['text_norway']           = 'Norv&egrave;ge';
$_['text_finland']          = 'Finlande';
$_['text_denmark']          = 'Danemark';
$_['text_germany']          = 'Allemagne';
$_['text_netherlands']      = 'Pays-Bas';

// Entry
$_['entry_merchant']        = 'ID Marchand Klarna :<br /><span class="help">(estore id) &agrave; utiliser pour le service Klarna (fourni par Klarna).</span>';
$_['entry_secret']          = 'Secret Klarna :<br /><span class="help">Secret partag&eacute; &agrave; utiliser avec le service Klarna (fourni par Klarna).</span>';
$_['entry_server']          = 'Serveur :';
$_['entry_total']			= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_pending_status']  = '&Eacute;tat en attente :';
$_['entry_accepted_status'] = '&Eacute;tat accept&eacute; :';
$_['entry_geo_zone']        = 'Zone g&eacute;ographique :';
$_['entry_status']          = '&Eacute;tat :';
$_['entry_sort_order']      = 'Classement :';

// Error
$_['error_permission']      = 'Attention: Vous n&#8217;avez pas les droits n&eacute;cessaires pour modifier le paiement par compte Klarna !';
$_['error_pclass']          = 'Impossible de retrouver pClass pour %s. Code d&#8217;erreur : %s; Message d&#8217;erreur : %s';
$_['error_curl']            = 'Erreur Curl - Code : %d; Message : %s';
$_['error_log']             = 'Des erreurs sont survenues lors de la mise &agrave; jour du module. Veuillez v&eacute;rifier le journal des erreurs.';
?>