<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'PSIGate';

// Text 
$_['text_payment']			= 'Paiement'; 
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du paiement <b>PSIGate</b> !';

// Entry
$_['entry_merchant']		= 'N&deg; d&#8217;identifiant marchand :';
$_['entry_password']		= 'Phrase de passe :';
$_['entry_gateway']			= 'URL de Gateway :';
$_['entry_test']			= 'Mode de test :';
$_['entry_total']			= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_order_status']	= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>PSIGate</b> !';
$_['error_merchant']		= 'Attention, le N&deg; d&#8217;identification marchand  est requis !';
$_['error_password']		= 'Attention, la phrase de passe est requise !';
$_['error_gateway']			= 'Attention, l&#8217;URL de Gateway est requise !';
?>