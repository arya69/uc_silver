<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Payment page links
$_['text_amazon_checkout']			= '<a onclick="window.open(\'http://go.amazonservices.com/UKCBASPOpenCart.html\');"><img src="view/image/payment/amazon.png" alt="Paiements Amazon" title="Paiements Amazon" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_amazon_join']				= '<a href="http://go.amazonservices.com/UKCBASPOpenCart.html" target="blank" title="Cliquez ici pour cr&eacute;er votre compte Paiements Amazon">Cliquez ici pour cr&eacute;er votre compte Paiements Amazon</a>';

//Headings
$_['heading_title']					= 'Paiements Amazon';
$_['text_home']						= 'Accueil';
$_['text_payment']					= 'Paiement';

// Text
$_['text_cron_job_url']				= 'URL de la t&acirc;che Cron:';
$_['help_cron_job_url']				= 'D&eacute;finir une t&acirc;che cron pour appeler cette URL';
$_['text_cron_job_token']			= 'Jeton secret';
$_['help_cron_job_token']			= 'Choisir comme long et difficile &agrave; deviner';
$_['text_access_key']				= 'Cl&eacute; d&#8217;acc&egrave;s :';
$_['text_access_secret']			= 'Cl&eacute; secr&eacute;te :';
$_['text_merchant_id']				= 'N&deg; d&#8217;identification marchand :';
$_['text_marketplace']				= 'Places de march&eacute; :';
$_['text_germany']					= 'Allemagne';
$_['text_uk']						= 'Angleterre';
$_['text_checkout_mode']			= 'Mode Checkout :';
$_['text_geo_zone']					= 'Zones g&eacute;ographiques';
$_['text_status']					= '&Eacute;tat :';
$_['text_live']						= 'Mode r&eacute;el';
$_['text_sandbox']					= 'Sandbox';
$_['text_sort_order']				= 'Classement :';
$_['text_minimum_total']			= 'Total minimum de la commande:';
$_['text_all_geo_zones']			= 'Toutes zones g&eacute;ographiques';
$_['text_status_enabled']			= 'Activ&eacute;';
$_['text_status_disabled']			= 'D&eacute;sactiv&eacute;';
$_['text_default_order_status']		= 'En attente :';
$_['text_ready_order_status']		= 'Pr&ecirc;t &agrave; &ecirc;tre exp&eacute;di&eacute; :';
$_['text_canceled_order_status']	= 'Commande annul&eacute;e:';
$_['text_shipped_order_status']		= 'Livraison de la commande :';
$_['text_last_cron_job_run']		= 'Moment d&#8217;ex&eacute;cution de la derni&egrave;re t&acirc;che cron :';
$_['text_allowed_ips']				= 'IPs autoris&eacute;es';
$_['text_add']						= 'Ajouter';
$_['text_upload_success']			= 'F&eacute;licitations, le fichier a &eacute;t&eacute; t&eacute;l&eacute;charg&eacute; avec succ&egrave;s !';
$_['help_adjustment']				= 'Champs en gras et au moins un "-adj" champs sont requis';
$_['help_allowed_ips']				= 'Laissez vide si vous voulez que tout le monde voit le bouton de commande';

// Buttons
$_['button_cancel']					= 'Annuler';
$_['button_save']					= 'Sauvegarder';

// Errors
$_['error_permissions']				= 'Attention, vous n&#8217;avez pas la permission de modifier le module <b>Paiements Amazon</b>';
$_['error_empty_access_secret']		= 'Attention, <b>la cl&eacute; secr&egrave;te</b> est requise !';
$_['error_empty_access_key']		= 'Attention, <b>la cl&eacute; d&#8217;acc&egrave;sv est requise !';
$_['error_empty_merchant_id']		= 'Attention, <b>le N&deg; d&#8217;identification marchand</b> est requis !';
$_['error_curreny']					= 'Votre boutique doit avoir cette devise %s d&#8217;install&eacute;e et d&#8217;activ&eacute;e';
$_['error_upload']					= 'Chargement &eacute;chou&eacute;';

// Checkout button settings
$_['text_button_settings']			= 'Param&egrave;tres du bouton <b>Commander</b>';
$_['text_colour']					= 'Couleur :';
$_['text_orange']					= 'Orange';
$_['text_tan']						= 'Fauve';
$_['text_white']					= 'Blanc';
$_['text_light']					= 'Clair';
$_['text_dark']						= 'Fonc&eacute;';
$_['text_size']						= 'Taille :';
$_['text_medium']					= 'Moyen';
$_['text_large']					= 'Grand';
$_['text_x_large']					= 'Tr&egrave;s grand';
$_['text_background']				= 'Fond :';
$_['text_download']					= '<a href="%s">T&eacute;l&eacute;chargement</a> fichier du th&egrave;me graphique';

// Messages
$_['text_success']					= 'Le module Paiements Amazon a bien &eacute;t&eacute; mis &agrave; jour';

// Order Info
$_['text_amazon_details']			= 'D&eacute;tails Amazon';
$_['text_amazon_order_id']			= 'N&deg; de commande Amazon :';
$_['text_upload']					= 'Mise &agrave; jour';
$_['text_upload_template']			= 'T&eacute;l&eacute;charger le mod&egrave;le graphique en cliquant sur le bouton ci-dessous. Assurez-vous qu&#8217;il sera enregistr&eacute; dans un fichier d&eacute;limit&eacute; par des tabulations.';
$_['tab_order_adjustment']			= 'Ajustement de la commande';

//Table columns
$_['column_submission_id']			= 'Soumission de l&#8217;identifiant';
$_['column_status']					= '&Eacute;tat';
$_['column_text']					= 'R&eacute;ponse';
$_['column_amazon_order_item_code']	= 'Code produit de la commande Amazon';
?>