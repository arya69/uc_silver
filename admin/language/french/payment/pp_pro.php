<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'PayPal Website Paiement Pro (US)';

// Text 
$_['text_payment']			= 'Paiement';
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du paiement <b>PayPal Website Paiement Pro Checkout</b> avec succ&egrave;s !';
$_['text_pp_pro']			= '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Paiement Pro" title="PayPal Website Paiement Pro" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']	= 'Autorisation';
$_['text_sale']				= 'Vente';

// Entry
$_['entry_username']		= 'Nom de l&#8217;utilisateur API :';
$_['entry_password']		= 'Mot de passe API :';
$_['entry_signature']		= 'Signature API :';
$_['entry_test']			= 'Mode de test :<br /><span class="help">Utilisez r&eacute;el ou test (sandbox) pour traiter les transactions ?</span>';
$_['entry_transaction']		= 'Mode de transaction :';
$_['entry_total']			= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_order_status']	= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier les d&eacute;tails du paiement <b>PayPal Website Paiement Pro Checkout</b> !';
$_['error_username']		= 'Attention, le nom de l&#8217;utilisateur API  est requis !'; 
$_['error_password']		= 'Attention, le mot de passe  est requis !'; 
$_['error_signature']		= 'Attention, la signature API est requise !'; 
?>