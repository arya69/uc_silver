<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Headings
$_['heading_title']					= 'PayPal Payflow Pro iFrame';
$_['heading_refund']				= 'Remboursement';

//Table columns
$_['column_transaction_id']			= 'Identifiant de la transaction';
$_['column_transaction_type']		= 'Type de la transaction ';
$_['column_amount']					= 'Montant';
$_['column_time']					= 'Temps';
$_['column_actions']				= 'Actions';

// Text
$_['text_payment']					= 'Paiement';
$_['text_success']					= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails de votre compte PayPal Payflow Pro iFrame avec succ&egrave;s !';
$_['text_pp_payflow_iframe']		= '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL\');"><img src="view/image/payment/paypal.png" alt="PayPal Payflow Pro" title="PayPal Payflow Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Autorisation';
$_['text_sale']						= 'Vente';
$_['text_authorise']				= 'Autoriser';
$_['text_capture']					= 'Blocage retard&eacute;';
$_['text_void']						= 'Annulation';
$_['text_payment_info']				= 'Information de paiement';
$_['text_complete']					= 'Compl&egrave;te';
$_['text_incomplete']				= 'Incompl&egrave;te';
$_['text_transaction']				= 'Transaction';
$_['text_confirm_void']				= 'Si vous annulez vous ne pourrez pas bloquer d&#8217;autres fonds';
$_['text_refund']					= 'Remboursement';
$_['text_refund_issued']			= 'Le remboursement a &eacute;t&eacute; &eacute;mis avec succ&egrave;s';
$_['text_redirect']					= 'Redirection';
$_['text_iframe']					= 'Iframe';
$_['help_vendor']					= 'Identifiant marchand que vous avez cr&eacute;&eacute; lors de votre inscription sur le site de paiements pro.';
$_['help_user']						= 'Si vous configurez un ou plusieurs utilisateurs suppl&eacute;mentaires sur le compte, cette valeur sera l&#8217;identifiant de l&#8217;utilisateur autoris&eacute; &agrave; traiter les transactions. Si toutefois, vous n&#8217;avez pas d&eacute;fini d&#8217;autres utilisateurs sur le compte, l&#8217;utilisateur aura la m&ecirc;me valeur que l&#8217;identifiant du <b>vendeur</b>.';
$_['help_password']					= 'Mot de passe de 6 &agrave; 32 caract&egrave;res que vous avez d&eacute;fini lors de l&#8217;enregistrement du compte.';
$_['help_partner']					= 'Identifiant mis &agrave; votre disposition par le revendeur agr&eacute;&eacute; PayPal qui vous a enregistr&eacute; pour le SDK Payflow. Si vous avez achet&eacute; votre compte directement &agrave; partir de PayPal, utiliser PayPal';
$_['help_checkout_method']			= 'Veuillez utiliser la m&eacute;thode de redirection si n&#8217;avez pas de connexion SSL ou si vous n&#8217;avez pas pay&eacute; avec l&#8217;option PayPal d&eacute;sactiv&eacute;e sur votre page de paiement h&eacute;berg&eacute;e.';
$_['help_debug']					= 'Logs des informations suppl&eacute;mentaires.';

//Buttons
$_['button_refund']					= 'Remboursement';
$_['button_void']					= 'Annulation';
$_['button_capture']				= 'Blocage';

//Tabs
$_['tab_settings']					= 'Param&egrave;tres';
$_['tab_order_status']				= '&Eacute;tat de la commande';
$_['tab_checkout_customisation']	= 'Personnalisation de la commande';

//Form entry
$_['entry_vendor']					= 'Vendeur :';
$_['entry_user']					= 'Utilisateur :';
$_['entry_password']				= 'Mot de passe :';
$_['entry_partner']					= 'Partenaire :';
$_['entry_test']					= 'Mode de test :<br /><span class="help">Utilisez le serveur de la passerelle en direct ou en test (sandbox) pour traiter les transactions ? Le test peut &eacute;chouer avec Internet Explorer</span>';
$_['entry_total']					= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_order_status']			= '&Eacute;tat de la commande :';
$_['entry_geo_zone']				= 'Zone g&eacute;ographique :';
$_['entry_status']					= '&Eacute;tat :';
$_['entry_sort_order']				= 'Classement :';
$_['entry_transaction_method']		= 'Mode de transaction :';
$_['entry_transaction_id']			= 'Identifiant de transaction';
$_['entry_full_refund']				= 'Remboursement int&eacute;gral';
$_['entry_amount']					= 'Montant';
$_['entry_message']					= 'Message';
$_['entry_ipn_url']					= 'URL de l&#8217IPN :';
$_['entry_checkout_method']			= 'Mode de commande :';
$_['entry_debug']					= 'Mode de d&eacute;bogage :';
$_['entry_transaction_reference']	= 'R&eacute;f&eacute;rence de la transaction';
$_['entry_transaction_amount']		= 'Montant de la transaction';
$_['entry_refund_amount']			= 'Montant du remboursement';
$_['entry_capture_status']			= '&Eacute;tat du blocage';
$_['entry_void']					= 'Annulation';
$_['entry_capture']					= 'Blocage';
$_['entry_transactions']			= 'Transactions';
$_['entry_complete_capture']		= 'Blocage int&eacute;gral';
$_['entry_canceled_reversal_status']= 'Inversion annul&eacute; :';
$_['entry_completed_status']		= 'Compl&eacute; :';
$_['entry_denied_status']			= 'Refus&eacute; :';
$_['entry_expired_status']			= 'Expir&eacute; :';
$_['entry_failed_status']			= '&Eacute;chou&eacute; :';
$_['entry_pending_status']			= 'En attente :';
$_['entry_processed_status']		= 'En traitement :';
$_['entry_refunded_status']			= 'Rembours&eacute; :';
$_['entry_reversed_status']			= 'Revers&eacute; :';
$_['entry_voided_status']			= 'Annul&eacute; :';
$_['entry_cancel_url']				= 'Adresse (URL) d&#8217;annulation :';
$_['entry_error_url']				= 'Adresse (URL) d&#8217;erreur :';
$_['entry_return_url']				= 'Adresse (URL) de retour :';
$_['entry_post_url']				= 'Adresse (URL) de POST :';

//Errors
$_['error_permission']				= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>PayPal Website Paiement Pro iFrame (UK)</b> !';
$_['error_vendor']					= 'Attention, le vendeur est requis !'; 
$_['error_user']					= 'Attention, l&#8217;utilisateur est requis !'; 
$_['error_password']				= 'Attention, le mot de passe est requis !'; 
$_['error_partner']					= 'Attention, le partenaire est requis !'; 
$_['error_missing_data']			= 'Donn&eacute;es manquantes';
$_['error_missing_order']			= 'Commande non trouv&eacute;e';
$_['error_general']					= 'Il y a une erreur';
$_['error_capture_amt']				= 'Entrer un montant &agrave; bloquer';
?>