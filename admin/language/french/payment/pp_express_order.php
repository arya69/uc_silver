<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Text
$_['text_payment_info']				= 'Information du paiement';
$_['text_capture_status']			= '&Eacute;tat du blocage';
$_['text_amount_auth']				= 'Montant autoris&eacute;';
$_['text_amount_captured']			= 'Montant bloqu&eacute;';
$_['text_amount_refunded']			= 'Montant rembours&eacute;';
$_['text_capture_amount']			= 'Blocage du montant';
$_['text_complete_capture']			= 'Blocage complet';
$_['text_transactions']				= 'Transactions';
$_['text_complete']					= 'Compl&eacute;t&eacute;';
$_['text_confirm_void']				= 'Si vous annulez vous ne pourrez pas capturer d&#8217;autres fonds';
$_['text_view']						= 'Voir';
$_['text_refund']					= 'Rembourser';
$_['text_resend']					= 'Renvoyer';

// Column
$_['column_trans_id']				= 'Identifiant de la transaction';
$_['column_amount']					= 'Compte';
$_['column_type']					= 'Mode de paiement';
$_['column_status']					= '&Eacute;tat';
$_['column_pend_reason']			= 'Raison de l&#8217;attente';
$_['column_created']				= 'Cr&eacute;&eacute;';
$_['column_action']					= 'Action';

// Button
$_['btn_void']						= 'Annulation';
$_['btn_capture']					= 'Blocage';

// Message
$_['success_transaction_resent']	= 'La transaction a &eacute;t&eacute; renvoy&eacute; avec succ&egrave;s';

// Error
$_['error_capture_amt']				= 'Entrer un compte pour la capture';
$_['error_timeout']					= 'D&eacute;lai de la requ&ecirc;te d&eacute;pass&eacute;';
$_['error_transaction_missing']		= 'La transaction n&#8217;a pas &eacute;t&eacute; trouv&eacute;';
?>