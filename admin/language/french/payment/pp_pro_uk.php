<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'PayPal Website Paiement Pro (UK)';

// Text 
$_['text_payment']			= 'Paiement';
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du paiement <b>PayPal Website Paiement Pro (UK)</b> avec succ&egrave;s !';
$_['text_pp_pro_uk']		= '<a onclick="window.open(\https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\);"><img src="view/image/payment/paypal.png" alt="PayPal Website Paiement Pro (UK)" title="PayPal Website Paiement Pro (UK)" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']	= 'Autorisation';
$_['text_sale']				= 'Vente';

// Entry
$_['entry_username']		= 'Nom d&#8217;utilisateur de l&#8217;API :';
$_['entry_password']		= 'Mot de passe de l&#8217;API :';
$_['entry_signature']		= 'Signature de l&#8217;API :';
$_['entry_test']			= 'Mode de test :<br /><span class="help">Utilisez la passerelle du serveur r&eacute;elle ou de test (sandbox) pour traiter les transactions ?</span>';
$_['entry_transaction']		= 'Mode de transaction :';
$_['entry_total']			= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_order_status']	= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>PayPal Website Paiement Pro (UK)</b> !';
$_['error_vendor']			= 'Attention, le vendeur  est requis !'; 
$_['error_password']		= 'Attention, le mot de passe est requis !'; 
$_['error_partner']			= 'Attention, le partenaire est requis !'; 
?>