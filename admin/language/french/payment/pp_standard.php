<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']					= 'PayPal Standard';

// Text
$_['text_payment']					= 'Paiement';
$_['text_success']					= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du paiement <b>PayPal</b> avec succ&egrave;s !';
$_['text_pp_standard']				= '<a onclick="window.open(\'https://www.paypal.com/fr/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Autorisation';
$_['text_sale']						= 'Vente';

// Entry
$_['entry_email']					= 'Courriel :';
$_['entry_test']					= 'Mode de test :';
$_['entry_transaction']				= 'Mode de transaction :';
$_['entry_debug']					= 'Mode d&eacute;bogage :<br /><span class="help">Enregistre des informations suppl&eacute;mentaires dans le journal syst&egrave;me.</span>';
$_['entry_total']					= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_canceled_reversal_status']= 'Commande Retour :<br /><span class="help">Cela signifie qu&#8217;un renversement a &eacute;t&eacute; annul&eacute;. Par exemple, vous <i>(commer&ccedil;ant)</i> avez remport&eacute; un litige avec le client et les fonds pour cette op&eacute;ration qui avait &eacute;t&eacute; invers&eacute;e, vous ont &eacute;t&eacute; retourn&eacute;s.</span>';
$_['entry_completed_status']		= 'Commande Compl&egrave;t&eacute;e :<br />';
$_['entry_denied_status']			= 'Commande Refus&eacute;e :<br />';
$_['entry_expired_status']			= 'Commande Expir&eacute;e :<br />';
$_['entry_failed_status']			= 'Commande Echou&eacute;e :<br />';
$_['entry_pending_status']			= 'Commande En attente :<br />';
$_['entry_processed_status']		= 'Commande Trait&eacute;e :<br />';
$_['entry_refunded_status']			= 'Commande Rembours&eacute;e :<br />';
$_['entry_reversed_status']			= 'Commande Retourn&eacute;e :<br />';
$_['entry_voided_status']			= 'Commande Annul&eacute;e :';
$_['entry_geo_zone']				= 'Zone g&eacute;ographique :';
$_['entry_status']					= '&Eacute;tat :';
$_['entry_sort_order']				= 'Classement :';

// Error
$_['error_permission']				= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>PayPal</b> !';
$_['error_email']					= 'Attention, l&#8217;e-mail  est requis !'; 
?>