<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'LIQPAY';

// Text 
$_['text_payment']			= 'Paiement';
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails du paiement <b>LIQPAY</b> avec succ&egrave;s !';
$_['text_pay']				= 'LIQPAY';
$_['text_card']				= 'Carte de cr&eacute;dit';

// Entry
$_['entry_merchant']		= 'N&deg; d&#8217;identification marchand :';
$_['entry_signature']		= 'Signature:';
$_['entry_type']			= 'Type:';
$_['entry_total']			= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_order_status']	= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>LIQPAY</b> !';
$_['error_merchant']		= 'Attention, le N&deg; d&#8217;identification marchand  est requis !';
$_['error_signature']		= 'Attention, la signature est requise !';
?>