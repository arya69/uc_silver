<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'PayPal Payments Pro Payflow Edition';

// Text 
$_['text_payment']			= 'Paiement';
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails de votre compte <b>PayPal Direct (UK)</b> avec succ&egrave;s !';
$_['text_pp_pro_pf']		= '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Payments Pro Payflow Edition" title="PayPal Payments Pro Payflow Edition" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']	= 'Autorisation';
$_['text_sale']				= 'Vente';

// Entry
$_['entry_vendor']			= 'Vendeur :<br /><span class="help">Identifiant marchand que vous avez cr&eacute;&eacute; lors de votre inscription sur le site de paiements pro</span>';
$_['entry_user']			= 'Utilisateur :<br /><span class="help">Si vous configurez un ou plusieurs utilisateurs suppl&eacute;mentaires sur le compte, cette valeur sera l&#8217;identifiant de l&#8217;utilisateur autoris&eacute; &agrave; traiter les transactions. Si toutefois, vous n&#8217;avez pas d&eacute;fini d&#8217;autres utilisateurs sur le compte, l&#8217;utilisateur aura la m&ecirc;me valeur que l&#8217;identifiant du <b>vendeur</b></span>';
$_['entry_password']		= 'Mot de passe :<br /><span class="help">Mot de passe de 6 &agrave; 32 caract&egrave;res que vous avez d&eacute;fini lors de l&#8217;enregistrement du compte.</span>';
$_['entry_partner']			= 'Partenaire :<br /><span class="help">Identifiant mis &agrave; votre disposition par le revendeur agr&eacute;&eacute; PayPal qui vous a enregistr&eacute; pour le SDK Payflow. Si vous avez achet&eacute; votre compte directement &agrave; partir de PayPal, utiliser PayPal</span>';
$_['entry_test']			= 'Mode de test :<br /><span class="help">Utilisez la passerelle du serveur en mode r&eacute;el ou de test (sandbox) pour traiter les transactions ?</span>';
$_['entry_transaction']		= 'Mode de transaction :';
$_['entry_total']			= 'Total :<br /><span class="help">Montant total que la commande doit atteindre avant que ce mode de paiement ne devienne actif.</span>';
$_['entry_order_status']	= '&Eacute;tat de la commande :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>PayPal Website Paiement Pro iFrame (UK)</b> !';
$_['error_vendor']			= 'Attention, le bendeur est requis !'; 
$_['error_user']			= 'Attention, l&#8217;utilisateur est requis !'; 
$_['error_password']		= 'Attention, le mot de passe est requis !'; 
$_['error_partner']			= 'Attention, le partenaire est requis !'; 
?>