<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'Produits'; 

// Text
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les <b>Produits</b> avec succ&egrave;s !';
$_['text_plus']				= '+';
$_['text_minus']			= '-';
$_['text_default']			= 'Par d&eacute;faut';
$_['text_image_manager']	= 'Gestionnaire d&#8217;images';
$_['text_browse']			= 'Parcourir les fichiers';
$_['text_clear']			= 'Effacer l&#8217;image';
$_['text_option']			= 'Option';
$_['text_option_value']		= 'Valeur de l&#8217;option';
$_['text_percent']			= 'Pourcentage';
$_['text_amount']			= 'Montant fixe';

// Column
$_['column_name']			= 'Nom du produit';
$_['column_model']			= 'Mod&egrave;le';
$_['column_image']			= 'Image';
$_['column_price']			= 'Prix';
$_['column_quantity']		= 'Quantit&eacute;';
$_['column_status']			= '&Eacute;tat';
$_['column_action']			= 'Action';

// Entry
$_['entry_name']			= 'Nom du produit :';
$_['entry_meta_keyword']	= 'Balise m&eacute;ta "Mots-cl&eacute;s" :';
$_['entry_meta_description']= 'Balise m&eacute;ta "Description" :';
$_['entry_description']		= 'Description :';
$_['entry_store']			= 'Boutiques';
$_['entry_keyword']			= 'SEO "R&eacute;-&eacute;criture d&#8217;url" :<br /><span class="help">Mettre par ex. le nom de l&#8217;article. Pour activer, cocher <b>"Oui"</b> dans <b>"Utiliser des URL&#8217;s SEO"</b> situ&eacute; dans l&#8217;onglet : <b>Syst&egrave;me/Param&egrave;tres/Serveur</b>.</span>';
$_['entry_model']			= 'Mod&egrave;le :';
$_['entry_sku']				= 'R&eacute;f&eacute;rence "Gestion des stocks" :';
$_['entry_upc']				= 'Code universel des produits :<br /><span class="help">(Code-barres)</span>';
$_['entry_ean']				= 'EAN (European Article Numbering) :<br/><span class="help">Code barre normalis&eacute pour l&#8217;Europe</span>';
$_['entry_jan']				= 'JAN (Japanese Article Number) :<br/><span class="help">Code barre normalis&eacute pour le Japon</span>';
$_['entry_isbn']			= 'ISBN (International Standard Book Number):<br/><span class="help">Num&eacute;ro international normalis&eacute; du livre</span>';
$_['entry_mpn']				= 'MPN :<br/><span class="help">Num&eacute;ro de pi&egrave;ce du fabricant</span>';
$_['entry_location']		= 'Localisation :';
$_['entry_shipping']		= 'Livraison requise :'; 
$_['entry_manufacturer']	= 'Fabricant :';
$_['entry_date_available']	= 'Date de disponibilit&eacute; :';
$_['entry_quantity']		= 'Quantit&eacute;';
$_['entry_minimum']			= 'Quantit&eacute; minimum :<br /><span class="help">Forcer une quantit&eacute; minimum de commande</span>';
$_['entry_stock_status']	= '&Eacute;tat si rupture :';
$_['entry_price']			= 'Prix <acronym title="Hors Taxe">H.T.</acronym>';
$_['entry_tax_class']		= 'Type de taxe :';
$_['entry_points']			= 'Points :<br/><span class="help">Nombre de points n&eacute;cessaires &agrave; l&#8217;achat de cet article. Si vous ne souhaitez pas que ce produit soit achet&eacute; avec les points, laisser &agrave; 0.</span>';
$_['entry_option_points']	= 'Points ';
$_['entry_subtract']		= 'Soustraire du stock';
$_['entry_weight_class']	= 'Unit&eacute; de Poids :';
$_['entry_weight']			= 'Poids';
$_['entry_length']			= 'Unit&eacute; de longueur :';
$_['entry_dimension']		= 'Dimensions (L x l x H) :';
$_['entry_image']			= 'Image';
$_['entry_customer_group']	= 'Groupe client';
$_['entry_date_start']		= 'Date de d&eacute;but';
$_['entry_date_end']		= 'Date de fin';
$_['entry_priority']		= 'Priorit&eacute;';
$_['entry_attribute']		= 'Attribut';
$_['entry_attribute_group']	= 'Groupe d&#8217;attribut :';
$_['entry_text']			= 'Texte';
$_['entry_option']			= 'Option :';
$_['entry_option_value']	= 'Valeur de l&#8217;option';
$_['entry_required']		= 'Requis :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement';
$_['entry_category']		= 'Cat&eacute;gories :<br /><span class="help">(Compl&eacute;tion automatique)</span>';
$_['entry_filter']			= 'Filtres :<br /><span class="help">(Compl&eacute;tion automatique)</span>';
$_['entry_download']		= 'T&eacute;l&eacute;chargements :<br /><span class="help">(Compl&eacute;tion automatique)</span>';
$_['entry_related']			= 'Produits apparent&eacute;s :<br /><span class="help">(Compl&eacute;tion automatique)</span>';
$_['entry_tag']				= 'Balises du produit :<br /><span class="help">Elles doivent &ecirc;tre s&eacute;par&eacute;es par une virgule <i><small>(Etiquettes visibles en bas de la page du produit)</small></i></span>';
$_['entry_reward']			= 'Points de fid&eacute;lit&eacute;';
$_['entry_layout']			= 'Disposition';
$_['entry_profile']			= 'Profil';
$_['text_recurring_help']	= 'Les montants r&eacute;currents sont calcul&eacute;s par fr&eacute;quence et cycles.<br />Par exemple, si vous utilisez une fr&eacute;quence de � semaine � et un cycle de "2", l&#8217;utilisateur sera factur&eacute; toutes les 2 semaines.<br />La longueur est le nombre de fois que l&#8217;utilisateur effectuera un paiement, r&eacute;glez la longueur sur 0 si vous voulez payer jusqu&#8217;au solde des paiements.';
$_['text_recurring_title']	= 'Paiements r&eacute;currents ';
$_['text_recurring_trial']	= 'P&eacute;riode d&#8217;essai';
$_['entry_recurring']		= 'Facturation r&eacute;currente :';
$_['entry_recurring_price']	= 'Prix r&eacute;rent :';
$_['entry_recurring_freq']	= 'Fr&eacute;quence r&eacute;currente :';
$_['entry_recurring_cycle']	= 'Cycles r&eacute;rents :<span class="help">Nombre de fois de facturation, doit &ecirc;tre de 1 ou plus</span>';
$_['entry_recurring_length']= 'Longueur r&eacute;rente :<span class="help">0 = jusqu&#8217;au solde des paiements</span>';
$_['entry_trial']			= 'P&eacute;riode d&#8217;essai :';
$_['entry_trial_price']		= 'Premi&egravere instance de prix r&eacute;current :';
$_['entry_trial_freq']		= 'Premi&egravere instance de fr&eacute;quence r&eacute;currente :';
$_['entry_trial_cycle']		= 'Premi&egravere instance de cycles r&eacute;currents :<span class="help"Nombre de facturation, doit &ecirc;tre de 1 ou plus</span>';
$_['entry_trial_length']	= 'Premi&egravere instance de longueur r&eacute;currente :';

$_['text_length_day']		= 'Jour';
$_['text_length_week']		= 'Semaine';
$_['text_length_month']		= 'Mois';
$_['text_length_month_semi']= 'Mois Jumel&eacute;s';
$_['text_length_year']		= 'Ann&eacute;e';

// Error
$_['error_warning']			= 'Attention, veuillez v&eacute;rifier soigneusement le formulaire afin qu&#8217;il n&#8217;y ai pas d&#8217;erreurs !';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier les <b>Produits</b> !';
$_['error_name']			= 'Le <b>Nom du produit</b> doit &ecirc;tre compos&eacute; de 3 &agrave; 255 caract&egrave;res !';
$_['error_model']			= 'Le <b>Mod&egrave;le</b> doit &ecirc;tre compos&eacute; de 3 &agrave; 24 caract&egrave;res !';
?>