<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['heading_title']			= 'Profils';

$_['button_insert']			= 'Ins&eacute;rer';
$_['button_copy']			= 'Copier';
$_['button_delete']			= 'Supprimer';
$_['button_remove']			= 'd&eacute;placer';

$_['text_no_results']		= 'Aucun r&eacute;sultat !';
$_['text_remove']			= 'd&eacute;placer';
$_['text_edit']				= 'Editer';
$_['text_enabled']			= 'Activ&eacute;';
$_['text_disabled']			= 'D&eacute;sactiv&eacute;';
$_['text_success']			= 'Le profil a &eacute;t&eacute; ajout&eacute; avec succ&egrave;s !';
$_['text_day']				= 'Jour';
$_['text_week']				= 'Semaine';
$_['text_semi_month']		= 'Mois jumel&eacute;s';
$_['text_month']			= 'Mois';
$_['text_year']				= 'Ann&eacute;';

$_['entry_name']			= 'Nom :';
$_['entry_sort_order']		= 'Classement :';
$_['entry_price']			= 'Prix :';
$_['entry_duration']		= 'Dur&eacute;e :';
$_['entry_status']			= '&Eacute;tat :';
$_['entry_cycle']			= 'Cycle :';
$_['entry_frequency']		= 'Fr&eacute;quence :';
$_['entry_trial_price']		= 'Premi&egravere instance de prix :';
$_['entry_trial_duration']	= 'Premi&egravere instance de dur&eacute;e :';
$_['entry_trial_status']	= 'Premi&egravere instance d&#8217;&eacute;tat :';
$_['entry_trial_cycle']		= 'Premi&egravere instance de cycle :';
$_['entry_trial_frequency']	= 'Premi&egravere instance de fr&eacute;quence :';

$_['column_name']			= 'Nom';
$_['column_sort_order']		= 'Classement';
$_['column_action']			= 'Action';

$_['error_warning']			= 'Attention, veuillez v&eacute;rifier avec attention le journal d&#8217;erreurs !';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier les profils !';
$_['error_name']			= 'Le <b>Nom de profil</b> doit &ecirc:tre compris entre 3 et 255 caract&egrave;res !';

$_['text_recurring_help']	= 'Les montants r&eacute;currents sont calcul&eacute;s par fr&eacute;quence et cycles.<br />Par exemple, si vous utilisez une fr&eacute;quence de « semaine » et un cycle de "2", l&#8217;utilisateur sera factur&eacute; toutes les 2 semaines.<br />La longueur est le nombre de fois que l&#8217;utilisateur effectuera un paiement, r&eacute;glez la longueur sur 0 si vous voulez payer jusqu&#8217;au solde des paiements.';
?>