<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_heading_title']					= 'OpenBay Pro pour eBay | Param&egrave;tres';
$_['lang_openbay']							= 'OpenBay Pro';
$_['lang_ebay']								= 'eBay';
$_['lang_settings']							= 'Param&egrave;tres';
$_['lang_save']								= 'Sauvegarder';
$_['lang_cancel']							= 'Annuler';
$_['lang_yes']								= 'Oui';
$_['lang_update']							= 'Mise &agrave; jour';
$_['lang_no']								= 'Non';
$_['lang_import']							= 'Import';
$_['lang_clear']							= 'Nettoyer';
$_['lang_add']								= 'Ajouter';
$_['lang_remove']							= 'Supprimer';
$_['lang_load']								= 'Charger';
$_['lang_text_success']						= 'Vos Param&egrave;tresont bien &eacute;t&eacute; sauvegard&eacute;';

$_['lang_error_save_settings']				= 'Vous devez enregistrer vos paramètres en priorit&eacute;.';

$_['lang_tab_token']						= 'D&eacute;tails de l&#8217;API'; 
$_['lang_tab_general']						= 'Param&egrave;tres'; 
$_['lang_tab_setup']						= 'Param&egrave;tres'; 
$_['lang_tab_defaults']						= 'Annonces en nombre'; 
$_['lang_tab_shipping']						= 'Livraison defaults';

$_['lang_legend_api']						= 'D&eacute;tails de la connexion de l&#8217;API';
$_['lang_legend_app_settings']				= 'Param&egrave;tres de l&#8217;application';
$_['lang_legend_default_import']			= 'Param&egrave;tres de l&#8217;import par d&eacute;faut';
$_['lang_legend_payments']					= 'Paiements';
$_['lang_legend_returns']					= 'Retours';
$_['lang_legend_linkup']					= 'Liens article';
$_['lang_legend_usage']						= 'Appeler le support';
$_['lang_legend_subscription']				= 'Souscription';

$_['lang_error_oc_version']					= 'Version d&#8217;OpenCart non support&eacute;';

$_['lang_status']							= '&Eacute;tat';
$_['lang_enabled']							= 'Activ&eacute;';
$_['lang_disabled']							= 'D&eacute;sactiv&eacute;';
$_['lang_obp_token']						= 'Jeton';
$_['lang_obp_token_register']				= 'Cliquer ici pour vous inscrire &agrave; votre jeton';
$_['lang_obp_token_renew']					= 'Cliquer ici pour renouveler votre jeton';
$_['lang_obp_secret']						= 'Secret';
$_['lang_obp_string1']						= 'Cha&icirc;ne de cryptage 1';
$_['lang_obp_string2']						= 'Cha&icirc;ne de cryptage 2';
$_['lang_app_setting_msg']					= 'Vos param&egrave;tres d&#8217;application vous permettent de configurer la mani&egrave;re dont OpenBay Pro fonctionne et s&#8217;int&egrave;gre &agrave; votre syst&egrave;me.';
$_['lang_app_end_ebay']						= 'Fin des articles ?<span class="help">Si les articles se vendent ou si l&#8217;inscription se termine sur eBay?</span>';
$_['lang_app_relist_ebay']					= 'R&eacute;inscrire si en stock ?<span class="help">Si un lien d&#8217;article existait avant la remise en vente de l&#8217;article pr&eacute;c&eacute;dent si en stock</span>';
$_['lang_app_logging']						= 'Activer la journalisation';
$_['lang_app_currency']						= 'Devise par d&eacute;faut';
$_['lang_app_currency_msg']					= 'Bas&eacute; sur les devises de votre boutique';
$_['lang_app_cust_grp']						= 'Groupe client<span class="help">Lorsque de nouveaux clients sont cr&eacute;&eacute;s, doit on les ajouter &agrave; ce groupe ?';

$_['lang_app_stock_allocate']				= 'Stock allou&eacute;<span class="help">Lorsque le stock doit &ecirc;tre r&eacute;parti dans le magasin?</span>';
$_['lang_app_stock_1']						= 'Lorsque le client ach&egrave;te';
$_['lang_app_stock_2']						= 'Lorsque le client a pay&eacute;';
$_['lang_created_hours']					= 'Nouvelle limite d&#8217;&acirc;ge de commande<span class="help">Les commandes sont nouvelles quand cette limite n&#8217;est pas d&eacute;pass&eacute;e (72 en heures, par d&eacute;faut).</span>';

$_['lang_import_ebay_items']				= 'Import des articles eBay';

$_['lang_import_pending']					= 'Import des commandes impay&eacute;es';
$_['lang_import_def_id']					= 'Import de l&#8217;&eacute;tat par d&eacute;faut :';
$_['lang_import_paid_id']					= '&Eacute;tats des paiements :';
$_['lang_import_shipped_id']				= '&Eacute;tats des livraisons :';
$_['lang_import_cancelled_id']				= '&Eacute;tats des annulations:';
$_['lang_import_refund_id']					= '&Eacute;tats des remboursements :';
$_['lang_import_part_refund_id']			= '&Eacute;tats des remboursements partiels ';

$_['lang_developer']						= 'd&eacute;veloppeur';
$_['lang_developer_desc']					= 'Vous ne devriez pas utiliser cette zone, &agrave; moins d&#8217;y &ecirc;tre invit&eacute;.'; 
$_['lang_developer_empty']					= 'Vider tous les champs ?'; 
$_['lang_setting_desc']						= 'Les param&egrave;tres par d&eacute;faut sont utilis&eacute;s pour pr&eacute;-&eacute;tablir de nombreuses options d&#8217;eBay. Toutes les valeurs par d&eacute;faut peuvent &ecirc;tre modifi&eacute;s sur la page d&#8217;inscription pour chaque &eacute;l&eacute;ment.';
$_['lang_payment_instruction']				= 'Instructions de paiement';
$_['lang_payment_paypal']					= 'PayPal Accepted';
$_['lang_payment_paypal_add']				= 'Adresse courriel pour payPal';
$_['lang_payment_cheque']					= 'Ch&egrave;que accept&eacute;';
$_['lang_payment_card']						= 'Cartes accept&eacute;es';
$_['lang_payment_desc']						= 'Voir description (Ex. : transfert bancaire)';
$_['lang_payment_desc']						= 'Voir description (Ex. : transfert bancaire)';
$_['lang_payment_imediate']					= 'Paiement imm&eacute;diat requis';
$_['lang_tax_listing']						= 'Taxe du produit<span class="help">Si vous utilisez le taux appliqu&eacute; &agrave; l&#8217;annonce, assurez-vous que vos articles sur eBay ont la bonne taxe</span>';
$_['lang_tax_use_listing']					= 'Utiliser un taux d&#8217;imposition pour votre annonce sur eBay';
$_['lang_tax_use_value']					= 'Utilisez une valeur d&eacute;finie pour tout';
$_['lang_tax']								= 'Taxe en % utilis&eacute;e pour tout<span class="help">Utilis&eacute; lorsque vous importez des articles ou des commandes</span>';

$_['lang_ajax_dev_enter_pw']				= 'Veuillez entrer votre mot de passe administrateur';
$_['lang_ajax_dev_enter_warn']				= 'Cette action est dangereuse si le mot de passe est prot&eacute;g&eacute;.';

$_['lang_legend_notify_settings']			= 'Notification des Param&egrave;tres';
$_['lang_openbaypro_update_notify']         = 'Mise &agrave; jour des commandes<span class="help">Seulement pour les mises &agrave; jour automatiques</span>';
$_['lang_notify_setting_msg']               = 'Contr&ocirc;le lorsque les clients reçoivent des notifications de l&#8217;application. L&#8217;activation des emails de mise &agrave; jour peut am&eacute;liorer vos taux de DSR d&egrave;s que l&#8217;utilisateur reçoit des mises &agrave; jour sur leur commande.';
$_['lang_openbaypro_confirm_notify']        = 'Nouvelle commande - Acheteur<span class="help">Notifier &agrave; l&#8217;acheteur de tous nouveaux messages de commande par d&eacute;faut.</span>';
$_['lang_openbaypro_confirmadmin_notify']   = 'Nouvelle commande - Administrateur<span class="help">Notifier &agrave; l&#8217;administrateur de tous nouveaux messages de commande par d&eacute;faut.</span>';
$_['lang_openbaypro_brand_disable']         = 'D&eacute;sactiver le lien<span class="help">Supprime le lien OpenBay Pro dans les emails de commandes</span>';

$_['lang_listing_1day']						= '1 jour';
$_['lang_listing_3day']						= '3 jours';
$_['lang_listing_5day']						= '5 jours';
$_['lang_listing_7day']						= '7 jours';
$_['lang_listing_10day']					= '10 jours';
$_['lang_listing_30day']					= '30 jours';
$_['lang_listing_gtc']						= 'BJA - Bon jusqu&#8217;&agrave; l&#8217;annulation';
$_['lang_openbay_duration']					= 'Dur&eacute;e de l&#8217;annonce par d&eacute;faut<span class="help">BJA est disponible uniquement si vous avez une boutique eBay.</span>';
$_['lang_address_format']					= 'Format de l&#8217;adresse par d&eacute;faut<span class="help">Utilis&eacute; uniquement si le pays ne dispose pas de format d&#8217;adresse d&eacute;fini.</span>';

$_['lang_legend_stock_rep']					= 'Rapport sur le stock';
$_['lang_desc_stock_rep']					= 'Envoyer un courriel aux administrateurs p&eacute;riodiquement sur ​​l&#8217;&eacute;tat des &eacute;l&eacute;ments li&eacute;s. Cette fonctionnalit&eacute; est uniquement disponible sur certaines formules d&#8217;abonnement.';
$_['lang_stock_reports']					= 'Envoyer les rapports';
$_['lang_stock_summary']					= 'Envoyer un rapport complet<span class="help">Si cela est d&eacute;sactiv&eacute; un r&eacute;sum&eacute; seulement sera envoy&eacute;</span>';

$_['lang_api_status']						= '&Eacute;tat de la connexion API';
$_['lang_api_checking']						= 'V&eacute;rification';
$_['lang_api_ok']							= 'Connection OK, jeton expir&eacute;';
$_['lang_api_failed']						= 'Validation &eacute;chou&eacute;e';
$_['lang_api_connect_fail']					= '&Eacute;chec &agrave; la connexion';
$_['lang_api_connect_error']				= 'Erreur de connexion';

$_['lang_create_date']						= 'Date de cr&eacute;ation des nouvelles commandes';
$_['lang_create_date_0']					= 'Lorsque ajout&eacute; &agrave; OpenCart';
$_['lang_create_date_1']					= 'Lorsque cr&eacute;&eacute; sur eBay';
$_['lang_obp_detail_update']				= 'Cliquez ici pour mettre &agrave; jour l&#8217;URL de votre magasin et &ecirc;tre contact&eacute; par courriel';
$_['lang_developer_repairlinks']			= 'R&eacute;parer les liens de l&#8217;article';

$_['lang_timezone_offset']					= 'D&eacute;calage horaire<span class="help">Bas&eacute; sur les heures. 0 est le d&eacute;calage horaire GMT. Ne fonctionne que si le temps d&#8217;eBay est utilis&eacute; pour la cr&eacute;ation de la commande.</span>';
?>