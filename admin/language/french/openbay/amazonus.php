<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']				= 'Amazon (US)';
$_['lang_heading_title']		= 'OpenBay Pro pour Amazon US';

// Param&egrave;tres tab
$_['token_text']				= 'Jeton :';
$_['enc_string1_text']			= 'Cha&icirc;ne de cryptage 1';
$_['enc_string2_text']			= 'Cha&icirc;ne de cryptage 2';
$_['enabled_text']				= 'Autoris&eacute; :';
$_['yes_text']					= 'Oui';
$_['no_text']					= 'Non';
$_['save_text']					= 'Sauvegarder';

// Orders tab
$_['order_statuses_text']		= '&Eacute;tats des commandes';
$_['unshipped_text']			= 'Non livr&eacute;';
$_['partially_shipped_text']	= 'Livr&eacute; partiellement';
$_['shipped_text']				= 'Livr&eacute;';
$_['canceled_text']				= 'Annul&eacute;';
$_['import_tax_text']			= 'Taxe pour les articles import&eacute;s';
$_['import_tax_help']			= 'Utilis&eacute; si Amazon ne renvoie pas d&#8217;informations fiscales';
$_['other_text']				= 'Autre';
$_['customer_group_text']		= 'Groupe client';
$_['customer_group_help_text']	= 'S&eacute;lectionnez un groupe de clients &agrave; assigner aux commandes import&eacute;es';
$_['marketplaces_text']			= 'Places de march&eacute;';
$_['markets_text']				= 'S&eacute;lectionnez une place de march&eacute;s &agrave; partir duquel vous souhaitez importer vos commandes';
$_['de_text']					= "Allemagne";
$_['fr_text']					= "France";
$_['it_text']					= "Italie";
$_['es_text']					= "Espagne";
$_['uk_text']					= 'Angleterre';

// Subscription tab
$_['register_invite_text']		= 'Vous ne disposez pas d&#8217;information d&#8217;identification ? Inscrivez-vous maintenant pour les obtenir';
$_['register_text']				= 'Enregistrement';
$_['loading_text']				= 'Chargement';
$_['change_plans_text']			= 'Changer les plans';
$_['your_plan_text']			= 'Plan actuel et balance de votre compte';
$_['change_plan_text']			= 'Changer de plan';
$_['change_plans_help_text']	= 'Non satisfait de votre plan actuel ?';
$_['name_text']					= 'Nom';
$_['description_text']			= 'Description';
$_['price_text']				= 'Prix';
$_['order_frequency_text']		= 'Fr&eacute;quence d&#8217;importation des commandes';
$_['product_listings_text']		= 'Nouvelles listes de produits par mois';
$_['listings_remaining_text']	= 'Listes des produits restants';
$_['listings_reserved_text']	= 'Produits en cours de traitement';

// Listings tab
$_['tax_percentage_text']		= 'Pourcentage ajout&eacute; par d&eacute;faut au prix du produit';
$_['default_mp_text']			= 'Place de march&eacute; par d&eacute;faut pour les listes de produits et les recherches';

// Misc
$_['setttings_updated_text']	= 'Les Param&egrave;tres ont &eacute;t&eacute; mis &agrave; jour avec succ&egrave;s !';
$_['error_permission']			= 'Vous ne pouvez pas acc&eacute;der &agrave; ce module';
$_['settings_text']				= 'Param&egrave;tres principaux';
$_['subscription_text']			= 'Souscription';
$_['link_items_text']			= 'Lien articles';
$_['listing_text']				= 'Annonces';
$_['orders_text']				= 'Commandes';
?>