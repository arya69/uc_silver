<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_title']					= 'OpenBay Pro pour Amazon EU';
$_['lang_heading']					= 'Amazon EU vue d&#8217;ensemble';
$_['lang_overview']					= 'Amazon EU vue d&#8217;ensemble';
$_['lang_openbay']					= 'OpenBay Pro';
$_['lang_heading_settings']			= 'Param&egrave;tres';
$_['lang_heading_account']			= 'Mon compte';
$_['lang_heading_links']			= 'Liens des articles';
$_['lang_heading_bulk_listing']		= 'Annonces en nombre';
$_['lang_heading_register']			= 'Enregistrement';
$_['lang_heading_stock_updates']	= 'Mises &agrave; jour du stock';
$_['lang_heading_saved_listings']	= 'Annonces sauvegard&eacute;es';
$_['lang_heading_bulk_linking']		= 'Lier en nombre';
?>