<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Headings
$_['lang_title']						= 'Nouvelle annonce sur Amazon EU';
$_['lang_openbay']						= 'OpenBay Pro';
$_['lang_amazon']						= 'Amazon EU';

//Buttons
$_['button_search']						= 'Recherche';
$_['button_new']						= 'Cr&eacute;er un nouveau produit';
$_['button_return']						= 'Retourner aux produits';
$_['button_amazon_price']				= 'Obtenez le prix d&#8217;Amazon';
$_['button_list']						= 'Liste sur Amazon';

//Text
$_['text_products_sent']				= 'Products were sent for en traitement';
$_['text_view_on_amazon']				= 'View on Amazon';
$_['text_list']							= 'Liste';
$_['text_new']							= 'Neuf';
$_['text_used_like_new']				= 'Occasion - Comme neuf';
$_['text_used_very_good']				= 'Occasion - Tr&egrave;s bon &eacute;tat';
$_['text_used_good']					= 'Occasion - Bon &eacute;tat';
$_['text_used_acceptable']				= 'Occasion - Acceptable';
$_['text_collectible_like_new']			= 'Collection - Comme neuf';
$_['text_collectible_very_good']		= 'Collection - Tr&egrave;s bon &eacute;tat';
$_['text_collectible_good']				= 'Collection - Bon &eacute;tat';
$_['text_collectible_acceptable']		= 'Collection - Acceptable';
$_['text_refurbished']					= 'Restaur&eacute;';
$_['help_sku']							= 'ID unique du produit assign&eacute; par le marchand';
$_['help_restock_date']					= 'Il s&#8217;agit de la date &agrave; laquelle vous serez en mesure d&#8217;exp&eacute;dier les articles au client. Cette date ne doit pas &ecirc;tre plus de 30 jours &agrave; compter de la date indiqu&eacute;e ou les commandes reçues automatiquement peuvent &ecirc;tre annul&eacute;es.';
$_['help_sale_price']					= 'Le prix ​​de vente doit avoir une date de d&eacute;but et une date de fin';
$_['lang_not_in_catalog']				= 'Ou, si ce n&#8217;est pas dans le catalogue&nbsp;&nbsp;&nbsp;';

//Table columns
$_['column_image']						= 'Image';
$_['column_asin']						= 'ASIN';
$_['column_price']						= 'Prix';
$_['column_action']						= 'Action';
$_['column_name']						= 'Nom du produit';
$_['column_model']						= 'Mod&egrave;le';
$_['column_combination']				= 'Combinaison';
$_['column_sku']						= 'R&eacute;f&eacute;rence SKU';
$_['column_amazon_sku']					= 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon';

//Form entry
$_['entry_sku']							= 'R&eacute;f&eacute;rence SKU :';
$_['entry_condition']					= '&Eacute;tat de l&#8217;article :';
$_['entry_condition_note']				= 'Note sur l&#8217;&eacute;tat :';
$_['entry_price']						= 'Prix :';
$_['entry_sale_price']					= 'Prix de vente :';
$_['entry_quantity']					= 'Quantit&eacute; :';
$_['entry_start_selling']				= 'Disponible &agrave; partir de :';
$_['entry_restock_date']				= 'Date de r&eacute;-approvisionnement :';
$_['entry_country_of_origin']			= 'Pays d&#8217;origine :';
$_['entry_release_date']				= 'Date de sortie :';
$_['entry_from']						= 'Date &agrave; partir de';
$_['entry_to']							= 'Date jusqu&#8217;&agrave;';

// Form input place holders
$_['lang_placeholder_search']			= 'Entrer le nom du produit, UPC, EAN, ISBN ou ASIN';
$_['lang_placeholder_condition']		= 'Utilisez cet emplacement pour d&eacute;crire l&#8217;&eacute;tat de vos produits.';

// Tab headers
$_['item_links_header_text']			= 'Liens des articles';
$_['quick_listing_header_text']			= 'Annonce rapide';
$_['advanced_listing_header_text']		= 'Annonce avanc&eacute;e';
$_['saved_header_text']					= 'Annonces sauvegard&eacute;es';
$_['lang_tab_main']						= 'Page principale';

// Tabs
$_['item_links_tab_text']				= 'Liens des articles';
$_['quick_listing_tab_text']			= 'Annonce rapide';
$_['advanced_listing_tab_text']			= 'Annonce avanc&eacute;e';
$_['saved_tab_text']					= 'Annonces sauvegard&eacute;es';
$_['tab_required_info']					= 'Information requise';
$_['tab_additional_info']				= 'Options additionnelles';

// Quick/advanced listing tabs
$_['quick_listing_description']			= 'Utilisez cette m&eacute;thode lorsque vous comparez le catalogue de produits existant d&eacute;j&agrave; dans Amazon. La correspondance sera trouv&eacute;e en utilisant l&#8217;ID standard du produit sur Amazon (ISBN, UPC, EAN)';
$_['advanced_listing_description']		= 'Utilisez cette m&eacute;thode pour cr&eacute;er de nouvelles annonces sur Amazon.';
$_['listing_row_text']					= 'Annonce pour le produit :';
$_['already_saved_text']				= 'Ce produit se trouve d&eacute;j&agrave; dans les annonces sauvegard&eacute;es. Cliquez sur Modifier si vous souhaitez v&eacute;rifier.';
$_['save_button_text']					= 'Sauvegarder';
$_['save_upload_button_text']			= 'Enregistrer et t&eacute;l&eacute;charger';
$_['saved_listings_button_text']		= 'Voir les annonces sauvegard&eacute;es';
$_['cancel_button_text']				= 'Annuler';
$_['field_required_text']				= 'Le champ est requis !';
$_['not_saved_text']					= 'L&#8217;annonce n&#8217;a pas &eacute;t&eacute; enregistr&eacute;. V&eacute;rifiez votre entr&eacute;e.';
$_['chars_over_limit_text']				= 'D&eacute;passement de la limite de caract&egrave;res autoris&eacute;s.';
$_['minimum_length_text']				= 'La longueur minimun est';
$_['characters_text']					= 'caract&egrave;res';
$_['delete_confirm_text']				= '&Ecirctes-vous sûr';
$_['clear_image_text']					= 'Nettoyer';
$_['browse_image_text']					= 'Parcourir';
$_['category_selector_field_text']		= 'Cat&eacute;gorie sur Amazon :';

//Item links tab
$_['item_links_description']			= 'Ici vous pouvez ajouter et modifier les liens des articles existants sur Amazon sans les &eacute;num&eacute;rer &agrave; partir OpenCart. Cela permettra aux actions de contr&ocirc;le entre les march&eacute;s permis. Si vous avez install&eacute; openStock - Cela vous permettra de lier &eacute;galement les options d&#8217;un seul SKU d&#8217;article sur Amazon. (T&eacute;l&eacute;chargement produits &agrave; partir OpenCart &agrave; Amazon va automatiquement ajouter des liens)';
$_['new_link_table_name']				= 'Nouveau lien';
$_['new_link_product_column']			= 'Produit';
$_['new_link_sku_column']				= 'R&eacute;f&eacute;rence SKU';
$_['new_link_amazon_sku_column']		= 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon';
$_['new_link_action_column']			= 'Action';
$_['item_links_table_name']				= 'Liens des articles';

// Marketplaces
$_['marketplaces_field_text']			= 'Place de march&eacute;';
$_['marketplaces_help']					= 'Vous pouvez choisir la place de march&eacute; par d&eacute;faut dans les param&egrave;tres d&#8217;extension d&#8217;Amazon';
$_['text_germany']						= 'Allemagne';
$_['text_france']						= 'France';
$_['text_italy']						= 'Italie';
$_['text_spain']						= 'Espagne';
$_['text_united_kingdom']				= 'Angleterre';

// Saved listings tab
$_['saved_listings_description']		= 'Il s&#8217;agit de la liste des annonces de produits enregistr&eacute;es localement et qui sont pr&ecirc;ts &agrave; &ecirc;tre t&eacute;l&eacute;charg&eacute; sur Amazon. Cliquez sur T&eacute;l&eacute;charger pour les poster.';
$_['actions_edit_text']					= 'Editer';
$_['actions_remove_text']				= 'Supprimer';
$_['upload_button_text']				= 'T&eacute;l&eacute;charger';
$_['name_column_text']					= 'Nom';
$_['model_column_text']					= 'Mod&egrave;le';
$_['sku_column_text']					= 'R&eacute;f&eacute;rence SKU';
$_['amazon_sku_column_text']			= 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon';
$_['actions_column_text']				= 'Action';
$_['saved_localy_text']					= 'Annonces sauvegard&eacute;es localement.';
$_['uploaded_alert_text']				= 'Annonces sauvegard&eacute;es t&eacute;l&eacute;charg&eacute;es !';
$_['upload_failed']						= 'L&#8217;ajout du produit avec ce SKU a &eacute;chou&eacute; : %s. Raison : %s Processus de T&eacute;l&eacute;chargement Annul&eacute;.';

//Item links
$_['links_header_text']					= 'Liens des articles';
$_['links_desc1_text']					= 'Lier vos articles permet un contr&ocirc;le du stock de vos annonces sur Amazon.<br /> Pour chaque article dont le stock est mis &agrave; jour en local (stock disponible dans votre magasin OpenCart) Amazon mettra &agrave; jour votre annonce';
$_['links_desc2_text']					= 'Vous pouvez lier manuellement des articles en entrant sur Amazon La r&eacute;f&eacute;rence SKU et le nom du produit ou charger tous les produits non li&eacute;s sur Amazon puis entrer leur r&eacute;f&eacute;rence. (Le t&eacute;l&eacute;chargement des produits depuis OpenCart vers Amazon va automatiquement ajouter les liens correspondants)';
$_['links_load_btn_text']				= 'Cgargement';
$_['links_new_link_text']				= 'Nouveau lien';
$_['links_autocomplete_product_text']	= 'Produit<span class="help">(Autocompl&eacute;tion depuis le nom)</span>';
$_['links_amazon_sku_text']				= 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon';
$_['links_action_text']					= 'Action';
$_['links_add_text']					= 'Ajouter';
$_['links_add_sku_tooltip']				= 'Ajouter un nouveau SKU';
$_['links_remove_text']					= 'Supprimer';
$_['links_linked_items_text']			= 'Liens li&eacute;s';
$_['links_unlinked_items_text']			= 'Liens d&eacute;li&eacute;s';
$_['links_name_text']					= 'Nom';
$_['links_model_text']					= 'Mod&egrave;le';
$_['links_sku_text']					= 'R&eacute;f&eacute;rence SKU';
$_['links_amazon_sku_text']				= 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon';
$_['links_sku_empty_warning']			= 'La r&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon ne peut pas &ecirc;tre vide !';
$_['links_name_empty_warning']			= 'Le nom du produit ne peut pas &ecirc;tre vide !';
$_['links_product_warning']				= 'Produit inexistant. Veuillez utiliser les valeurs de saisie semi-automatique.';
$_['option_default']					= '-- S&eacute;lectionner une option --';

// Listing edit page
$_['text_edit_heading']					= 'Vue d&#8217;ensemble des annonces';
$_['text_has_saved_listings']			= 'Ce produit a une ou plusieurs annonces enregistr&eacute;es localement.';
$_['text_product_links']				= 'Liens des produits';
$_['button_create_new_listing']			= 'Cr&eacute;er une nouvelle annonce';
$_['button_remove_links']				= 'Supprimer les liens';
$_['button_saved_listings']				= 'Voir les annonces sauvegard&eacute;es';

// Errors
$_['lang_error_load_nodes']				= 'Impossible de charger les n&oelig;uds de navigation';
$_['text_error_connecting']				= 'Attention : Il y a un probl&egrave;me de connexion aux serveurs de l&#8217;API Welford m&eacute;dias. Veuillez v&eacute;rifier vos param&egrave;tres de l&#8217; extension OpenBay Pro Amazon. Si le probl&egrave;me persiste, veuillez contacter le support Welford.';
$_['error_text_missing']				= 'Vous devez entrer des informations de recherche.';
$_['error_data_missing']				= 'Les donn&eacute;es requises sont manquantes';
$_['error_missing_asin']				= 'ASIN est manquant';
$_['error_marketplace_missing']			= 'Veuillez s&eacute;lectionner une place de march&eacute;';
$_['error_condition_missing']			= 'Veuillez s&eacute;lectionner un &eacute;tat pour le produit';
$_['error_fetch']						= 'Impossible d&#8217;obtenir les donn&eacute;es';
$_['error_amazon_price']				= 'Impossible d&#8217;obtenir le prix sur Amazon';
$_['error_stock']						= 'Vous ne pouvez pas mettre un article ayant moins de 1 en quantit&eacute; en stock';
$_['error_sku']							= 'Vous devez entrer un SKU pour cet article';
$_['error_price']						= 'Vous devez entrer un prix pour cet article';
$_['error_sending_products']			= 'Impossible d&#8217;envoyer les produits pour les annonces. Veuillez contacter le support OpenBay Pro';
$_['lang_no_results']					= 'Aucun r&eacute;sultat';
$_['error_no_products_selected']		= 'Aucun produit n&#8217;a &eacute;t&eacute; s&eacute;lectionn&eacute; dans les annonces.';

// Messages
$_['text_product_sent']					= 'Le produit a &eacute;t&eacute; envoy&eacute; avec succ&egrave;s &agrave; Amazon.';
$_['text_product_not_sent']				= 'Le produit n&#8217;a pas &eacute;t&eacute; envoy&eacute; &agrave; Amazon pour la raison suivante : %s';
$_['text_links_removed']				= 'Liens des produits sur Amazon supprim&eacute;s';
?>