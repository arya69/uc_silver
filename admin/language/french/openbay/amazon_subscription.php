<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_title']                    = 'OpenBay Pro pour Amazon | Mon compte';
$_['lang_my_account']               = 'Mon compte';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon vue d&#8217;ensemble';
$_['lang_my_account']               = 'Mon compte';
$_['lang_btn_return']               = 'Annuler';
$_['lang_current_plan']             = 'Plan actuel';
$_['lang_register_invite']          = 'Vous n&#8217;vez pas les pouvoirs. Inscrivez-vous pour les obtenir';
$_['lang_register']                 = 'Enregistrement';
$_['lang_loading']                  = 'Chargement';
$_['lang_change_plans']             = 'Changer les plans';
$_['lang_your_plan']                = 'Votre plan actuel et le solde de votre compte';
$_['lang_change_plan']              = 'Changer le plan';
$_['lang_change_plans_help']        = 'Non satisfait de votre plan actuel ?';
$_['lang_name']                     = 'Nom';
$_['lang_description']              = 'Description';
$_['lang_price']                    = 'Prix';
$_['lang_order_frequency']          = 'Fr&eacute;quence d&#8217;importation';
$_['lang_product_listings']         = 'Nouvelles listes de produits par mois';
$_['lang_listings_remaining']       = 'Liste des produits restants';
$_['lang_listings_reserved']        = 'Produits en cours de traitement';
$_['lang_account_status']           = '&Eacute;tat du compte';
$_['lang_merchantid']               = 'ID marchand';
$_['lang_change_merchantid']        = 'Changer';
$_['lang_bulk_listing']             = 'Annonces en nombre';
$_['lang_allowed']                  = 'Autoris&eacute;';
$_['lang_not_allowed']              = 'Non autoris&eacute;';
?>