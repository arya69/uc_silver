<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Headings
$_['lang_title']                    = 'OpenBay Pro pour Amazon US | Mises &agrave; jour du stock';

//Text
$_['lang_stock_updates']            = 'Mises &agrave; jour du stock';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon US vue d&#8217;ensemble';
$_['lang_my_account']               = 'Mon compte';
$_['lang_btn_return']               = 'Annuler';

//Table columns
$_['lang_ref']                      = 'R&eacute;f&eacute;rence';
$_['lang_date_requested']           = 'Date souhait&eacute;e';
$_['lang_date_updated']             = 'Date de mise &agrave; jour';
$_['lang_status']                   = '&Eacute;tat';
$_['lang_sku']                      = 'R&eacute;f&eacute;rence SKU sur Amazon US';
$_['lang_stock']                    = 'Stock';

//Table headings
$_['lang_empty']                    = 'Aucun r&eacute;sultat !';
$_['lang_date_start']               = 'Date de d&eacute;but :';
$_['lang_date_end']                 = 'Date de fin :';
$_['lang_filter_btn']               = 'Filtrer';
?>