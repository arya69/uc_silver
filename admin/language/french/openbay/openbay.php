<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']						= 'eBay';

// Install actions
$_['text_install']                      = 'Installer';
$_['text_edit']                         = 'Editer';
$_['text_uninstall']                    = 'D&eacute;sinstaller';

// Status
$_['text_enabled']                      = 'Activ&eacute;';
$_['text_disabled']                     = 'D&eacute;sactiv&eacute;';

// Messages
$_['error_category_nosuggestions']      = 'Impossible de charger les cat&eacute;gories propos&eacute;es';
$_['lang_text_success']                 = 'Vous avez enregistr&eacute; vos modifications &agrave; l&#8217;extension eBay';
?>