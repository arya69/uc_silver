<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_heading']                      = 'Import d&#8217;article';
$_['lang_openbay']                      = 'OpenBay Pro';
$_['lang_page_title']                   = 'OpenBay Pro pour eBay';
$_['lang_ebay']                         = 'eBay';
$_['lang_btn_return']                   = 'Retour';
$_['lang_sync_import_line1']            = '<strong>Attention !</strong> Cela importera tous vos produits eBay et construira une structure de cat&eacute;gorie dans votre magasin. Il est conseill&eacute; de supprimer toutes les cat&eacute;gories et les produits avant d&#8217;ex&eacute;cuter cette option.<br />La structure de cat&eacute;gorie des cat&eacute;gories d&#8217;eBay normales, pas les cat&eacute;gories de votre boutique (si vous avez une boutique eBay). Vous pouvez renommer, supprimer et modifier les cat&eacute;gories import&eacute;es sans affecter vos produits eBay.';
$_['lang_sync_import_line2']            = 'Cette option utilise un grand nombre d&#8217;appel &agrave; l&#8217;API, un pour chaque produit ainsi qu&#8217;un appel suppl&eacute;mentaire toutes les 20 articles.';
$_['lang_sync_import_line3']            = 'Vous devez vous assurer que votre serveur peut accepter et traiter les grandes tailles de donn&eacute;es POST. 1000 objets eBay est d&#8217:environ 40 Mo en taille, vous aurez besoin de calculer ce que vous d&eacute;sirez. Si votre appel &eacute;choue, alors il est probable que votre r&eacute;glage est trop faible. Votre limite de m&eacute;moire de PHP doit &ecirc;tre d&#8217;environ 128Mb.';
$_['lang_sync_server_size']             = 'Actuellement votre serveur peut accepter : ';
$_['lang_sync_memory_size']             = 'Votre limite de m&eacute;moire PHP : ';
$_['lang_sync_item_description']        = 'Descriptions importation d&#8217;articles<span class="help">Tout sera import&eacute;<br /> y compris HTML, compteurs etc...</span>';
$_['lang_sync_notes_location']          = 'Importer des notes eBay pour des donn&eacute;es de localisation';
$_['lang_import_ebay_items']            = 'Importer les articles eBay';
$_['lang_import']                       = 'Import';
$_['lang_error_validation']             = 'Vous devez vous inscrire pour votre cl&eacute; API et activer le module.';
$_['lang_ajax_import_confirm']          = 'Cela va importer tous vos objets eBay ainsi que de nouveaux produits, &ecirc;tes-vous sûr ? Cela ne peut pas &ecirc;tre annul&eacute;! Assurez-vous d&#8217;avoir d&#8217;abord une sauvegarde!';
$_['lang_ajax_import_notify']           = 'Votre demande d&#8217;importation a &eacute;t&eacute; envoy&eacute; pour traitement. Une importation dure environ 1 heure par 1000 articles.';
$_['lang_ajax_load_error']              = 'Impossible de se connecter au serveur';
$_['lang_maintenance_fail']             = 'Votre bourique est en maintenance. L&#8217;mportation &eacute;chouera !';
$_['lang_import_images_msg1']           = 'Les images sont en cours d&#8217;importation et de copie sur eBay. Actualisez cette page, si le nombre ne diminue pas';
$_['lang_import_images_msg2']           = 'cliquer ici';
$_['lang_import_images_msg3']           = 'et patienter. Plus d&#8217;informations sur les raisons de ce qui s&#8217;est pass&eacute; peut &ecirc;tre trouv&eacute; sur<a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=8_45" target="_blank">here</a>';
?>