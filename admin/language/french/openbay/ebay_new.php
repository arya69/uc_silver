<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_page_title']               = 'eBay listing';
$_['lang_cancel']                   = 'Annuler';
$_['lang_none']                     = 'Aucun';
$_['lang_preview']                  = 'Aper&ccedil;u';
$_['lang_pixels']                   = 'Pixels';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';
$_['lang_add']                      = 'Ajouter';
$_['lang_remove']                   = 'Supprimer';
$_['lang_save']                     = 'Sauvegarder';
$_['lang_other']                    = 'Autres';
$_['lang_select']                   = 'S&eacute;lectionner';
$_['lang_loading']                  = 'Chargement';
$_['lang_confirm_action']           = '&Ecirc;tes-vous s&ucirc;r ?';

$_['lang_return']                   = 'Retour aux produits';
$_['lang_view']                     = 'Voir les annonces';
$_['lang_edit']                     = 'Editer';

$_['lang_catalog_pretext']          = 'Cet onglet affiche tous les r&eacute;sultats pour les &eacute;l&eacute;ments trouv&eacute;s dans le catalogue eBay - s&eacute;lectionnez votre premi&egrave;re cat&eacute;gorie.';
$_['lang_feature_pretext']          = 'Cet onglet affiche toutes les caract&eacute;ristiques des articles disponibles - s&eacute;lectionnez votre premi&egrave;re cat&eacute;gorie.';

$_['lang_ajax_noload']              = 'D&eacute;sol&eacute;, mais il est impossible de se connecter';
$_['lang_ajax_catproblem']          = 'Vous devez r&eacute;gler votre probl&egrave;me de cat&eacute;gorie avant de lister. Essayez de les re-synchroniser dans la zone du module de l&#8217;administration.';
$_['lang_ajax_item_condition']      = '&Eacute;tat de l&#8217;article';
$_['lang_ajax_error_cat']           = 'Veuillez choisir une cat&eacute;gorie eBay';
$_['lang_ajax_error_sku']           = 'Vous ne pouvez pas soumettre un produit sans SKU';
$_['lang_ajax_error_name']          = 'Vous ne pouvez pas soumettre un produit sans nom';
$_['lang_ajax_error_name_len']      = 'Le nom du produit doit &ecirc;tre compos&eacute; de moins de 80 caract&egrave;res';
$_['lang_ajax_error_loc']           = 'Entrer un code postal pour la localisation de l&#8217;article';
$_['lang_ajax_error_time']          = 'Entrer un d&eacute;lai de livraison';
$_['lang_ajax_error_nat_svc']       = 'Ajouter au moins un service de livraison';
$_['lang_ajax_error_stock']         = 'Vous devez avoir en stock un article de la liste';
$_['lang_ajax_error_duration']      = 'S&eacute;lectionner une dur&eacute;e pour l&#8217;annonce et s&eacute;lectionner la cat&eacute;gorie pour charger ces options';
$_['lang_ajax_image_size']          = 'Assurez-vous que vous avez une taille pour les images de la galerie ainsi que pour les vignettes';
$_['lang_ajax_duration']            = 'S&eacute;lectionner une dur&eacute;e pour l&#8217;annonce';
$_['lang_ajax_noimages']            = 'Etes-vous sûr de ne pas vouloir ajouter d&#8217;images sur eBay ?';
$_['lang_ajax_mainimage']           = 'Vous devez choisir une image principale &agrave; partir de votre s&eacute;lection d&#8217;images sur eBay';

//Tabs
$_['lang_tab_general']              = 'Cat&eacute;gorie';
$_['lang_tab_feature']              = 'Fonctionnalit&eacute;s';
$_['lang_tab_description']          = 'Description';
$_['lang_tab_images']               = 'Th&egrave;me &amp; galerie';
$_['lang_tab_price']                = 'Prix &amp; d&eacute;tails';
$_['lang_tab_payment']              = 'Paiement';
$_['lang_tab_shipping']             = 'Livraison';
$_['lang_tab_returns']              = 'Retours';

//Category
$_['lang_category']                 = 'Cat&eacute;gorie';
$_['lang_category_suggested']       = 'Cat&eacute;gorie sugg&eacute;r&eacute; pour eBay';
$_['lang_category_suggested_help']  = 'Bas&eacute; sur votre titre';
$_['lang_category_suggested_check'] = 'V&eacute;rification des cat&eacute;gories propos&eacute;es par eBay, veuillez patienter';
$_['lang_category_popular']         = 'Cat&eacute;gories recherch&eacute;es';
$_['lang_category_popular_help']    = 'Bas&eacute; sur votre historique';
$_['lang_category_checking']        = 'V&eacute;rification des exigencesde de la cat&eacute;gorie sur eBay, veuillez patienter';
$_['lang_category_features']        = 'Caract&eacute;ristiques de l&#8217;article<span class="help">La saisie de d&eacute;tails au sujet de votre article aidera les acheteurs &agrave; affiner leur recherche sur le produit exact dont ils ont besoin. Cela peut &eacute;galement am&eacute;liorer les performances du produit et eBay peut marquer une meilleure correspondance de valeur plus &eacute;lev&eacute;e.</span>';

//Description
$_['lang_title']                    = 'Titre';
$_['lang_title_error']				= 'Votre titre doit &ecirc;tre de 80 caract&egrave;res maximum';
$_['lang_subtitle']					= 'Sous-titre';
$_['lang_subtitle_help']			= 'eBay facturera des frais suppl&eacute;mentaires pour un sous-titre - <a href="http://pages.ebay.co.uk/help/sell/seller-fees.html" target="_BLANK" onclick="subtitleRefocus();"> cliquer ici pour les drais</a>';
$_['lang_template']					= 'Th&egrave;me';
$_['lang_template_link']			= 'Besoin d&#8217;un th&egrave;me personnalis&eacute; ?';
$_['lang_description']				= 'Description';

//Images
$_['lang_images_text_1']			= 'eBay images seront t&eacute;l&eacute;charg&eacute;es sur eBay et engendrera des frais suppl&eacute;mentaires.<br />Supersize et galerie Plus engendreront &eacute;galement des frais suppl&eacute;mentaires.';
$_['lang_images_text_2']			= 'Les mod&egrave;les d&#8217;images seront ajout&eacute;es &agrave; la description de votre annonce et h&eacute;berg&eacute;es sur votre site web, ils sont gratuits. (Votre mod&egrave;le d&#8217;annonce doit avoir le tag {gallery})';
$_['lang_image_gallery']			= 'Taille de l&#8217;image Galerie';
$_['lang_image_thumb']				= 'Taille de la vignette';
$_['lang_template_image']			= 'Image du th&egrave;me graphique';
$_['lang_main_image_ebay']			= 'Image principale eBay';
$_['lang_image_ebay']				= 'Image eBay';
$_['lang_images_none']				= 'Il n&#8217;y a aucune image pour ce produit';
$_['lang_images_supersize']			= 'Images Supersize<span class="help">Tr&egrave;s grandes photos</span>';
$_['lang_images_gallery_plus']		= 'Galerie plus<span class="help">Grande photo dans les r&eacute;sultats de recherche</span>';

//Price and details
$_['lang_listing_condition']		= '&Eacute;tat de l&#8217;article';
$_['lang_listing_duration']			= 'Dur&eacute;e de parution de l&#8217;annonce';
$_['lang_listing_1day']				= '1 jour';
$_['lang_listing_3day']				= '3 jours';
$_['lang_listing_5day']				= '5 jours';
$_['lang_listing_7day']				= '7 jours';
$_['lang_listing_10day']			= '10 jours';
$_['lang_listing_30day']			= '30 jours';
$_['lang_listing_gtc']				= 'Jusqu&#8217;&agrave; l&#8217;annulation';
$_['lang_stock_matrix']				= 'Matrice de stock';
$_['lang_stock_col_qty_total']		= 'En stock';
$_['lang_stock_col_qty']			= 'Obtention de la liste';
$_['lang_stock_col_qty_reserve']	= 'R&eacute;serv&eacute;';
$_['lang_stock_col_comb']			= 'Combinaison';
$_['lang_stock_col_price']			= 'Prix';
$_['lang_stock_col_enabled']		= 'Activ&eacute;';
$_['lang_qty']						= 'Quantit&eacute; par annonce<span class="help">Entrer un montant inf&eacute;rieur si vous voulez maintenir un niveau de stock inf&eacute;rieur sur eBay</span>';
$_['lang_price_ex_tax']				= 'Prix HT';
$_['lang_price_ex_tax_help']		= 'Prix standard de votre article hors taxes. Cette valeur n&#8217;est pas envoy&eacute;e &agrave; eBay.';
$_['lang_price_inc_tax']			= 'Prix TTC';
$_['lang_price_inc_tax_help']		= 'C&#8217;est la valeur qui sera envoy&eacute;e &agrave; eBay et que les utilisateurs devront r&eacute;gler.';
$_['lang_tax_inc']					= 'Taxe incluse';
$_['lang_offers']					= 'Permettre aux acheteurs de faire des offres';
$_['lang_private']					= 'Vente aux ench&egrave;res priv&eacute;e (cacher les noms des acheteurs)';
$_['lang_imediate_payment']			= 'Paiement imm&eacute;diat n&eacute;cessaire ?';
$_['lang_payment']					= 'Paiements accept&eacute;s';
$_['lang_payment_pp_email']			= 'Courriel pour le paiement PayPal :';
$_['lang_payment_instruction']		= 'Instructions de paiement';

//Shipping tab
$_['lang_item_postcode']			= 'Code postal d&#8217;emplacement<span class="help">Un code postal aidera eBay &agrave; choisir un emplacement appropri&eacute; pour votre annoce.</span>';
$_['lang_item_location']			= 'Ville ou r&eacute;gion d&#8217;emplacement<span class="help">La saisie d&#8217;une ville est moins fiable qu&#8217;un code postal</span>';
$_['lang_despatch_time']			= 'D&eacute;lai d&#8217;exp&eacute;dition<span class="help">Maximun de jours pour l&#8217;envoi</span>';
$_['lang_despatch_country']         = 'Pays d&#8217;exp&eacute;dition';
$_['lang_shipping_national']		= 'Services nationaux';
$_['lang_shipping_international']	= 'Services internationaux';
$_['lang_service']					= '<strong>Service</strong>';
$_['lang_shipping_cost_first']		= '<strong>Co&ucirc;t</strong>';
$_['lang_shipping_cost_add']		= '<strong>chaque article suppl&eacute;mentaire</strong>';
$_['lang_shipping_post_to']			= '<strong>Poster &agrave;</strong>';
$_['lang_shipping_post_ww']			= 'Monde entier';
$_['lang_shipping_post_dm']			= 'Domestique et suivante';
$_['lang_shipping_max_national']	= 'Pour la cat&eacute;gorie choisie, eBay permet &agrave; un responsable de l&#8217;exp&eacute;dition nationale maximale de ';
$_['lang_shipping_getitfast']		= 'Obtention rapide !';

//Returns
$_['lang_return_accepted']			= 'Retour accept&eacute; ?';
$_['lang_return_type']				= 'Type de retour';
$_['lang_return_policy']			= 'Politique de retour';
$_['lang_return_days']				= 'Jours de retour';
$_['lang_return_scosts']			= 'Co&ucirc;ts de livraison';
$_['lang_return_restock']           = 'Frais de restockage';


$_['lang_return_scosts_1']			= 'L&#8217;acheteur paie toute l&#8217;exp&eacute;dition de retour';
$_['lang_return_scosts_2']			= 'Le vendeur paie toute l&#8217;exp&eacute;dition de retour';

//Review page
$_['lang_review_costs']				= 'Co&uacir;ts de l&#8217;annonce';
$_['lang_review_costs_total']		= 'Totaux des frais eBay';
$_['lang_review_edit']				= 'Modifier l&#8217;annonce';
$_['lang_review_preview']			= 'Visualiser l&#8217;annonce';
$_['lang_review_preview_help']		= '(balises eBay non affich&eacute;es)';

//Created
$_['lang_created_title']			= 'Annonce cr&eacute;&eacute;e';
$_['lang_created_msg']				= 'Votre annonce eBay a &eacute;t&eacute; cr&eacute;&eacute;. Le num&eacute;ro d&#8217;article eBay est ';

//Failed page
$_['lang_failed_title']				= 'L&#8217;annonce pour votre article a &eacute;chou&eacute;';
$_['lang_failed_msg1']				= 'Il peut y avoir plusieurs raisons &agrave; cela.';
$_['lang_failed_li1']				= 'Si vous &ecirc;tes un nouveau vendeur sur eBay (ou que vous n&#8217;avez pas vendu beaucoup par le pass&eacute;), vous devrez contacter eBay pour retirer vos restrictions de vendeur';
$_['lang_failed_li2']				= 'Vous n&#8217;&ecirc;tes pas abonn&eacute; au Gestionnaire de ventes Pro sur eBay - c&#8217;est une exigence.';
$_['lang_failed_li3']				= 'Votre compte OpenBay Pro est suspendue, veuillez consulter via votre espace d&#8217;administration du module sous l&#8217;onglet "Mon compte"';
$_['lang_failed_contact']			= 'Si cette erreur persiste, veuillez contacter le support technique apr&egrave;s avoir r&eacute;pondu &agrave; la question de ce qui pr&eacute;c&egrave;de.';
$_['lang_gallery_select_all']		= 'Tout s&eacute;lectionner<span class="help">Cochez la case pour s&eacute;lectionner toutes vos images &agrave; la fois.</span>';
$_['lang_template_images']			= 'Images du th&egrave;me graphique';
$_['lang_ebay_images']				= 'Images eBay';
$_['lang_shipping_in_description']	= 'Information de transport dans la description<span class="help">US, UK, AU & CA seulement</span>';
$_['lang_profile_load']				= 'Chargement du profil';
$_['lang_shipping_first']			= 'Premier article : ';
$_['lang_shipping_add']				= 'Articles additionnels : ';
$_['lang_shipping_service']			= 'Service : ';
$_['lang_btn_remove']				= 'Enlever';
$_['lang_shop_category']			= 'Cat&eacute;gorie de la boutique';
$_['lang_tab_ebay_catalog']			= 'Catalogue eBay';

//Option images
$_['lang_option_images']			= 'Images de variation';
$_['lang_option_images_grp']		= 'Choisir un groupe d&#8217;option';
$_['lang_option_images_choice']		= 'Images';
$_['lang_option_description']		= 'Les images de variation peuvent &ecirc;tre utilis&eacute;es pour afficher une image pr&eacute;cise lorsque l&#8217;utilisateur effectue une s&eacute;lection d&#8217;une option. Vous pouvez utiliser un seul jeu de variation pour les images mais pouvez avoir jusqu&#8217;&agrave; 12 images par variation. Les images par d&eacute;faut sont charg&eacute;s de vos valeurs d&#8217;option (d&eacute;fini dans le catalogue > Options)';

//Product catalog
$_['lang_search_catalog']			= 'Recherche dans le catalogue eBay :';
$_['lang_image_catalog']			= 'Image par d&eacute;faut :<span class="help">Cela va changer votre image principale et sera configur&eacute; pour utiliser l&#8217;image du catalogue eBay</span>';

//Errors
$_['lang_error_choose_category']	= 'Vous devez choisir une cat&eacute;gorie';
$_['lang_error_enter_text']			= 'Entrer le texte de recherche';
$_['lang_error_no_stock']			= 'Vous ne pouvez pas mettre un article ayant z&eacute;ro comme quantit&eacute; en stock';
$_['lang_error_no_catalog_data']	= 'Aucune donn&eacute;e du catalogue eBay n&#8217;a &eacute;t&eacute; trouv&eacute; pour votre produit dans eBay';
$_['lang_error_missing_settings']   = 'Vous ne pouvez pas lister les articles que vous synchronisez avec les param&egrave;tres d&#8217;eBay';
?>