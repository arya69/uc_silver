<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_openbay']              = 'OpenBay Pro';
$_['lang_page_title']           = 'OpenBay Pro pour eBay';
$_['lang_ebay']                 = 'eBay';
$_['lang_heading']              = 'Synchronisation';
$_['lang_btn_return']           = 'Retour';
$_['lang_legend_ebay_sync']     = 'Synchroniser avec eBay';
$_['lang_sync_desc']            = 'Vous devez synchroniser votre magasin avec les derni&egrave;res options et les cat&eacute;gories d&#8217;exp&eacute;dition d&#8217;eBay disponibles, ces donn&eacute;es sont seulement pour les options lors de l&#8217;inscription d&#8217;un point &agrave; eBay - il ne sera pas importer de cat&eacute;gories de votre magasin, etc... <br /> <strong> Vous devriez mettre &agrave; jour fr&eacute;quemment celles-cipour vous assurer d&#8217;avoir toujours les derni&egrave;res donn&eacute;es d&#8217;eBay.</strong>';
$_['lang_sync_cats_lbl']        = 'Obtenir les cat&eacute;gories principales<span class="help">Cela n&#8217;importera pas les cat&eacute;gories de votre magasin !</span>';
$_['lang_sync_shop_lbl']        = 'Obtenir les cat&eacute;gories de la boutique<span class="help">Cela n&#8217;importera pas les cat&eacute;gories de votre magasin !</span>';
$_['lang_sync_setting_lbl']     = 'Obtenir les Param&egrave;tres<span class="help">Importation de types de paiement disponibles, comme l&#8217;exp&eacute;dition, les lieux et plus encore.</span>';
$_['lang_sync_btn']             = 'Mise &agrave; jour';
$_['lang_ajax_ebay_categories'] = 'Cela peut prendre un certain temps, veuillez patienter 5 minutes avant de faire quoi que ce soit d&#8217;autre.';
$_['lang_ajax_cat_import']      = 'Les cat&eacute;gories de votre boutique ont &eacute;t&eacute; import&eacute;s sur eBay.';
$_['lang_ajax_setting_import']  = 'Vos Param&egrave;tres ont bien &eacute;t&eacute; import&eacute;s.';
$_['lang_ajax_setting_import_e']= 'Un erreur s&#8217;est produite lors du chargement des Param&egrave;tres';
$_['lang_ajax_load_error']      = 'Erreur de connexion au serveur';
?>