<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['heading_title']							= 'OpenBay Pro pour Amazon | Annonces en nombre';

$_['text_openbay']							= 'OpenBay Pro';
$_['text_overview']							= 'Amazon vue d&#8217;ensemble';
$_['text_bulk_listing']						= 'Annonces en nombre';
$_['text_searching']						= 'Recherche';
$_['text_finished']							= 'Termin&eacute;';
$_['text_marketplace']						= 'Place de march&eacute';
$_['text_de']								= 'Allemagne';
$_['text_fr']								= 'France';
$_['text_es']								= 'Espagne';
$_['text_it']								= 'Italie';
$_['text_uk']								= 'Angleterre';
$_['text_filter_results']					= 'Filtrer les r&eacute;sultats';
$_['text_dont_list']						= 'Ne pas &eacute;num&eacute;rer';
$_['text_listing_values']					= 'Valeurs des annonces';
$_['text_condition']						= '&Eacute;tat de l&#8217;article';
$_['text_condition_note']					= 'Note sur l&#8217;&eacute;tat';
$_['text_start_selling']					= 'D&eacute;marrer la vente';
$_['text_new']								= 'Neuf';
$_['text_used_like_new']					= 'Occasion - Comme neuf';
$_['text_used_very_good']					= 'Occasion - Tr&egrave;s bon &eacute;tat';
$_['text_used_good']						= 'Occasion - Bon &eacute;tat';
$_['text_used_acceptable']					= 'Occasion - Acceptable';
$_['text_collectible_like_new']				= 'Collection - Comme neuf';
$_['text_collectible_very_good']			= 'Collection - Tr&egrave;s bon &eacute;tat';
$_['text_collectible_good']					= 'Collection - Bon &eacute;tat';
$_['text_collectible_acceptable']			= 'Collection - Acceptable';
$_['text_refurbished']						= 'Restaur&eacute;';

$_['column_name']							= 'Nom';
$_['column_image']							= 'Image';
$_['column_model']							= 'Mod&egrave;le';
$_['column_status']							= '&Eacute;tat';
$_['column_matches']						= 'Nombre de r&eacute;sultats';
$_['column_result']							= 'R&eacute;sultat';

$_['button_return']							= 'Retour';
$_['button_list']							= 'Liste';
$_['button_search']							= 'Recherche';

$_['error_product_sku']						= 'Le produit doit avoir un SKU';
$_['error_product_no_searchable_fields']	= 'Le produit doit avoir un ISBN, EAN, JAN ou un champ UPC de rempli';
$_['error_bulk_listing_not_allowed']		= 'Les annonces en nombre ne sont pas autoris&eacute;es. Votre compte doit &ecirc;tre au moins &agrave; moyen sur le plan';
?>