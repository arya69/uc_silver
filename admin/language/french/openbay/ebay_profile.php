<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Headings
$_['lang_heading']						= 'Profils';
$_['lang_ebay']							= 'eBay';
$_['lang_openbay']						= 'OpenBay Pro';
$_['lang_page_title']					= 'OpenBay Pro pour eBay';

//Buttons
$_['lang_btn_add']						= 'Ajouter un profil';
$_['lang_btn_edit']						= 'Editer';
$_['lang_btn_delete']					= 'Supprimer';
$_['lang_btn_cancel']					= 'Annuler';
$_['lang_btn_return']					= 'Retour';
$_['lang_btn_save']						= 'Sauvegarder';
$_['lang_btn_remove']					= 'Supprimer';

//General profile options
$_['lang_title_list']					= 'Profils';
$_['lang_title_list_add']				= 'Ajouter un profil';
$_['lang_title_list_edit']				= 'Editer le profil';
$_['lang_no_results']					= 'Aucun profil trouv&eacute;';
$_['lang_profile_name']					= 'Nom';
$_['lang_profile_default']				= 'D&eacute;faut';
$_['lang_profile_type']					= 'Type';
$_['lang_profile_desc']					= 'Description';
$_['lang_profile_action']				= 'Action';
$_['lang_yes']							= 'Oui';
$_['lang_no']							= 'Non';

//Tabs
$_['lang_tab_shipping']					= 'Livraison';
$_['lang_tab_general']					= 'G&eacute;n&eacute;ral';
$_['lang_tab_returns']					= 'Retours';
$_['lang_tab_template']					= 'Th&egrave;me graphque';
$_['lang_tab_gallery']					= 'Galerie';
$_['lang_tab_settings']					= 'Param&egrave;tres';

//Shipping Profile
$_['lang_shipping_dispatch_country']    = 'Livraison &agrave; partir du pays';
$_['lang_shipping_postcode']            = 'Localisation - code postal';
$_['lang_shipping_location']            = 'Localisation - ville ou &eacute;tat';
$_['lang_shipping_despatch']            = 'D&eacute;lai d&#8217;exp&eacute;dition<span class="help">(Nombre maximum de jours)</span>';
$_['lang_shipping_nat']                 = 'Services de livraison nationaux';
$_['lang_shipping_intnat']              = 'Services de livraison internationaux';
$_['lang_shipping_first']               = 'Premier article : ';
$_['lang_shipping_add']                 = 'Articles additionnels : ';
$_['lang_shipping_service']             = 'Service : ';
$_['lang_shipping_in_desc']             = 'Infos du fret dans la description<span class="help">US, UK, AU & CA seulement</span>';
$_['lang_shipping_getitfast']           = 'Livraison express!';
$_['lang_shipping_zones']               = 'Exp&eacute;dition par zone';
$_['lang_shipping_worldwide']           = 'Partout dans le monde';

//Returns profile
$_['lang_returns_accept']				= 'Retours accept&eacute;s';
$_['lang_returns_inst']					= 'Politique de retour';
$_['lang_returns_days']					= 'Jours de retour';
$_['lang_returns_days10']				= '10 jours';
$_['lang_returns_days14']				= '14 jours';
$_['lang_returns_days30']				= '30 jours';
$_['lang_returns_days60']				= '60 jours';
$_['lang_returns_type']					= 'Type de retour';
$_['lang_returns_type_money']			= 'Satisfait ou rembours&eacute;';
$_['lang_returns_type_exch']			= 'Rembours&eacute; ou &eacute;chang&eacute;';
$_['lang_returns_costs']				= 'Frais de retour';
$_['lang_returns_costs_b']				= 'Paiement acheteur';
$_['lang_returns_costs_s']				= 'Paiement vendeur';
$_['lang_returns_restock']				= 'Frais de restockage';

//Template profile
$_['lang_template_choose']				= 'Th&eacute;me graphique par d&eacute;faut<span class="help">Un th&eacute;me graphique par d&eacute;faut se charge automatiquement lors de l&#8217;inscription pour gagner du temps</span>';
$_['lang_image_gallery']				= 'Taille de l&#8217;image de la galerie<span class="help">Taille des pixels de la galerie d&#8217;images qui seront ajout&eacute;s &agrave; votre mod&eacute;le graphique.</span>';
$_['lang_image_thumb']					= 'Taille de l&#8217;image de la vignette<span class="help">Taille des pixels des vignettes qui seront ajout&eacute;s &agrave; votre mod&eacute;le graphique.</span>';
$_['lang_image_super']					= 'Taille maximale des images';
$_['lang_image_gallery_plus']			= 'Galerie plus';
$_['lang_image_all_ebay']				= 'Ajouter toutes les images &agrave; eBay';
$_['lang_image_all_template']			= 'Ajouter toutes les images au mod&eacute;le graphique';
$_['lang_image_exclude_default']		= 'Exclure l&#8217;image par d&eacute;faut<span class="help">Seulement pour fonction d&#8217;inscription en nombre ! Cela n&#8217;inclura pas l&#8217;image du produit par d&eacute;faut dans la liste des th&eacute;mes d&#8217;image</span>';
$_['lang_confirm_delete']				= '&Ecirc;tas-vous s&ucirc;r de vouloir supprimer le profil ?';

//General profile
$_['lang_general_private']				= 'Liste des articles des ench&egrave;res priv&eacute;es';
$_['lang_general_price']				= 'Prix % modification <span class="help">0 est par d&eacute;faut, -10 r&eacute;duira de 10 %, 10 augmentera de 10 % (utilis&eacute;e uniquement sur ​​les annonces en nombre)</span>';

//Success messages
$_['lang_added']						= 'Le nouveau profil a bien &eacute;t&eacute; ajout&eacute;';
$_['lang_updated']						= 'Le profil a bien &eacute;t&eacute; mis &agrave; jour';

//Errors
$_['invalid_permission']				= 'Vous n&#8217;avez pas la permission de modifier les profils';
$_['lang_error_name']					= 'Vous devez entrer un nom de profil';
$_['lang_no_template']					= 'ID du th&egrave;me graphique inexistant';
$_['lang_error_missing_settings']		= 'Vous ne pouvez pas ajouter, modifier ou supprimer des profils jusqu&#8217;&agrave; ce que vous syncronisiez vos param&egrave;tres eBay';
?>