<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_heading']              = 'eBay ommaire';
$_['lang_openbay']              = 'OpenBay Pro';
$_['lang_ebay']                 = 'eBay';
$_['lang_btn_return']           = 'Retour';
$_['lang_use_desc']             = 'Ceci est votre page de r&eacute;sum&eacute; de compte eBay. Il s&#8217;agit d&#8217;un aper&ccedil;u rapide de toutes les limites de votre compte ainsi que de vos performances de vente DSR.';
$_['lang_load']                 = 'Rafra&icirc;chir';
$_['lang_limits_heading']       = 'Limite des ventes';
$_['lang_error_validation']     = 'Vous devez vous inscrire &agrave; votre jeton d&#8217;API et activer le module.';
$_['lang_ajax_load_error']      = 'D&eacute;sol&eacute;, la connexion au serveur a &eacute;chou&eacute;';
$_['lang_ebay_limit_head']      = 'Limite des ventes de votre comte eBay';
$_['lang_ebay_limit_t1']        = 'Vous pouvez vendre';
$_['lang_ebay_limit_t2']        = 'plus d&#8217;articles (cela est le montant total des articles, et non pas les dossiers individuels) pour la valeur de';
$_['lang_ebay_limit_t3']        = 'Lorsque vous essayez de cr&eacute;er de nouvelles annonces, ce sera un &eacute;chec si vous d&eacute;passez les montants ci-dessus.';
$_['lang_as_described']         = 'Article comme d&eacute;crit';
$_['lang_communication']        = 'Communication';
$_['lang_shippingtime']         = 'D&eacute;lai de livraison';
$_['lang_shipping_charge']      = 'Frais d&#8217;exp&eacute;dition';
$_['lang_score']                = 'Score';
$_['lang_count']                = 'Compteur';
$_['lang_report_30']            = '30 jours';
$_['lang_report_52']            = '52 semaines';
$_['lang_title_dsr']            = 'Rapports DSR';
?>