<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Text
$_['text_filter']						= 'Filtrer les r&eacute;sultats';
$_['text_title']						= 'Titre';
$_['text_stock_range']					= 'Variation de stock';
$_['text_status']						= '&Eacute;tat';
$_['text_status_marketplace']			= '&Eacute;tat de la place de march&eacute;';
$_['text_price_range']					= 'Prix range';
$_['text_model']						= 'Mod&egrave;le';
$_['text_category']						= 'Cat&eacute;gorie';
$_['text_category_missing']				= 'Cat&eacute;gorie manquante';
$_['text_manufacturer']					= 'Fabricant';
$_['text_populated']					= 'Peupl&eacute;';
$_['text_sku']							= 'R&eacute;f&eacute;rence SKU';
$_['text_description']					= 'Description';
$_['text_variations']					= 'variations';
$_['text_variations_stock']				= 'stock';

// Text status options
$_['text_status_all']					= 'Tout';
$_['text_status_ebay_active']			= 'eBay actif';
$_['text_status_ebay_inactive']			= 'eBay inactif';
$_['text_status_amazoneu_saved']		= 'Amazon EU sauvegard&eacute;';
$_['text_status_amazoneu_processing']	= 'Amazon EU en traitement';
$_['text_status_amazoneu_active']		= 'Amazon EU actif';
$_['text_status_amazoneu_notlisted']	= 'Amazon EU non list&eacute;';
$_['text_status_amazoneu_failed']		= 'Amazon EU &eacute;chou&eacute;';
$_['text_status_amazoneu_linked']		= 'Amazon EU li&eacute;';
$_['text_status_amazoneu_notlinked']	= 'Amazon EU d&eacute;li&eacute;';
$_['text_status_amazonus_saved']		= 'Amazon US sauvegard&eacute;';
$_['text_status_amazonus_processing']	= 'Amazon US en traitement';
$_['text_status_amazonus_active']		= 'Amazon US actif';
$_['text_status_amazonus_notlisted']	= 'Amazon US non list&eacute;';
$_['text_status_amazonus_failed']		= 'Amazon US &eacute;chou&eacute;';
$_['text_status_amazonus_linked']		= 'Amazon US li&eacute;';
$_['text_status_amazonus_notlinked']	= 'Amazon US d&eacute;li&eacute;';