<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_title']                    = 'OpenBay Pro pour Amazon EU | Param&egrave;tres';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon EU vue d&#8217;ensemble';
$_['lang_settings']                 = 'Param&egrave;tres';
$_['lang_token']                    = 'Jeton';
$_['lang_enc_string1']              = 'Cha&icirc;ne de cryptage 1';
$_['lang_enc_string2']              = 'Cha&icirc;ne de cryptage 2';
$_['lang_enabled']                  = 'Autoris&eacute;';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';
$_['lang_btn_save']                 = 'Sauvegarder';
$_['lang_btn_cancel']               = 'Annuler';
$_['lang_api_status']               = '&Eacute;tat de la connexion de l&#8217;API';
$_['lang_api_ok']                   = 'Connexion OK, Authentification OK';
$_['lang_api_auth_error']           = 'Connexion OK, Authentification &eacute;chou&eacute;e';
$_['lang_api_error']                = 'Erreur de connexion';
$_['lang_order_statuses']           = '&Eacute;tats des commandes';
$_['lang_unshipped']                = 'Non livr&eacute;';
$_['lang_partially_shipped']        = 'Livr&eacute; partiellement';
$_['lang_shipped']                  = 'Livr&eacute;';
$_['lang_canceled']                 = 'Annul&eacute;';
$_['lang_import_tax']               = 'Taxe pour les articles import&eacute;s';
$_['lang_import_tax_help']          = 'Utilis&eacute; si Amazon ne taxe pas les informations de retour';
$_['lang_other']                    = 'Autre';
$_['lang_customer_group']           = 'Groupe client';
$_['lang_customer_group_help']      = 'S&eacute;lectionner un groupe de clients &agrave; assigner aux commandes import&eacute;es';
$_['lang_marketplaces']             = 'Places de march&eacute;';
$_['lang_markets']                  = 'Choisissez parmi les places de march&eacute;s auxquelles vous souhaitez importer vos commandes';
$_['lang_de']                       = 'Allemagne';
$_['lang_fr']                       = 'France';
$_['lang_it']                       = 'Italie';
$_['lang_es']                       = 'Espagne';
$_['lang_uk']                       = 'Angleterre';
$_['lang_setttings_updated']        = 'Les param&egrave;tres ont &eacute;t&eacute; correctement mis &agrave; jour.';
$_['lang_error_permission']         = 'Vous n&#8217;avez pas acc&egrave;s &agrave; ce module';
$_['lang_main_settings']            = 'D&eacute;tails de l&#8217;API';
$_['lang_main_settings_heading']    = 'D&eacute;tails de la connexion &agrave; l&#8217;API';
$_['lang_listings_heading']         = 'Annonces';
$_['lang_subscription']             = 'Souscription';
$_['lang_link_items']               = 'Liens des articles';
$_['lang_listing']                  = 'Annonces';
$_['lang_orders']                   = 'Commandes';
$_['lang_tax_percentage']           = 'Pourcentage ajout&eacute; par d&eacute;faut au prix du produit';
$_['lang_default_condition']        = 'Type de l&#8217;&eacute;tat du produit par d&eacute;faut';
$_['lang_default_mp']               = 'Place de march&eacute; par d&eacute;faut';
$_['lang_default_mp_help']          = 'Place de march&eacute; par d&eacute;faut pour les inscriptions et les recherches produits';
$_['lang_admin_notify']             = 'Notifier l&#8217;administrateur de chaque nouvelle commande';
?>