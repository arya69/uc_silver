<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_title']                    = 'OpenBay Pro pour Amazon EU | Annonces sauvegard&eacute;es';
$_['lang_saved_listings']           = 'Annonces sauvegard&eacute;es';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon EU vue d&#8217;ensemble';
$_['lang_btn_return']               = 'Annuler';
$_['lang_description']              = 'Il s&#8217;agit de la liste des annonces de produits enregistr&eacute;es localement et qui sont pr&ecirc;ts &agrave; &ecirc;tre t&eacute;l&eacute;charg&eacute; sur Amazon. Cliquez sur T&eacute;l&eacute;charger pour les poster.';
$_['lang_actions_edit']             = 'Editer';
$_['lang_actions_remove']           = 'Supprimer';
$_['lang_btn_upload']               = 'T&eacute;l&eacute;charger';
$_['lang_name_column']              = 'Nom';
$_['lang_model_column']             = 'Mod&egrave;le';
$_['lang_sku_column']               = 'R&eacute;f&eacute;rence SKU';
$_['lang_amazon_sku_column']        = 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon EU';
$_['lang_actions_column']           = 'Action';
$_['lang_uploaded_alert']           = 'Annonces sauvegard&eacute;es t&eacute;l&eacute;charg&eacute;es !';
$_['lang_delete_confirm']           = '&Eacute;tes-vous s&ucirc;r ?';
?>