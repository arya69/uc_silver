<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

//Headings
$_['lang_page_title']               = 'Annonces en nombre';

//Buttons
$_['lang_cancel']                   = 'Annuler';
$_['lang_none']                     = 'Aucun';
$_['lang_preview']                  = 'Aper&ccedil;u';
$_['lang_add']                      = 'Ajouter';
$_['lang_remove']                   = 'Supprimer';
$_['lang_save']                     = 'Sauvegarder';
$_['lang_preview_all']              = 'Tout v&eacute;rifier';
$_['lang_view']                     = 'Voir les annonces';
$_['lang_edit']                     = 'Editer';
$_['lang_submit']                   = 'Soumettre';
$_['lang_features']                 = 'Editer les fonctionnalit&eacute;s';
$_['lang_catalog']                  = 'S&eacute;lectionner le catalogue';
$_['lang_catalog_search']           = 'Rechercher le catalogue';

//Form options / text
$_['lang_pixels']                   = 'Pixels';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';
$_['lang_other']                    = 'Autres';
$_['lang_select']                   = 'S&eacute;lectionner';

//Profile names
$_['lang_profile_theme']            = 'Profil du th&egrave;me :';
$_['lang_profile_shipping']         = 'Profil de la livraison :';
$_['lang_profile_returns']          = 'Profil des retours :';
$_['lang_profile_generic']          = 'Profil g&&eacute;acute;n&eacute;rique :';

//Text
$_['lang_title']                    = 'Titre';
$_['lang_price']                    = 'Prix';
$_['lang_stock']                    = 'Stock';
$_['lang_search']                   = 'Recherche';
$_['lang_loading']                  = 'Chargement des d&eacute;tails';
$_['lang_preparing0']               = 'Pr&eacute;paration';
$_['lang_preparing1']               = 'de';
$_['lang_preparing2']               = '&eacute;l&eacute;ments';
$_['lang_condition']                = '&Eacute;tat :';
$_['lang_duration']                 = 'D&eacute;lai :';
$_['lang_category']                 = 'Cat&eacute;gorie :';
$_['lang_exists']                   = 'Certains &eacute;l&eacute;ments figurent d&eacute;j&agrave; sur eBay ,ils ont donc &eacute;t&eacute; retir&eacute;s';
$_['lang_error_count']              = 'Vous &avez s&eacute;lectionn&eacute; %s articles, cela peut prendre un certain temps pour traiter vos donn&eacute;es';
$_['lang_verifying']                = 'V&eacute;rification des articles';
$_['lang_processing']               = 'Traitement <span id="activeItems"></span> des articles';
$_['lang_listed']                   = 'ID article list&eacute; : ';
$_['lang_ajax_confirm_listing']     = '&Ecirc;tes-vous sûr de vouloir lister en nombre ces articles ?';
$_['lang_bulk_plan_error']          = 'Votre plan actuel ne permet pas d&#8217;ajout en nombre, mettre &agrave; niveau votre plan <a href="%s">ici</a>';
$_['lang_item_limit']               = 'Vous ne pouvez pas lister ces articles car vous d&eacute;passez la limite de votre plan, am&eacute;liorer votre plan <a href="%s">ici</a>';
$_['lang_search_text']              = 'Entrer le texte de recherche';
$_['lang_catalog_no_products']      = 'Aucun &eacute;l&eacute;ment du catalogue trouv&eacute;';
$_['lang_search_failed']            = 'Recherche &eacute;chou&eacute;e';
$_['lang_esc_key']                  = 'La page d&#8217;accueil a &eacute;t&eacute; cach&eacute;, mais peut ne pas avoir termin&eacute; le chargement';

//Errors
$_['lang_error_ship_profile']       = 'Vous devez avoir un profil de livraison par d&eacute;faut mis en place';
$_['lang_error_generic_profile']    = 'Vous devez avoir un profil g&eacute;n&eacute;rique par d&eacute;faut mis en place';
$_['lang_error_return_profile']     = 'Vous devez avoir un profil des retours par d&eacute;faut mis en place';
$_['lang_error_theme_profile']      = 'Vous devez avoir un profil de th&egrave;me par d&eacute;faut mis en place';
$_['lang_error_variants']           = 'Les articles avec des variations ne peuvent pas &ecirc;tre r&eacute;pertori&eacute;s en nombre et n&#8217ont pas &eacute;t&eacute; s&eacute;lectionn&eacute;';
$_['lang_error_stock']              = 'Certains articles ne sont pas en stock et ont &eacute;t&eacute; enlev&eacute;s';
$_['lang_error_no_product']         = 'Il n&#8217;y a pas de produits s&eacute;lectionn&eacute;s susceptibles d&#8217;utiliser la fonction de transfert group&eacute;';
$_['lang_error_reverify']           = 'Il y a une erreur, vous devez re-v&eacute;rifier les articles et les modifier';
$_['lang_error_missing_settings']   = 'Vous ne pouvez pas lister vos articles en nombre tant que vous n&#8217;aurez pas synchronis&eacute; vos param&egrave;tres eBay';
?>