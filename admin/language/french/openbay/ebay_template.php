<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_heading']					= 'Mod&eacute;le graphique de l&#8217;annonce';
$_['lang_ebay']                     = 'eBay';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_page_title']               = 'OpenBay Pro pour eBay';
$_['lang_btn_add']                  = 'Ajouter un mod&eacute;le graphique';
$_['lang_btn_edit']                 = 'Editer';
$_['lang_btn_delete']               = 'Supprimer';
$_['lang_btn_cancel']               = 'Annuler';
$_['lang_btn_return']               = 'Retourner';
$_['lang_btn_save']                 = 'Sauvegarder';
$_['lang_btn_remove']               = 'D&eacute;placer';
$_['lang_yes']                      = 'Oui';
$_['lang_no']                       = 'Non';

$_['lang_title_list']               = 'Mod&eacute;les graphique';
$_['lang_title_list_add']           = 'Ajouter un mod&eacute;le graphique';
$_['lang_title_list_edit']          = 'Editer le mod&eacute;le graphique';
$_['lang_no_results']               = 'Aucun mod&eacute;le graphique trouv&eacute;';
$_['lang_template_name']            = 'Nom';
$_['lang_template_html']            = 'HTML';
$_['lang_template_action']          = 'Action';
$_['lang_no_template']              = 'ID du mod&eacute;le graphique inexistant';
$_['lang_added']                    = 'Le nouveau mod&eacute;le graphique a bien &eacute;t&eacute; ajout&eacute;';
$_['lang_updated']                  = 'Le mod&eacute;le graphique a bien &eacute;t&eacute; mis &agrave; jour';

$_['lang_error_name']               = 'Vous devez entrer un nom de mod&eacute;le graphique';
$_['lang_confirm_delete']           = '&Eacute;tes-vous s&ucirc;r de vouloir supprimer le mod&eacute;le graphique ?';
$_['invalid_permission']			= 'Attention, vous n&#8217;avez pas les droits n&eacute;cessaires pour modifier le <b>Mod&eacute;le graphique</b> !';
?>