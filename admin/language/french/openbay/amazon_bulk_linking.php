<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['heading_title']						= 'Liens en nombre';

$_['button_return']						= 'Retour';
$_['button_load']						= 'Charger';
$_['button_link']						= 'Lien';

$_['text_openbay']						= 'OpenBay Pro';
$_['text_overview']						= 'Vue d&#8217;ensemble';
$_['text_amazon']						= 'Amazon';
$_['text_local']						= 'Local';
$_['text_bulk_linking']					= 'Liens en nombre';
$_['text_load_listings']				= 'Chargez vos annonces &agrave; partir d&#8217;Amazon';
$_['text_load_listings_help']			= 'Le chargement de toutes vos annonces &agrave; partir d&#8217;Amazon peut prendre un certain temps (jusqu&#8217;&agrave; 2 heures dans certains cas). Si vous liez vos articles, les niveaux de stock sur Amazon seront mis &agrave; jour avec les niveaux des stocks de votre magasin.';
$_['text_loading_listings']				= 'Les annonces sont en cours de chargement sur ​​Amazon';
$_['text_report_requested']				= 'Le rapport sur les annonces demand&eacute; &agrave; Amazon s&#8217; d&eacute;roul&eacute; avec succ&egrave;s';
$_['text_report_request_failed']		= 'Demande de rapport sur les annonces impossible';

$_['text_uk']							= 'Angleterre';
$_['text_de']							= 'Allemagne';
$_['text_fr']							= 'France';
$_['text_it']							= 'Italie';
$_['text_es']							= 'Espagne';

$_['column_asin']						= 'ASIN';
$_['column_price']						= 'Prix';
$_['column_name']						= 'Nom';
$_['column_sku']						= 'R&eacute;f&eacute;rence SKU';
$_['column_quantity']					= 'Quantit&eacute;';
$_['column_combination']				= 'Combinaison';

$_['error_bulk_linking_not_allowed']	= 'Les liens en nombre ne sont pas autoris&eacute;s. Votre compte doit &ecirc;tre au moins &agrave; moyen sur le plan';
?>