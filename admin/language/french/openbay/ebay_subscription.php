<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_heading']				= 'Souscription';
$_['lang_openbay']              = 'OpenBay Pro';
$_['lang_ebay']                 = 'eBay';
$_['lang_page_title']           = 'OpenBay Pro pour eBay';
$_['lang_error_validation']     = 'Vous devez vous inscrire &agrave; votre jeton d&#8217;API et activer le module.';
$_['lang_btn_return']           = 'Retour';
$_['lang_load']                 = 'Rafra&icirc;chir';
$_['lang_usage_title']          = 'Utilisation';
$_['lang_subscription_current'] = 'Plan actuel';
$_['lang_subscription_avail']   = 'Plans disponibles';
$_['lang_subscription_avail1']  = 'La modification des plans seront imm&eacute;diats et les appels non utilis&eacute;s ne seront pas cr&eacute;dit&eacute;s.';
$_['lang_subscription_avail2']  = 'Pour r&eacute;trograder au r&eacute;gime de base veuillez annuler votre abonnement actif &agrave; PayPal.';
$_['lang_ajax_acc_load_plan']   = 'Identifiant de souscription PayPal : ';
$_['lang_ajax_acc_load_plan2']  = ', vous devriez annuler tous les autres abonnements vers nous';
$_['lang_ajax_acc_load_text1']  = 'Nom du plan';
$_['lang_ajax_acc_load_text2']  = 'Limite d&#8217;appels';
$_['lang_ajax_acc_load_text3']  = 'Prix (par mois)';
$_['lang_ajax_acc_load_text4']  = 'Description';
$_['lang_ajax_acc_load_text5']  = 'Plan actuel';
$_['lang_ajax_acc_load_text6']  = 'Changer de plan';
$_['lang_ajax_load_error']      = 'D&eacute;sol&eacute;, pas de r&eacute;ponse. Essayez plus tard.';
?>