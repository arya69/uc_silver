<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_title']                    = 'OpenBay Pro';
$_['lang_page_title']               = 'V&eacute;rification des annonces eBay';
$_['lang_home']                     = 'Accueil';
$_['lang_save']                     = 'Sauvegarder';
$_['lang_ebay']                     = 'eBay';
$_['lang_pageaction']               = 'V&eacute;rification des annonces';
$_['lang_view']                     = 'Voir les annonces';
$_['lang_remove']                   = 'Supprimer le lien';
$_['lang_end']                      = 'Fin des annonces';
$_['lang_cancel']                   = 'Annuler';
$_['lang_loading']                  = 'Obtenir les informations de l&#8217;article depuis eBay';
$_['lang_retry']                    = 'R&eacute;essayer';
$_['lang_error_loading']            = 'Erreur d&#8217;obtention d&#8217;information depuis eBay';
$_['lang_saved']                    = 'Les annonces ont &eacute;t&eacute; sauvegard&eacute;es';
$_['lang_tbl_title']                = 'Titre';
$_['lang_tbl_price']                = 'Prix<span class="help">Taxes incluses</span>';
$_['lang_tbl_qty_instock']          = 'Niveau du stock en local<span class="help">C&#8217;est le niveau de stock sur OpenCart</span>';
$_['lang_tbl_qty_listed']           = 'Quantit&eacute; sur eBay<span class="help">C&#8217;est le niveau de stock courant sur eBay</span>';
$_['lang_tbl_qty_reserve']          = 'Niveau de r&eacute;serve<span class="help">C&#8217;est le niveau maximum de stock sur eBay (0 = aucune limite de r&eacute;serve)</span>';
$_['lang_alert_removed']            = 'Les annonces ont &eacute;t&eacute; d&eacute;li&eacute;es';
$_['lang_alert_ended']              = 'Les annonces sont termin&eacute;es sur eBay';
$_['lang_confirm']                  = '&Ecirc;tes-vous s&ucirc;r ?';

$_['lang_stock_matrix_active']      = 'Matrice de stock (actif)';
$_['lang_stock_matrix_inactive']    = 'Matrice de stock (inactif)';
$_['lang_stock_col_code']           = 'Code Var / SKU';
$_['lang_stock_col_listed']         = 'Mise en vente';
$_['lang_stock_col_limit']          = 'Limite<span class="help">0 = aucune limite</span>';
$_['lang_stock_col_qty_total']      = 'En stock';
$_['lang_stock_col_price']          = 'Prix';
$_['lang_stock_col_active']         = 'Actif';
$_['lang_stock_col_add']            = 'Ajouter <span class="help">Doit &ecirc;tre actif sur OpenCart</span>';
$_['lang_stock_col_comb']           = 'Combinaison';
$_['lang_error_ended']              = 'Les annonces li&eacute;es sont termin&eacute;es, il est impossible de les modifier. Vous devez supprimer le lien.';
$_['lang_error_reserve_size']       = 'Vous ne pouvez pas d&eacute;finir une r&eacute;serve maximum pour le stock local';
?>