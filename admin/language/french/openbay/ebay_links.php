<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
//--------------------------------//

// Headings
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_page_title']               = 'OpenBay Pro pour eBay';
$_['lang_ebay']                     = 'eBay';
$_['lang_heading']                  = 'Liens des articles';
$_['lang_linked_items']             = 'Articles li&eacute;s';
$_['lang_unlinked_items']           = 'Articles d&eacute;li&eacute;s';

// Buttons
$_['lang_btn_resync']               = 'Re-synchroniser';
$_['lang_btn_return']               = 'Retour';
$_['lang_btn_save']                 = 'Sauvegarder';
$_['lang_btn_edit']                 = 'Editer';
$_['lang_btn_check_unlinked']       = 'Cochez les articles non li&eacute;s';
$_['lang_btn_remove_link']          = 'Supprimer le lien';

// Errors & alerts
$_['lang_error_validation']         = 'Vous devez vous inscrire pour votre cl&eacute; API et activer le module.';
$_['lang_alert_stock_local']        = 'Votre annonce eBay sera mis &agrave; jour avec vos niveaux de stocks locaux !';
$_['lang_ajax_error_listings']      = 'Aucun article li&eacute; trouv&eacute;';
$_['lang_ajax_load_error']          = 'D&eacute;sol&eacute;, aucune r&eacute;ponse. Essayez plus tard.';
$_['lang_ajax_error_link']          = 'Le lien de ce produit n&#8217;a pas de valeur';
$_['lang_ajax_error_link_no_sk']    = 'Un lien ne peut &ecirc;tre cr&eacute;&eacute; pour un article en rupture de stock. Mettre fin &agrave; l&#8217;&eacute;l&eacute;ment manuellement sur ​​eBay.';
$_['lang_ajax_loaded_ok']           = 'Les articles ont bien &eacute;t&eacute; charg&eacute;s';

// Text
$_['lang_link_desc1']               = 'Lier vos articles permettra un contr&ocirc;le des stocks sur vos annonces eBay.';
$_['lang_link_desc2']               = 'Chaque &eacute;l&eacute;ment mis &agrave; jour sur le stock local (stock disponible dans votre magasin OpenCart) engendrera la mise &agrave; jour de votre annonce sur eBay';
$_['lang_link_desc3']               = 'Votre stock local est le stock disponible pour la vente. Vos niveaux de stock sur eBay doivent correspondre &agrave; celui-ci.';
$_['lang_link_desc4']               = 'Votre magasin est affect&eacute; d&#8217;articles vendus, mais pas encore &eacute;t&eacute; r&eacute;gl&eacute;s. Ces articles doivent &ecirc;tre mis de c&ocirc;t&eacute; et ne pas &ecirc;tre calcul&eacute;s dans vos niveaux de stocks disponibles.';
$_['lang_text_linked_desc']         = 'Articles li&eacute;s sont des articles OpenCart qui ont un lien vers une annonce eBay.';
$_['lang_text_unlinked_desc']       = 'Les articles d&eacute;li&eacute;s sont les annonces sur eBay qui ne sont pas li&eacute;s pour vos produits OpenCart.';
$_['lang_text_unlinked_info']       = 'Cliquez sur le bouton de contr&ocirc;le des articles non li&eacute;s &agrave; la recherche de vos annonces actives sur eBay o&ugrave; des articles sont non li&eacute;s. Cela peut prendre un certain temps si vous avez beaucoup d&#8217;annonces sur eBay.';
$_['lang_text_loading_items']       = 'Chargement des articles';

// Tables
$_['lang_column_action']            = 'Action';
$_['lang_column_status']            = '&Eacute;tat';
$_['lang_column_variants']          = 'Variantes';
$_['lang_column_itemId']            = 'ID article sur eBay';
$_['lang_column_product']           = 'Produit';
$_['lang_column_product_auto']      = 'Produit<span class="help">(Autocompl&eacute;tion depuis le nom)</span>';
$_['lang_column_stock_available']   = 'Stock local<br /><span class="help">Disponible &agrave; la vente</span>';
$_['lang_column_listing_title']     = 'Titre des annonces<span class="help">(Titre des annonces sur eBay)</span>';
$_['lang_column_allocated']         = 'Stock allou&eacute;<br /><span class="help">Vendu mais non r&eacute;gl&eacute;</span>';
$_['lang_column_ebay_stock']        = 'Stock sur eBay<span class="help">Sur annonce</span>';
?>