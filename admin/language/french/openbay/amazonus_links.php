<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_title']                    = 'OpenBay Pro pour Amazon | Liens des articles';
$_['lang_openbay']                  = 'OpenBay Pro';
$_['lang_overview']                 = 'Amazon vue d&#8217;ensemble';
$_['lang_btn_return']               = 'Retour';
$_['lang_link_items']               = 'Lier les articles';
$_['lang_item_links']               = 'Liens des articles';
$_['lang_desc1']                    = 'Lier vos articles permet un contr&ocirc;le du stock de vos annonces sur Amazon.<br /> Pour chaque article dont le stock est mis &agrave; jour en local (stock disponible dans votre magasin OpenCart) Amazon mettra &agrave; jour votre annonce';
$_['lang_desc2']                    = 'Vous pouvez lier manuellement des articles en entrant sur Amazon La r&eacute;f&eacute;rence SKU et le nom du produit ou charger tous les produits non li&eacute;s sur Amazon puis entrer leur r&eacute;f&eacute;rence. (Le t&eacute;l&eacute;chargement des produits depuis OpenCart vers Amazon va automatiquement ajouter les liens correspondants)';
$_['lang_load_btn']                 = 'Chargement';
$_['lang_new_link']                 = 'Nouveau lien';
$_['lang_autocomplete_product']     = 'Produit<span class="help">(Autocompl&eacute;tion depuis le nom)</span>';
$_['lang_amazonus_sku']             = 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon US';
$_['lang_action']                   = 'Action';
$_['lang_add']                      = 'Ajouter';
$_['lang_add_sku_tooltip']          = 'Ajouter une autre r&eacute;f&eacute;rence SKU';
$_['lang_remove']                   = 'Supprimer';
$_['lang_linked_items']             = 'Articles li&eacute;s';
$_['lang_unlinked_items']           = 'Articles d&eacute;li&eacute;s';
$_['lang_name']                     = 'Nom';
$_['lang_model']                    = 'Mod&egrave;le';
$_['lang_combination']              = 'Combinaison';
$_['lang_sku']                      = 'R&eacute;f&eacute;rence SKU';
$_['lang_amazonus_sku']             = 'R&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon US';
$_['lang_sku_empty_warning']        = 'La r&eacute;f&eacute;rence SKU de l&#8217;article sur Amazon US ne peut pas &ecirc;tre vide !';
$_['lang_name_empty_warning']       = 'Le nom du produit ne peut pas &ecirc;tre vide !';
$_['lang_product_warning']          = 'Produit inexistant. Veuillez utiliser les valeurs de saisie semi-automatique.';
?>