<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['lang_openbay_new']				= 'Cr&eacute;er un nouveau listing';
$_['lang_openbay_edit']             = 'Afficher / &Eacute;diter le listing';
$_['lang_openbay_fix']              = 'Fixer les erreurs';
$_['lang_openbay_processing']       = 'Traitement';

$_['lang_amazonus_saved']           = 'Sauvegard&eacute; (pas t&eacute;l&eacute;charg&eacute;)';
$_['lang_amazon_saved']             = 'Sauvegard&eacute; (pas t&eacute;l&eacute;charg&eacute;)';

$_['lang_markets']                  = 'March&eacute;s';
$_['lang_bulk_btn']                 = 'eBay transfert group&eacute;';
$_['lang_bulk_amazon_btn']          = 'Transfert group&eacute; EU';

$_['lang_marketplace']              = 'Place de march&eacute;';
$_['lang_status']                   = '&Eacute;tat';
$_['lang_option']                   = 'Option';
?>