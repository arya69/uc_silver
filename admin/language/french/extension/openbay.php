<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['lang_heading_title']				= 'OpenBay Pro'; 
$_['lang_text_manager']					= 'Gestion d&#8217;OpenBay Pro'; 

// Text
$_['text_install']						= 'Installer';
$_['text_uninstall']					= 'D&eacute;sinstaller';
$_['lang_text_success']					= 'F&eacute;licitations, les param&egrave;tres ont bien &eacute;t&eacute; sauvegard&eacute;s';
$_['lang_text_no_results']				= 'Aucun r&eacute;sultat';
$_['lang_checking_version']				= 'V&eacute;rification de la version';
$_['lang_btn_manage']					= 'Gestion';
$_['lang_btn_retry']					= 'r&eacute;essayer';
$_['lang_btn_save']						= 'Sauvegarder';
$_['lang_btn_cancel']					= 'Annuler';
$_['lang_btn_update']					= 'Mise &agrave; jour';
$_['lang_btn_settings']					= 'Param&egrave;tres';
$_['lang_btn_patch']					= 'Chemin';
$_['lang_btn_test']						= 'Test de connexion';
$_['lang_latest']						= 'Vous utilisez la derni&egrave;re version';
$_['lang_installed_version']			= 'Version install&eacute;e :';
$_['lang_admin_dir']					= 'R&eacute;pertoire d&#8217;administration :';
$_['lang_admin_dir_desc']				= 'Si vous avez mis &agrave; jour votre r&eacute;pertoire administration pour le nouvel emplacement';
$_['lang_version_old_1']				= 'Une nouvelle version est disponible. Votre version est la ';
$_['lang_version_old_2']				= 'la derni&egrave;re est la ';
$_['lang_use_beta']						= 'Utilisez la version b&ecirc;ta :';
$_['lang_use_beta_2']					= 'Non sugg&eacute;r&eacute; !';
$_['lang_test_conn']					= 'Test de connexion FTP :';
$_['lang_text_run_1']					= 'Ex&eacute;cuter la mise &agrave; jour';
$_['lang_text_run_2']					= 'Ex&eacute;cuter';
$_['lang_no']							= 'Non';
$_['lang_yes']							= 'Oui';
$_['lang_language']						= 'Langue de la r&eacute;ponse pour l&#8217;API :';
$_['lang_getting_messages']				= 'Recevoir les messages d&#8217;OpenBay Pro';

// Column
$_['lang_column_name']					= 'Nom du plugin';
$_['lang_column_status']				= '&Eacute;tat';
$_['lang_column_action']				= 'Action';

// Error
$_['error_permission']					= 'Attention, vous n&#8217;avez pas la permission de modifier eBay extensions!';
$_['lang_error_retry']					= 'Impossible de se connecter au serveur OpenBay.';

// Updates
$_['lang_use_pasv']						= 'Utiliser le FTP passif :';
$_['field_ftp_user']					= 'Nom d&#8217;utilisateur pour le FTP :';
$_['field_ftp_pw']						= 'Mot de passe pour le FTP :';
$_['field_ftp_server_address']			= 'Adresse du serveur pour le FTP :';
$_['field_ftp_root_path']				= 'Chemin sur le serveur pour le FTP :';
$_['field_ftp_root_path_info']			= '(Ex. : httpdocs/www)';
$_['desc_ftp_updates']					= 'Activation des mises &agrave; jour d&#8217;ici signifie que vous n&#8217;avez pas &agrave; mettre &agrave; jour manuellement votre module en utilisant le standard glisser-d&eacute;poser via votre FTP.<br />';

//Updates
$_['lang_run_patch_desc']               = 'Poster la mise &agrave; jour du chemin :<span class="help">Seulement n&eacute;cessaire si vous d&eacute;sirez mettre &agrave; jour manuellement</span>';
$_['lang_run_patch']                    = 'Appliquer le correctif';
$_['update_error_username']             = 'Nom d&#8217;utilisateur attendu';
$_['update_error_password']             = 'Mot de passe attendu';
$_['update_error_server']               = 'Serveur attendu';
$_['update_error_admindir']             = 'R&eacute;pertoire d&#8217;administration attendu';
$_['update_okcon_noadmin']              = 'Connexion OK mais votre r&eacute;pertoire d&#8217;administration OpenCart n&#8217;a pas &eacute;t&eacute; trouv&eacute;';
$_['update_okcon_nofiles']              = 'Connexion OK mais vos dossiers OpenCart n&#8217;ont pas &eacute;t&eacute; trouv&eacute; ! Le chemin de la racine est-il correct ?';
$_['update_okcon']                      = 'Connexion au serveur OK. Les dossiers OpenCart ont &eacute;t&eacute; trouv&eacute;';
$_['update_failed_user']                = 'Impossible de se connecter avec ce nom d&#8217;utilisateur';
$_['update_failed_connect']             = 'Impossible de se connecter au serveur';
$_['update_success']                    = 'Le module a &eacute;t&eacute; mis &agrave; jour (v.%s)';
$_['lang_patch_notes1']                 = 'Pour en savoir plus sur les mises &agrave; jour r&eacute;centes et pass&eacute;es,';
$_['lang_patch_notes2']                 = 'cliquer ici';
$_['lang_patch_notes3']                 = 'L&#8217;outil de mise &agrave; jour apportera des modifications aux fichiers syst&egrave;me de votre boutique. Assurez-vous que vous avez une sauvegarde avant d&#8217;utiliser cet outil.';

//Help tab
$_['lang_help_title']                   = 'Information sur l&#8217;aide et le support';
$_['lang_help_support_title']           = 'Support :';
$_['lang_help_support_description']     = 'Vous devriez consulter notre <a href="http://shop.openbaypro.com/index.php?route=information/faq" title="FAQ OpenBay Pro pour support opencart ">FAQ</a> afin de voir si votre question y figure. <br />Si vous ne trouvez pas de r&eacute;ponse, vous pouvez cr&eacute;er un ticket de support en cliquant <a href="http://support.welfordmedia.co.uk" title="Site support OpenBay Pro pour OpenCart">ici</a>';
$_['lang_help_template_title']          = 'Cr&eacute;ation de mod&egrave;les graphique pour eBay :';
$_['lang_help_template_description']    = 'Informations pour les d&eacute;veloppeurs et les concepteurs sur la cr&eacute;ation des mod&egrave;les personnalis&eacute;s pour leurs annonces eBay, <a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=30" title="OpenBay Pro HTML templates for eBay">cliquer ici</a>';

$_['lang_tab_help']                     = 'Aide';
$_['lang_help_guide']                   = 'Guides de l&#8217;utilisateur :';
$_['lang_help_guide_description']       = 'Pour t&eacute;l&eacute;charger et consulter les guides de l&#8217;utilisateur d&#8217;eBay et d&#8217;Amazon <a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=37" title="Guides de l&#8217;utilisateur d&#8217;OpenBay Pro">cliquer ici</a>';

$_['lang_mcrypt_text_false']            = 'La fonction PHP "mcrypt_encrypt" n&#8217:est pas activ&eacute;e. Contacter votre fournisseur d&#8217:h&eacute;bergement.';
$_['lang_mb_text_false']                = 'La librairie PHP "mb strings" n&#8217;est pas activ&eacute;. Contacter votre fournisseur d&#8217;h&eacute;bergement.';
$_['lang_ftp_text_false']               = 'Les fonctions PHP FTP ne sont pas activ&eacute;s. Contacter votre fournisseur d&#8217;h&eacute;bergement.';
$_['lang_error_oc_version']             = 'Votre version d&#8217;OpenCart n&#8217;a pas &eacute;t&eacute; test&eacute; pour fonctionner avec ce module. Vous pourriez rencontrer des probl&egrave;mes.';
$_['lang_patch_applied']                = 'Patch appliqu&eacute;';
$_['faqbtn']                            = 'Voir la FAQ';
$_['lang_clearfaq']                     = 'Effacer les fen&ecirc;tres contextuelles cach&eacute;es de la FAQ :';
$_['lang_clearfaqbtn']                  = 'Nettoyer';

// Ajax elements
$_['lang_ajax_amazoneu_shipped']        = 'La commande sera marqu&eacute; comme exp&eacute;di&eacute;e sur Amazon EU automatiquement';
$_['lang_ajax_amazonus_shipped']        = 'La commande sera marqu&eacute; comme exp&eacute;di&eacute;e sur Amazon US automatiquement';
$_['lang_ajax_refund_reason']           = 'Raison du remboursement';
$_['lang_ajax_refund_message']          = 'Message du remboursement';
$_['lang_ajax_refund_entermsg']         = 'Vous devez saisir un message de remboursement';
$_['lang_ajax_refund_charmsg']          = 'Votre message de remboursement doit &ecirc;tre inf&eacute;rieur &agrave; 1000 caract&egrave;res';
$_['lang_ajax_refund_charmsg2']         = 'Votre message ne ​​peut pas contenir les caract&egrave;res > ou <';
$_['lang_ajax_courier']                 = 'Courrier';
$_['lang_ajax_courier_other']           = 'Autre courrier';
$_['lang_ajax_tracking']                = 'Suivi #';
$_['lang_ajax_tracking_msg']            = 'Vous devez entrer un ID de suivi, utiliser "none" si vous n&#8217;en n&#8217;avez pas.';
$_['lang_ajax_tracking_msg2']           = 'Votre ID de suivi ne ​​peut pas contenir les caract&egrave;res > ou <';
$_['lang_ajax_tracking_msg3']           = 'Vous devez s&eacute;lectionner courrier si vous souhaitez t&eacute;l&eacute;charger un num&eacute;ro de suivi.';
$_['lang_ajax_tracking_msg4']           = 'Veuillez laissez le champ de messagerie vide si vous souhaitez utiliser un courrier personnalis&eacute;.';

$_['lang_title_help']                   = 'Besoin d&#8217;aide avec OpenBay Pro ?';
$_['lang_pod_help']                     = 'Aide';
$_['lang_title_manage']                 = 'Gestion OpenBay Pro; mises &agrave; jour, param&egrave;tres et plus';
$_['lang_pod_manage']                   = 'Gestion';
$_['lang_title_shop']                   = 'OpenBay Pro boutique; addons, th&egrave;mes graphiques et plus';
$_['lang_pod_shop']                     = 'Boutique';

$_['lang_checking_messages']            = 'V&eacute;rification des messages';
$_['lang_title_messages']               = 'Messages &amp; notifications';
?>