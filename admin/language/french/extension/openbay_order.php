<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//----------------------------------

$_['lang_btn_status']           = 'Changement d&#8217&eacute;tat';
$_['lang_order_channel']        = 'Canal de commande';
$_['lang_confirmed']            = 'commandes ont &eacute;t&eacute; marqu&eacute;es';
$_['lang_no_orders']            = 'Pas de commande s&eacute;lectionn&eacute;s &agrave; mettre &agrave; jour';
$_['lang_confirm_title']        = 'Mise &agrave; jour de l&#8217&eacute;tat des avis en nombre';
$_['lang_confirm_change_text']  = 'Changer l&#8217;&eacute;tat de la commande';
$_['lang_column_addtional']     = 'Informations compl&eacute;mentaires';
$_['lang_column_comments']      = 'Commentaires';
$_['lang_column_notify']        = 'Notifier';
$_['lang_carrier']              = 'Transporteur';
$_['lang_tracking']             = 'Suivi';
$_['lang_other']                = 'Autre';
$_['lang_refund_reason']        = 'Raison du remboursement';
$_['lang_refund_message']       = 'Message du remboursement';
$_['lang_update']               = 'Mise &agrave; jour';
$_['lang_cancel']               = 'Annulation';
$_['lang_e_ajax_1']             = 'Un ordre de lecture est manquant au message de remboursement !';
$_['lang_e_ajax_2']             = 'Un ordre de lecture est absent aux informations de suivi !';
$_['lang_e_ajax_3']             = 'Il manque l&#8217;entr&eacute;e d&#8217; "Autre transporteur" dans la commande Amazon!';
$_['lang_title_order_update']   = 'Mise &agrave; jour en nombre des commandes';
?>