<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//----------------------------------

// Heading
$_['heading_title']							= 'Profils de paiement';

// Text
$_['text_success']							= 'F&eacute;licitation, vous avez modifi&eacute; les <b>Profils de paiement</b> avec succ&egrave;s !';
$_['text_payment_profiles']					= 'Profils de paiement';
$_['text_status_active']					= 'Actif';
$_['text_status_inactive']					= 'Inactif';
$_['text_status_cancelled']					= 'Annul&eacute;';
$_['text_status_suspended']					= 'Suspendu';
$_['text_status_expired']					= 'Expir&eacute;';
$_['text_status_pending']					= 'En attente';
$_['text_transactions']						= 'Transactions';
$_['text_return']							= 'Retour';
$_['text_cancel']							= 'Annuler';
$_['text_filter']							= 'Filtrer';
$_['text_cancel_confirm']					= 'L&#8217;annulation du profil ne peut pas &ecirc;tre annul&eacute; ! &Ecirc;tes-vous s�r de vouloir le faire ?';
$_['text_transaction_created']				= 'Cr&eacute;&eacute;';
$_['text_transaction_payment']				= 'Paiement';
$_['text_transaction_outstanding_payment']	= 'Paiement impay&eacute;';
$_['text_transaction_skipped']				= 'Paiement ignor&eacute;';
$_['text_transaction_failed']				= 'Paiement &eacute;chou&eacute;';
$_['text_transaction_cancelled']			= 'Annul&eacute;';
$_['text_transaction_suspended']			= 'Suspendu';
$_['text_transaction_suspended_failed']		= 'Suspendu pour paiement &eacute;chou&eacute;';
$_['text_transaction_outstanding_failed']	= 'Impay&eacute; pour paiement &eacute;chou&eacute;';
$_['text_transaction_expired']				= 'Expir&eacute;';

// Entry
$_['entry_cancel_payment']					= 'Paiement annul&eacute; :';
$_['entry_order_recurring']					= 'ID';
$_['entry_order_id']						= 'N&deg; de commande';
$_['entry_payment_reference']				= 'R&eacute;f&eacute;rence paiement';
$_['entry_customer']						= 'Client';
$_['entry_date_created']					= 'Date de cr&eacute;ation';
$_['entry_status']							= '&Eacute;tat';
$_['entry_type']							= 'Type';
$_['entry_action']							= 'Action';
$_['entry_email']							= 'Courriel';
$_['entry_profile_description']				= 'Description du profil';
$_['entry_product']							= 'Produit :';
$_['entry_quantity']						= 'Quantit&eacute; :';
$_['entry_amount']							= 'Montant :';
$_['entry_profile']							= 'Profil :';
$_['entry_payment_type']					= 'Mode de paiement :';

// Error
$_['error_not_cancelled']					= 'Erreur : %s';
$_['error_not_found']						= 'Impossible d&#8217;annuler le profil';
$_['success_cancelled']						= 'Le paiement r&eacute;current a &eacute;t&eacute; annul&eacute;';
?>