<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Locale
$_['code']							= 'fr';
$_['direction']						= 'ltr';
$_['date_format_short']				= 'd/m/Y';
$_['date_format_long']				= 'l j F Y';
$_['time_format']					= 'H:i:s';
$_['decimal_point']					= ',';
$_['thousand_point']				= '.';

// Text
$_['text_yes']						= 'Oui';
$_['text_no']						= 'Non';
$_['text_enabled']					= 'Activ&eacute;';
$_['text_disabled']					= 'D&eacute;sactiv&eacute;';
$_['text_none']						= ' --- Aucun --- ';
$_['text_select']					= ' -- S&eacute;lection -- ';
$_['text_select_all']				= 'Tout s&eacute;lectionner';
$_['text_unselect_all']				= 'Tout d&eacute;s&eacute;lectionner';
$_['text_all_zones']				= 'Toutes zones';
$_['text_default']					= ' <b>(par d&eacute;faut)</b>';
$_['text_close']					= 'Fermer';
$_['text_pagination']				= 'Voir {start} &agrave; {end} de {total} ({pages} Pages)';
$_['text_no_results']				= 'Aucun r&eacute;sultat !';
$_['text_separator']				= ' &gt; ';
$_['text_edit']						= 'Modifier';
$_['text_view']						= 'Visualiser';
$_['text_home']						= 'Accueil';

// Button
$_['button_insert']					= 'Ins&eacute;rer';
$_['button_delete']					= 'Supprimer';
$_['button_save']					= 'Sauvegarder';
$_['button_cancel']					= 'Annuler';
$_['button_clear']					= 'Effacer les entr&eacute;es du journal';
$_['button_close']					= 'Fermer';
$_['button_filter']					= 'Filtrer';
$_['button_send']					= 'Envoyer';
$_['button_edit']					= '&Eacute;diter';
$_['button_copy']					= 'Copier';
$_['button_back']					= 'Retour';
$_['button_remove']					= 'Retirer';
$_['button_backup']					= 'Sauvegarde';
$_['button_restore']				= 'Restauration';
$_['button_repair']					= 'R&eacute;parer';
$_['button_upload']					= 'Chargement';
$_['button_submit']					= 'Soumettre';
$_['button_invoice']				= 'Facture';
$_['button_add_address']			= 'Ajouter une adresse';
$_['button_add_attribute']			= 'Ajouter un attribut';
$_['button_add_banner']				= 'Ajouter une banni&egrave;re';
$_['button_add_custom_field_value'] = 'Ajouter un champ sp&eacute;cial';
$_['button_add_product']			= 'Ajouter un produit';
$_['button_add_voucher']			= 'Ajouter un bon de r&eacute;duction';
$_['button_add_filter']             = 'Ajouter un Filtre';
$_['button_add_option']				= 'Ajouter une option';
$_['button_add_option_value']		= 'Ajouter une valeur';
$_['button_add_discount']			= 'Ajouter une r&eacute;duction';
$_['button_add_special']			= 'Ajouter une promotion';
$_['button_add_image']				= 'Ajouter une image';
$_['button_add_geo_zone']			= 'Ajouter une zone g&eacute;ographique';
$_['button_add_history']			= 'Ajouter un historique';
$_['button_add_transaction']		= 'Ajouter une transaction';
$_['button_add_total']				= 'Ajouter un total';
$_['button_add_reward']				= 'Ajouter des points de fidelit&eacute;';
$_['button_add_route']				= 'Ajouter un chemin';
$_['button_add_rule' ]				= 'Ajouter une r&egrave;gle';
$_['button_add_module']				= 'Ajouter un module';
$_['button_add_link']				= 'Ajoutet un lien';
$_['button_update_total']			= 'Mise &agrave; jour des totaux';
$_['button_approve']				= 'Approuver';
$_['button_reset']					= 'R&eacute;initialiser';
$_['button_add_profile']			= 'Ajouter un profil';

// Tab
$_['tab_address']					= 'Adresse';
$_['tab_admin']						= 'Admin';
$_['tab_attribute']					= 'Attributs';
$_['tab_customer']					= 'D&eacute;tails client';
$_['tab_data']						= 'Donn&eacute;es';
$_['tab_design']					= 'Design';
$_['tab_discount']					= 'R&eacute;ductions';
$_['tab_general']					= 'G&eacute;n&eacute;ral';
$_['tab_history']					= 'Historique';
$_['tab_fraud']						= 'Fraude';
$_['tab_ftp']						= 'FTP';
$_['tab_ip']						= 'Adresses IP';
$_['tab_links']						= 'Liens';
$_['tab_log']						= 'Journal';
$_['tab_image']						= 'Images';
$_['tab_option']					= 'Options';
$_['tab_server']					= 'Serveur';
$_['tab_store']						= 'Boutique';
$_['tab_special']					= 'Promotions';
$_['tab_local']						= 'Localisation';
$_['tab_mail']						= 'Courriel';
$_['tab_marketplace_links']         = '<abbr title="Liens des places de march&eacute;">Liens PM</abbr>';
$_['tab_module']					= 'Modules';
$_['tab_order']						= 'D&eacute;tails des commandes';
$_['tab_payment']					= 'D&eacute;tails des paiements';
$_['tab_product']					= 'Produits';
$_['tab_return']					= 'D&eacute;tails des retours';
$_['tab_reward']					= 'Points de fidelit&eacute;';
$_['tab_profile']					= 'Profils';
$_['tab_shipping']					= 'Adresse de livraison';
$_['tab_total']						= 'Totaux';
$_['tab_transaction']				= 'Transactions';
$_['tab_voucher']					= 'Bon de r&eacute;duction';
$_['tab_voucher_history']			= 'Historique des r&eacute;c&eacute;piss&eacute;s';
$_['tab_price']						= 'Prix';

// Error
$_['error_upload_1']				= 'Attention, le fichier envoy&eacute; d&eacute;passe la taille maiximale de fichier d&eacute;finie dans php.ini (upload_max_filesize) !';
$_['error_upload_2']				= 'Attention, le fichier envoy&eacute; d&eacute;passe la taille maiximale de fichier d&eacute;finie dans dans le formulaire HTML (MAX_FILE_SIZE) !';
$_['error_upload_3']				= 'Attention, le fichier envoy&eacute; a seulement &eacute;t&eacute; partiellement envoy&eacute; !';
$_['error_upload_4']				= 'Attention, aucun fichier n&#8217;a &eacute;t&eacute; envoy&eacute; !';
$_['error_upload_6']				= 'Attention, il manque un dossier temporaire !';
$_['error_upload_7']				= 'Attention, &eacute;chec d&#8217;&eacute;criture sur le disque !';
$_['error_upload_8']				= 'Attention, envoi du fichier arr&ecirc;t&eacute; par extension !';
$_['error_upload_999']				= 'Attention, aucun code d&#8217;erreur disponible !';
?>