<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

$_['heading_title']		= 'OpenBay Pro';

$_['text_module']		= 'Modules';
$_['text_installed']	= 'Le module OpenBay Pro est d&eacute;sormais install&eacute;. Il est accessible sous Extensions -> OpenBay Pro';
?>