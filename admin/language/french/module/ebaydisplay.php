<?php
//--------------------------------//
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//--------------------------------//

// Heading
$_['heading_title']			= 'eBay display';

// Text
$_['text_module']			= 'Modules';
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; le module <b>eBay display</b> avec succ&egrave;s !';
$_['text_content_top']		= 'En-t&ecirc;te';
$_['text_content_bottom']	= 'Pied de page';
$_['text_column_left']		= 'Colonne de gauche';
$_['text_column_right']		= 'Colonne de droite';

$_['text_introduction']		= 'Le module eBay display vous permet de pr&eacute;senter les produits de votre compte eBay directement dans votre boutique Opencart';
$_['text_settings']			= 'Param&egrave;tres';
$_['text_username']			= 'Nom d&#8217;utilisateur';
$_['text_keysearch']   		= 'Recherche par mots clefs';
$_['text_include']			= 'Inclure la description dans la recherche';
$_['text_limit']			= 'Limite';
$_['text_sort']				= 'Classement';
$_['text_start_time']		= 'Commencer au plus r&eacute;cent';
$_['text_random']			= 'Al&eacute;atoire';
$_['text_display']			= 'Pr&eacute;sentation';

// Entry
$_['entry_product']			= 'Produits :<br /><span class="help">(Autocompl&eacute;tion)</span>';
$_['entry_limit']			= 'Limite';
$_['entry_image']			= 'Image (L x H) et type de redimensionnement';
$_['entry_layout']			= 'Disposition';
$_['entry_position']		= 'Emplacement';
$_['entry_status']			= '&Eacute;tat';
$_['entry_sort_order']		= 'Classement';

// Error 
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le module <b>eBay display</b> !';
$_['error_image']			= 'Attention, les dimensions de l&#8217;image <b>(hauteur et largeur)</b> sont requises ';
?>