<?php
//----------------------------------
// Opencart France				  //
// http://www.opencart-france.fr  //
// Traduit par LeorLindel		  //
// Possession opencart-france.fr  //
//----------------------------------

// Heading
$_['heading_title']         = 'Champs personnalis&eacute;s';

// Text
$_['text_success']			= 'F&eacute;licitations, vous avez modifi&eacute; les <b>Champs personnalis&eacute;s</b> avec succ&egrave;s !';
$_['text_choose']			= 'Choix';
$_['text_select']			= 'S&eacute;lecteur';
$_['text_radio']			= 'Bouton radio';
$_['text_checkbox']			= 'Case &agrave; cocher';
$_['text_input']			= 'Entr&eacute;e de donn&eacute;es';
$_['text_text']				= 'Texte';
$_['text_textarea']			= 'Zone de texte';
$_['text_file']				= 'Fichier';
$_['text_date']				= 'Date';
$_['text_datetime']			= 'Date &amp; Heure';
$_['text_time']				= 'Heure';
$_['text_customer']			= 'Client';
$_['text_address']			= 'Adresse';
$_['text_payment_address']	= 'Adresse de paiement';
$_['text_shipping_address']	= 'Adresse de livraison';

// Column
$_['column_name']           = 'Nom du champ sp&eacute;cial';
$_['column_type']           = 'Type';
$_['column_location']       = 'Emplacement';
$_['column_sort_order']     = 'Classement';
$_['column_action']         = 'Action';

// Entry
$_['entry_name']			= 'Nom du champ sp&eacute;cial :';
$_['entry_type']			= 'Type :';
$_['entry_value']			= 'Valeur :';
$_['entry_custom_value']	= 'Nom de la valeur du champ sp&eacute;cial :';
$_['entry_required']		= 'Requis :';
$_['entry_location']		= 'Localisation :';
$_['entry_position']		= 'Emplacement :';
$_['entry_sort_order']		= 'Classement :';

// Error
$_['error_permission']     = 'Attention, vous n&#8217;avez pas la permission de modifier les champs sp&eacute;ciaux !';
$_['error_name']           = 'Le nom de l&#8217;option doit &ecirc;tre compos&eacute; de 1 &agrave; 128 caract&egrave;res !';
$_['error_type']           = 'Attention, la valeur de l&#8217;option est requise !';
$_['error_custom_value']   = 'Le nom de la valeur du champ sp&eacute;cial doit &ecirc;tre compos&eacute; de 1 &agrave; 128 caract&egrave;res !';
$_['error_product']        = 'Attention, ce champ sp&eacute;cial ne peut pas &ecirc;tre supprim&eacute; car %s produits lui sont encore associ&eacute;s !';
?>