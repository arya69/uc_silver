<!doctype html> 
<!--
      //===========================================//
     // Cart Checkout Requirements                //
    // Author: Joel Reeds                        //
   // Company: OpenCart Addons                  //
  // Website: http://opencartaddons.com        //
 // Contact: webmaster@opencartaddons.com     //
//===========================================//
-->
<?php echo $header; ?>	
<?php if ($ocversion >= 150) { ?>			
	<div id="content">
		<div class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
			<?php } ?>
		</div>
<?php } ?>	
	<?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
		<div class="box">
			<?php if ($ocversion < 150) { ?>
				<div class="left"></div>
				<div class="right"></div>
			<?php } ?>
			<div class="heading">
				<?php if ($ocversion < 150) { ?>
					<h1 style="background-image: url('view/image/<?php echo $extensiontype; ?>.png');"><?php echo $heading_title; ?></h1>
				<?php } else { ?>
					<h1><img src="view/image/<?php echo $extensiontype; ?>.png" alt="" /> <?php echo $heading_title; ?></h1>
				<?php } ?>
				<div class="buttons"><a onclick="$('#form').attr('action', '<?php echo $action; ?>&apply=true'); $('#form').submit();" class="button"><span><?php echo $button_apply; ?></span></a><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
			</div>
			<div class="content">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
					<div class="oca-rate">
						<div class="oca-head"><?php echo $text_settings; ?> <div class="oca-remove"><a href="http://opencartaddons.com" target="_blank"><img src="view/image/oca_logo.png" alt="OpenCart Addons" height="17px"/></a></div></div>
						<div class="oca-content">
							<table class="oca-table">
								<thead>
									<tr style="border-bottom: 1px solid #DDD;">
										<td class="center"><?php echo $entry_system_status; ?></td>
									</tr>
								</thead>
								<tbody>
									<tr style="border-bottom: 1px solid #DDD;">
										<td class="center">
											<select name="<?php echo $extension; ?>_status">
												<?php if (${$extension . '_status'}) { ?>
													<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
													<option value="0"><?php echo $text_disabled; ?></option>
												<?php } else { ?>
													<option value="1"><?php echo $text_enabled; ?></option>
													<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
												<?php } ?>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							<table class="oca-table">
								<thead>
									<tr style="border-bottom: 1px solid #DDD;">
										<td class="center"><?php echo $entry_customer_group; ?></td>
										<td class="left" style="width: 15%;"><?php echo $entry_total; ?></td>
										<td class="left" style="width: 15%;"><?php echo $entry_quantity; ?></td>
										<td class="left" style="width: 15%;"><?php echo $entry_weight; ?></td>
										<td class="left" style="width: 15%;"><?php echo $entry_product; ?></td>
										<td class="center" style="width: 15%;"><?php echo $entry_status; ?></td>
									</tr>
								</thead>
								<tbody>			
									<?php foreach ($customer_groups as $customer_group) { ?>
										<tr style="border-bottom: 1px solid #DDD;">
											<td class="center">
												<?php echo $customer_group['name']; ?>
											</td>
											<td class="left">
												<?php echo $text_min; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][total_min]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['total_min'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['total_min']; } ?>" size="5" /><br/>
												<?php echo $text_max; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][total_max]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['total_max'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['total_max']; } ?>" size="5" />
											</td>
											<td class="left">
												<?php echo $text_min; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][quantity_min]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['quantity_min'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['quantity_min']; } ?>" size="5" /><br/>
												<?php echo $text_max; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][quantity_max]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['quantity_max'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['quantity_max']; } ?>" size="5" />
											</td>
											<td class="left">
												<?php echo $text_min; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][weight_min]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['weight_min'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['weight_min']; } ?>" size="5" /><br/>
												<?php echo $text_max; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][weight_max]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['weight_max'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['weight_max']; } ?>" size="5" />
											</td>
											<td class="left">
												<?php echo $text_min; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][product_min]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['product_min'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['product_min']; } ?>" size="5" /><br/>
												<?php echo $text_max; ?> <input type="text" name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][product_max]" value="<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['product_max'])) { echo ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['product_max']; } ?>" size="5" />
											</td>
											<td class="center">
												<select name="<?php echo $extension; ?>_customer_group[<?php echo $customer_group['customer_group_id']; ?>][status]">
													<?php if (isset(${$extension . '_customer_group'}[$customer_group['customer_group_id']]['status']) && ${$extension . '_customer_group'}[$customer_group['customer_group_id']]['status']) { ?>
														<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
														<option value="0"><?php echo $text_disabled; ?></option>
													<?php } else { ?>
														<option value="1"><?php echo $text_enabled; ?></option>
														<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</form>
				<center><?php echo $text_contact; ?></center>
			</div>
		</div>
	</div>
<?php if ($ocversion >= 150) { ?>
	</div>
<?php } ?>

<?php echo $footer; ?> 