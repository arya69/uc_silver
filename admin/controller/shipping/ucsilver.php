<?php
class ControllerShippingUcsilver extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('shipping/ucsilver');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('ucsilver', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_none'] = $this->language->get('text_none');
		
		$this->data['entry_shipping_courier'] = $this->language->get('entry_shipping_courier');
		$this->data['entry_ucsilver_method'] = $this->language->get('entry_ucsilver_method');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/ucsilver', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/ucsilver', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['shipping_courier'])) {
			$this->data['shipping_courier'] = $this->request->post['shipping_courier'];
		} else {
			$this->data['shipping_courier'] = $this->config->get('shipping_courier');
		}
		
		if (isset($this->request->post['ucsilver_method'])) {
			$this->data['ucsilver_method'] = $this->request->post['ucsilver_method'];
		} else {
			$this->data['ucsilver_method'] = $this->config->get('ucsilver_method');
		}
		
		if (isset($this->request->post['ucsilver_status'])) {
			$this->data['ucsilver_status'] = $this->request->post['ucsilver_status'];
		} else {
			$this->data['ucsilver_status'] = $this->config->get('ucsilver_status');
		}
		
		if (isset($this->request->post['ucsilver_sort_order'])) {
			$this->data['ucsilver_sort_order'] = $this->request->post['ucsilver_sort_order'];
		} else {
			$this->data['ucsilver_sort_order'] = $this->config->get('ucsilver_sort_order');
		}				

		$this->template = 'shipping/ucsilver.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/ucsilver')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>