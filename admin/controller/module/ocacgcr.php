<?php
      //===========================================//
     // Customer Group Checkout Requirements      //
    // Author: Joel Reeds                        //
   // Company: OpenCart Addons                  //
  // Website: http://opencartaddons.com        //
 // Contact: webmaster@opencartaddons.com     //
//===========================================//

class ControllerModuleOCACGCR extends Controller {
	private $error = array();
	private $version = '2.1';
	private $extension = 'ocacgcr';
	private $extensionType = 'module';
	
	private function getVersion() {
		if (defined('VERSION') && VERSION < 1.5) {
			$oc = 140;
		} elseif (defined('VERSION') && strpos(VERSION, '1.5.0') === 0) {
			$oc = 150;
		} elseif (defined('VERSION') && strpos(VERSION, '1.5.1') === 0) {
			$oc = 151;
		} else {
			$oc = '';
		}
		if (defined('VERSION') && VERSION >= 1.5 && !$oc) {
			$oc = 152;
		}
		return $oc;
	}
	
	public function index() {  
		
		$this->data['extension'] = $this->extension;
		$this->data['extensiontype'] = $this->extensionType;
		$this->data['ocversion'] = $this->getVersion();
		
		$this->data = array_merge($this->data, $this->load->language($this->extensionType . '/' . $this->extension));
		$this->document->addStyle('view/stylesheet/oca_stylesheet.css');
		
		$this->load->model('localisation/weight_class');
		if ($this->config->get('config_weight_class_id')) {
			$weight_class = $this->model_localisation_weight_class->getWeightClass($this->config->get('config_weight_class_id'));
			$weight_units = isset($weight_class['unit']) ? $weight_class['unit'] : $this->config->get('config_weight_class_id');
		} else {
			$weight_class = $this->model_localisation_weight_class->getWeightClass($this->config->get('config_weight_class'));
			$weight_units = isset($weight_class['unit']) ? $weight_class['unit'] : $this->config->get('config_weight_class');
		}
		
		$this->data['entry_weight'] = sprintf($this->data['entry_weight'], $weight_units);
		
		$this->data['text_contact'] = sprintf($this->data['text_contact'], $this->version);
		
		$this->document->setTitle($this->language->get('text_name'));
		
		$this->load->model('setting/setting');
				
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$post_data = $this->request->post;
			if ($this->getVersion() <= 150) {
				foreach ($post_data as $key => $value) {
					if (is_array($value)) $post_data[$key] = serialize($value);
				}
			}
			$this->model_setting_setting->editSetting($this->extension, $post_data);	

			if (isset($this->request->get['apply'])) {
				$this->session->data['success'] = $this->language->get('text_success');
				$this->redirect($this->getLink($this->extensionType, $this->extension)); 
			} else {
				$this->session->data['success'] = $this->language->get('text_success');
				$this->redirect($this->getLink('extension', $this->extensionType));
			}
		}
		
 		$this->data['success'] = isset($this->session->data['success']) ? $this->session->data['success'] : '';
		unset($this->session->data['success']);
		$this->data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';
		
  		$breadcrumbs = array();

   		$breadcrumbs[] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->getLink('common', 'home'),
      		'separator' => false
   		);

   		$breadcrumbs[] = array(
       		'text'      => $this->language->get('text_' . $this->extensionType),
			'href'      => $this->getLink('extension', $this->extensionType),
      		'separator' => ' :: '
   		);
		
   		$breadcrumbs[] = array(
       		'text'      => $this->language->get('text_name'),
			'href'      => $this->getLink($this->extensionType, $this->extension),
      		'separator' => ' :: '
   		);
		
		$fields = array('status', 'customer_group');
		
		foreach ($fields as $field) {
			$key = $this->extension . '_' . $field;
			$this->data[$key] = isset($this->request->post[$key]) ? $this->request->post[$key] : $this->config->get($key);
			if (is_string($this->data[$key]) && strpos($this->data[$key], 'a:') === 0) {
				$this->data[$key] = unserialize($this->data[$key]);
			}
		}
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->load->model('sale/customer_group');
		$this->data['customer_groups'] = array_merge(array(array('name'=>$this->data['text_guest_checkout'],'customer_group_id'=>0)), $this->model_sale_customer_group->getCustomerGroups());
		
		
		$this->template = $this->extensionType . '/' . $this->extension . '.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		
		$this->data['action'] = $this->getLink($this->extensionType, $this->extension);
		$this->data['cancel'] = $this->getLink('extension', $this->extensionType); 
		
		if ($this->getVersion() < 150) {
			$this->document->title = $this->language->get('text_name');
			$this->document->breadcrumbs = $breadcrumbs;
			$this->response->setOutput($this->render(true), $this->config->get('config_compression'));
		} else {
			$this->document->setTitle($this->language->get('text_name'));
			$this->data['breadcrumbs'] = $breadcrumbs;
			$this->response->setOutput($this->render());
		}
	}
	
	private function getLink($a, $b) {
		$route = $a . '/' . $b;
		if ($this->getVersion() >= 150) {
			return $this->url->link($route, 'token=' . $this->session->data['token'], 'SSL');
		} else {
			return HTTPS_SERVER . 'index.php?route=' . $route . '&token=' . $this->session->data['token']; 
		}
	}
	
	private function validate() {		
		if (!$this->user->hasPermission('modify', $this->extensionType . '/' . $this->extension)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>