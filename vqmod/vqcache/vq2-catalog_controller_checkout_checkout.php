<?php  
class ControllerCheckoutCheckout extends Controller { 
	public function index() {
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
	  		$this->redirect($this->url->link('checkout/cart'));
    	}	
		
		// Validate minimum quantity requirments.			
		$products = $this->cart->getProducts();
				
		foreach ($products as $product) {
			$product_total = 0;
				
			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}		
			

			$this->load->model('oca/functions');
			$customer_group_id = ($this->customer->isLogged()) ? $this->customer->getCustomerGroupId() : 0;
			if ($this->model_oca_functions->getField('ocacgcr', 'status') && $this->model_oca_functions->getField('ocacgcr', 'customer_group')) {
				$customer_group_info = $this->model_oca_functions->getField('ocacgcr', 'customer_group');
				if (isset($customer_group_info[$customer_group_id]) && $customer_group_info[$customer_group_id]['status']) {
					$this->load->language('module/ocacgcr');
					if ($this->config->get('config_weight_class_id')) {
						$weight_class = 'weight_class_id';
					} else {
						$weight_class = 'weight_class';
					}
					if ($customer_group_info[$customer_group_id]['total_min'] > $this->cart->getSubTotal()) {
						$this->session->data['error'] = sprintf($this->language->get('error_min_total'), $this->currency->format($customer_group_info[$customer_group_id]['total_min']));
						$this->redirect($this->url->link('checkout/cart'));
					}
					if ($customer_group_info[$customer_group_id]['total_max']) {
						if ($customer_group_info[$customer_group_id]['total_max'] < $this->cart->getSubTotal()) {
							$this->session->data['error'] = sprintf($this->language->get('error_max_total'), $this->currency->format($customer_group_info[$customer_group_id]['total_max']));
							$this->redirect($this->url->link('checkout/cart'));
						}
					}
					if ($customer_group_info[$customer_group_id]['quantity_min'] > $this->cart->countProducts()) {
						$this->session->data['error'] = sprintf($this->language->get('error_min_quantity'), $customer_group_info[$customer_group_id]['quantity_min']);
						$this->redirect($this->url->link('checkout/cart'));
					}
					if ($customer_group_info[$customer_group_id]['quantity_max']) {
						if ($customer_group_info[$customer_group_id]['quantity_max'] < $this->cart->countProducts()) {
							$this->session->data['error'] = sprintf($this->language->get('error_max_quantity'), $customer_group_info[$customer_group_id]['quantity_max']);
							$this->redirect($this->url->link('checkout/cart'));
						}
					}
					if ($customer_group_info[$customer_group_id]['weight_min'] > $this->cart->getWeight()) {
						$this->session->data['error'] = sprintf($this->language->get('error_min_weight'), $this->weight->format($customer_group_info[$customer_group_id]['weight_min'], $this->config->get('config_' . $weight_class)));
						$this->redirect($this->url->link('checkout/cart'));
					}
					if ($customer_group_info[$customer_group_id]['weight_max']) {
						if ($customer_group_info[$customer_group_id]['weight_max'] < $this->cart->getWeight()) {
							$this->session->data['error'] = sprintf($this->language->get('error_max_weight'), $this->weight->format($customer_group_info[$customer_group_id]['weight_max'], $this->config->get('config_' . $weight_class)));
							$this->redirect($this->url->link('checkout/cart'));
						}
					}
					if ($customer_group_info[$customer_group_id]['product_min'] > $product_total) {
						$this->session->data['error'] = sprintf($this->language->get('error_min_product'), $product['name'], $customer_group_info[$customer_group_id]['product_min']);
						$this->redirect($this->url->link('checkout/cart'));
					}
					if ($customer_group_info[$customer_group_id]['product_max']) {
						if ($customer_group_info[$customer_group_id]['product_max'] < $product_total) {
							$this->session->data['error'] = sprintf($this->language->get('error_max_product'), $product['name'], $customer_group_info[$customer_group_id]['product_max']);
							$this->redirect($this->url->link('checkout/cart'));
						}
					}
				}
			}
			
			if ($product['minimum'] > $product_total) {
				$this->redirect($this->url->link('checkout/cart'));
			}				
		}
				
		$this->language->load('checkout/checkout');
		
		$this->document->setTitle($this->language->get('heading_title')); 
		$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
					
		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_cart'),
			'href'      => $this->url->link('checkout/cart'),
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
					
	    $this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_checkout_option'] = $this->language->get('text_checkout_option');
		$this->data['text_checkout_account'] = $this->language->get('text_checkout_account');
		$this->data['text_checkout_payment_address'] = $this->language->get('text_checkout_payment_address');
		$this->data['text_checkout_shipping_address'] = $this->language->get('text_checkout_shipping_address');
		$this->data['text_checkout_shipping_method'] = $this->language->get('text_checkout_shipping_method');
		$this->data['text_checkout_payment_method'] = $this->language->get('text_checkout_payment_method');		
		$this->data['text_checkout_confirm'] = $this->language->get('text_checkout_confirm');
		$this->data['text_modify'] = $this->language->get('text_modify');
		
		$this->data['logged'] = $this->customer->isLogged();
		$this->data['shipping_required'] = $this->cart->hasShipping();	
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/checkout.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/checkout/checkout.tpl';
		} else {
			$this->template = 'default/template/checkout/checkout.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
        
        if (isset($this->request->get['quickconfirm'])) {
            $this->data['quickconfirm'] = $this->request->get['quickconfirm'];
        }
				
		$this->response->setOutput($this->render());
  	}
	
	public function country() {
		$json = array();
		
		$this->load->model('localisation/country');

    	$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		
		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>